<?php

namespace Drupal\Tests\uw_migrate\Kernel;

use Drupal\block\Entity\Block;
use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\Tests\file\Kernel\Migrate\d7\FileMigrationSetupTrait;
use Drupal\Tests\migrate_drupal\Kernel\d7\MigrateDrupal7TestBase;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Test for the complete migration process.
 *
 * @group uw_migrate
 */
class UwMigrateTest extends MigrateDrupal7TestBase {

  /**
   * {@inheritdoc}
   */
  // @codingStandardsIgnoreLine
  protected $strictConfigSchema = FALSE;

  use FileMigrationSetupTrait {
    assertEntity as assertFile;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime', 'image', 'link', 'node', 'taxonomy', 'user', 'text', 'file',
    'simplify_menu', 'image', 'media', 'field', 'block', 'block_content',
    'layout_builder', 'paragraphs', 'migrate_plus', 'path', 'crop', 'views',
    'language', 'content_translation', 'media_library', 'filefield_sources',
    'fences', 'entity_browser', 'require_on_publish', 'image_widget_crop',
    'responsive_image', 'breakpoint', 'entity_browser_entity_form', 'token',
    'metatag', 'menu_link_content', 'menu_ui', 'entity_browser_block_layout',
    'autocomplete_deluxe', 'layout_discovery', 'entity_reference_revisions',
    'better_exposed_filters', 'term_reference_tree', 'telephone', 'address',
    'geofield', 'geofield_map', 'smart_date', 'markup', 'webform_migrate',
    'webform', 'path_alias', 'webform_node', 'filter', 'editor', 'linkit',
    'ckeditor', 'views_taxonomy_term_name_into_id', 'migrate_scanner',
    'content_moderation', 'workflows', 'pathauto', 'ctools', 'comment',
    'features', 'config_update', 'office_hours', 'office_hours_exceptions',

    'uw_media', 'uw_migrate', 'uw_cfg_common', 'uw_ct_blog', 'uw_ct_catalog',
    'uw_ct_contact', 'uw_ct_event', 'uw_ct_news_item', 'uw_ct_web_page',
    'uw_ct_sidebar', 'uw_ct_site_footer', 'uw_custom_blocks', 'uw_ct_profile',
    'uw_ct_service', 'uw_ct_project',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences', 'key_value_expire']);
    $this->installSchema('node', 'node_access');
    $this->installSchema('layout_builder', 'inline_block_usage');
    $this->installSchema('webform', ['webform']);
    $this->installEntitySchema('language_content_settings');
    $this->installEntitySchema('entity_view_mode');
    $this->installEntitySchema('media');
    $this->installEntitySchema('block_content');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('webform');
    $this->installEntitySchema('webform_submission');
    $this->installEntitySchema('path_alias');
    $this->installEntitySchema('menu_link_content');

    $this->installConfig(['user']);
    $this->installConfig(['language']);
    $this->installConfig(['media']);
    $this->installConfig(['media_library']);
    $this->installConfig(['node']);
    $this->installConfig(['taxonomy']);
    $this->installConfig('webform');
    $this->installConfig('webform_node');

    $this->installConfig(['uw_cfg_common']);
    $this->installConfig(['uw_custom_blocks']);
    $this->installConfig(['uw_media']);
    $this->installConfig(['uw_ct_profile']);
    $this->installConfig(['uw_ct_blog']);
    $this->installConfig(['uw_ct_catalog']);
    $this->installConfig(['uw_ct_contact']);
    $this->installConfig(['uw_ct_event']);
    $this->installConfig(['uw_ct_news_item']);
    $this->installConfig(['uw_ct_web_page']);
    $this->installConfig(['uw_ct_sidebar']);
    $this->installConfig(['uw_ct_site_footer']);
    $this->installConfig(['uw_ct_service']);
    $this->installConfig(['uw_ct_project']);
  }

  /**
   * Tests custom migration discovery.
   */
  public function testMigrationDiscovery() {
    /** @var \Drupal\migrate\Plugin\MigrationPluginManager $plugin_manager */
    $plugin_manager = $this->container->get('plugin.manager.migration');
    $definitions = $plugin_manager->getDefinitions();

    // Check the amount of custom migrations.
    $this->assertCount(116, $definitions);

    // Check some random migrations.
    $this->assertArrayHasKey('uw_user', $definitions);
    $this->assertArrayHasKey('uw_file', $definitions);
    $this->assertArrayHasKey('uw_url_alias', $definitions);
    $this->assertArrayHasKey('uw_para_timeline', $definitions);
    $this->assertArrayHasKey('uw_ct_blog', $definitions);
    $this->assertArrayHasKey('uw_mt_image', $definitions);
    $this->assertArrayHasKey('uw_layout_builder_sections:body', $definitions);
    $this->assertArrayHasKey('uw_layout_builder_components:field_body_no_summary', $definitions);
    $this->assertArrayHasKey('uw_cbl_timeline', $definitions);
    $this->assertArrayHasKey('uw_cbl_blockquote:body', $definitions);
    $this->assertArrayHasKey('uw_cbl_remote_video:field_body_no_summary', $definitions);
    $this->assertArrayHasKey('uw_mt_remote_video:field_sidebar_content', $definitions);
  }

  /**
   * Tests custom UW migrations.
   */
  public function testMigrationResults() {
    // Collect all messages of our migrations.
    $this->startCollectingMessages();

    // Users.
    $this->executeMigration('uw_user');
    $this->assertSame('6', $this->getEntityCount('user'));
    $this->assertUser(1, [
      'name' => 'wcmsadmin',
      'mail' => 'wcmsadmin@uwaterloo.ca',
      'roles' => [['target_id' => 'administrator']],
    ]);
    $this->assertUser(2, [
      'name' => 'ajenning',
      'mail' => 'ajenning@uwaterloo.ca',
      'roles' => [
        ['target_id' => 'uw_role_content_editor'],
        ['target_id' => 'uw_role_form_results_access'],
      ],
    ]);

    // Files.
    $this->fileMigrationSetup();
    $this->assertSame('32', $this->getEntityCount('file'));
    $this->assertFile(1, 'zebras2.jpg', 'public://uploads/images/zebras2_0.jpg', 'image/jpeg', '92125', '1321302115', '1321302115', '0');
    $this->assertFile(8, 'Program Change Form.pdf', 'public://fillpdf/Program Change Form.pdf', 'application/pdf', '189937', '1351703482', '1351703482', '0');
    $this->assertFile(18, 'sink_3.jpg', 'public://sink_3.jpg', 'image/jpeg', '104514', '1615841316', '1615841316', '4');

    // Media images.
    $this->executeMigration('uw_mt_image');
    $this->assertSame('27', $this->getEntityCount('media'));
    $this->assertMedia(1, [
      'bundle' => [['target_id' => 'uw_mt_image']],
      'field_media_image' => [
        [
          'target_id' => '1',
          'alt' => '',
          'title' => NULL,
          'width' => '946',
          'height' => '256',
        ],
      ],
    ]);
    $this->assertMedia(15, [
      'bundle' => [['target_id' => 'uw_mt_image']],
      'field_media_image' => [
        [
          'target_id' => '17',
          'alt' => 'Sink ',
          'title' => NULL,
          'width' => '1000',
          'height' => '1000',
        ],
      ],
    ]);

    // Media icons.
    $this->executeMigration('uw_mt_icon');
    $this->assertSame('28', $this->getEntityCount('media'));
    $this->assertMedia(28, [
      'bundle' => [['target_id' => 'uw_mt_icon']],
      'field_media_image_1' => [
        [
          'target_id' => '21',
          'alt' => 'Boxer dog',
          'title' => NULL,
          'width' => '220',
          'height' => '193',
        ],
      ],
    ]);

    // Media files.
    $this->executeMigration('uw_mt_file');
    $this->assertSame('29', $this->getEntityCount('media'));

    // Taxonomy terms.
    $this->executeMigration('uw_taxonomy_term');
    $this->assertSame('67', $this->getEntityCount('taxonomy_term'));

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadMultiple();
    $names = array_map(function ($term) {
      return $term->label();
    }, $terms);

    // Check terms from different vocabularies.
    $this->assertContains('Alumni', $names);
    $this->assertContains('animals', $names);
    $this->assertContains('Conference', $names);
    $this->assertContains('Conrad Grebel University College', $names);
    $this->assertTerm(29, ['name' => 'Researcher']);
    $this->assertTerm(25, ['name' => 'Animal care provider']);
    $this->assertTerm(16, ['name' => 'Faculty']);
    $this->assertTerm(53, ['name' => 'party']);
    $this->assertTerm(2, ['name' => 'Current undergraduate students']);
    $this->assertTerm(50, [
      'name' => 'Coffee Listing',
      'description' => [
        [
          'value' => "<p>Coffee listings catalog.</p>\r\n",
          'format' => 'uw_tf_basic',
        ],
      ],
    ]);

    // Content items of uw_ct_contact type.
    $this->executeMigration('uw_ct_contact');
    $this->assertSame('1', $this->getEntityCount('node'));
    $this->assertNode(1, [
      'title' => 'Professor Dolittel',
      'type' => [['target_id' => 'uw_ct_contact']],
      'uid' => [['target_id' => '0']],
      'field_uw_ct_contact_group' => [['target_id' => '29']],
      'field_uw_ct_contact_affiliation' => 'University of Waterloo',
      'field_uw_ct_contact_title' => 'Animal Care Provider',
      'field_uw_ct_contact_location' => 'Zoo 222',
      'field_uw_ct_contact_image' => [['target_id' => '3']],
      'field_uw_ct_contact_sort_name' => 'Dolittel, Professor',
    ]);

    // Content items of uw_ct_event type.
    $this->executeMigration('uw_ct_event');
    $this->assertSame('2', $this->getEntityCount('node'));
    $this->assertNode(2, [
      'title' => 'Zoology anniversary party',
      'type' => [['target_id' => 'uw_ct_event']],
      'uid' => [['target_id' => '0']],
      'field_uw_event_date' => [
        [
          'value' => '1640291400',
          'end_value' => '1640899800',
          'duration' => '10140',
          'rrule' => NULL,
          'rrule_index' => NULL,
          'timezone' => 'America/Toronto',
        ],
      ],
      'field_uw_event_host' => [
        [
          'uri' => 'http://www.zoology.uwaterloo.ca',
          'title' => 'Department of Zoology',
          'options' => ['attributes' => []],
        ],
      ],
      'field_uw_event_location_address' => [
        [
          'langcode' => NULL,
          'country_code' => 'CA',
          'administrative_area' => 'ON',
          'locality' => 'Waterloo',
          'dependent_locality' => NULL,
          'postal_code' => 'N2L 3G1',
          'sorting_code' => '',
          'address_line1' => '200 University Avenue West',
          'address_line2' => '',
          'organization' => 'DWE - Douglas Wright Engineering Building',
          'given_name' => NULL,
          'additional_name' => NULL,
          'family_name' => NULL,
        ],
      ],
      'field_uw_audience' => [
        ['target_id' => '7'],
        ['target_id' => '8'],
      ],
    ]);
    $node = Node::load(2);
    // Event geolocation data.
    $this->assertSame(43.469981, $node->get('field_uw_event_location_coord')->lat);
    $this->assertSame(-80.540042, $node->get('field_uw_event_location_coord')->lon);

    // Content items of uw_ct_news_item type.
    $this->executeMigration('uw_ct_news_item');
    $this->assertSame('3', $this->getEntityCount('node'));
    $this->assertNode(3, [
      'title' => 'Zoology Prof talks to the animals',
      'type' => [['target_id' => 'uw_ct_news_item']],
      'uid' => [['target_id' => '0']],
      'field_uw_news_date' => [['value' => '2013-04-02']],
      'field_uw_audience' => [],
    ]);

    // Content items of uw_ct_profile type.
    $this->executeMigration('uw_ct_profile');
    $this->assertSame('4', $this->getEntityCount('node'));
    $this->assertNode(4, [
      'title' => 'Professor Dolittel, BA, MSc, PhD',
      'type' => [['target_id' => 'uw_ct_profile']],
      'uid' => [['target_id' => '0']],
      'field_uw_ct_profile_affiliation' => 'University of Waterloo',
      'field_uw_ct_profile_title' => 'Animal Care Provider',
      'field_uw_ct_profile_type' => [
        ['target_id' => '25'],
        ['target_id' => '16'],
      ],
    ]);

    // Content items of uw_ct_web_page type.
    $this->executeMigration('uw_ct_web_page');
    $this->assertSame('12', $this->getEntityCount('node'));
    $this->assertNode(8, [
      'title' => 'Adopt an animal',
      'type' => [['target_id' => 'uw_ct_web_page']],
    ]);
    $this->assertNode(10, [
      'title' => 'Kitchen sink',
      'type' => [['target_id' => 'uw_ct_web_page']],
    ]);
    $this->assertNode(11, [
      'title' => 'Meet our Professor!',
      'type' => [['target_id' => 'uw_ct_web_page']],
    ]);

    // Content items of uw_ct_blog type.
    $this->executeMigration('uw_ct_blog');
    $this->assertSame('14', $this->getEntityCount('node'));
    $this->assertNode(13, [
      'title' => 'First blog post, new you!',
      'type' => [['target_id' => 'uw_ct_blog']],
      'uid' => [['target_id' => '4']],
      'field_uw_blog_date' => '2021-03-26',
      'field_author' => 'Testing Todd',
      'field_uw_audience' => [
        ['target_id' => '2'],
        ['target_id' => '1'],
        ['target_id' => '3'],
        ['target_id' => '24'],
        ['target_id' => '5'],
        ['target_id' => '4'],
        ['target_id' => '6'],
        ['target_id' => '7'],
        ['target_id' => '8'],
        ['target_id' => '9'],
        ['target_id' => '10'],
        ['target_id' => '11'],
        ['target_id' => '12'],
        ['target_id' => '13'],
        ['target_id' => '14'],
      ],
    ]);
    $this->assertNode(14, [
      'title' => 'Economy of California',
      'type' => [['target_id' => 'uw_ct_blog']],
      'field_uw_blog_tags' => [['target_id' => '51']],
      'field_uw_audience' => [
        ['target_id' => '2'],
        ['target_id' => '1'],
        ['target_id' => '3'],
        ['target_id' => '24'],
        ['target_id' => '5'],
        ['target_id' => '4'],
        ['target_id' => '6'],
        ['target_id' => '7'],
        ['target_id' => '8'],
        ['target_id' => '9'],
        ['target_id' => '10'],
        ['target_id' => '11'],
        ['target_id' => '12'],
        ['target_id' => '13'],
        ['target_id' => '14'],
      ],
    ]);

    // Content items of uw_ct_catalog_item type.
    $this->executeMigration('uw_ct_catalog_item');
    $this->assertSame('15', $this->getEntityCount('node'));
    $this->assertNode(15, [
      'title' => 'Starbucks Coffee',
      'type' => [['target_id' => 'uw_ct_catalog_item']],
      'uid' => [['target_id' => '4']],
      'field_uw_catalog_catalog' => [['target_id' => '50']],
      'field_uw_catalog_faculty' => [['target_id' => '49']],
    ]);

    // Content items of uw_ct_sidebar type.
    $this->executeMigrations(['uw_ct_sidebar', 'uw_ct_sidebar_promo_item']);
    $this->assertSame('20', $this->getEntityCount('node'));
    $this->assertNode(17, [
      'title' => 'Adopt an animal',
      'type' => [['target_id' => 'uw_ct_sidebar']],
      // Should be the ID of created "Web page" node.
      'field_uw_attach_page' => [['target_id' => '8']],
    ]);
    $this->assertNode(18, [
      'type' => [['target_id' => 'uw_ct_sidebar']],
      'title' => 'Kitchen sink',
    ]);

    // Content items of uw_ct_site_footer type.
    $this->executeMigration('uw_ct_site_footer');
    $this->assertSame('21', $this->getEntityCount('node'));
    $this->assertNode(21, [
      'title' => 'Example Site footer',
      'type' => [['target_id' => 'uw_ct_site_footer']],
      'field_site_footer_logo' => '365',
    ]);

    // Install uw_ct_opportunities module via installer to trigger hook_install
    // and create taxonomy terms.
    \Drupal::service('module_installer')->install(['uw_ct_opportunities']);
    $this->installConfig(['uw_ct_opportunities']);

    // Content items of uw_ct_opportunity type.
    $this->executeMigration('uw_ct_opportunity');
    $this->assertSame('25', $this->getEntityCount('node'));
    $node = $this->assertNode(22, [
      'title' => 'Drupal Developer',
      'type' => [['target_id' => 'uw_ct_opportunity']],
      'field_uw_opportunity_pay_rate' => 'TBD',
      'field_uw_opportunity_job_id' => '12345678',
      'field_uw_opportunity_post_by' => 'Information Systems & Technology',
      'field_uw_opportunity_date' => '2022-01-05',
      'field_uw_opportunity_deadline' => '2022-06-30T16:30:00',
      'field_uw_opportunity_start_date' => '2022-02-01',
      'field_uw_opportunity_end_date' => '2023-01-31',
      'field_uw_opportunity_pos_number' => '1',
      'field_uw_opportunity_report' => 'Joe Kwan',
      'field_uw_opportunity_link' => [
        [
          'uri' => 'https://google.ca',
          'title' => NULL,
          'options' => [],
        ],
      ],
    ]);
    /** @var \Drupal\taxonomy\TermInterface $type_term */
    $type_term = $node->get('field_uw_opportunity_type')->entity;
    $this->assertEquals('Paid', $type_term->label());
    /** @var \Drupal\taxonomy\TermInterface $type_term */
    $employment_term = $node->get('field_uw_opportunity_employment')->entity;
    $this->assertEquals('Full time', $employment_term->label());
    /** @var \Drupal\taxonomy\TermInterface $type_term */
    $pay_type_term = $node->get('field_uw_opportunity_pay_type')->entity;
    $this->assertEquals('USG', $pay_type_term->label());

    $this->assertNode(24, [
      'field_uw_opportunity_pos_number' => '10+',
    ]);

    $node = $this->assertNode(25, [
      'title' => 'Paid position',
      'type' => [['target_id' => 'uw_ct_opportunity']],
      'field_uw_opportunity_pay_rate' => '$60/hr',
      'field_uw_opportunity_post_by' => 'Cash',
      'field_uw_opportunity_pos_number' => '6',
    ]);
    /** @var \Drupal\taxonomy\TermInterface $type_term */
    $pay_type_term = $node->get('field_uw_opportunity_pay_type')->entity;
    $this->assertEquals('Hourly', $pay_type_term->label());

    // Content items of uw_ct_service type.
    $this->executeMigration('uw_ct_service');
    $this->assertSame('28', $this->getEntityCount('node'));
    $service = $this->assertNode(26, [
      'title' => 'Test service',
      'type' => [['target_id' => 'uw_ct_service']],
      'field_uw_service_status' => 'active',
      'field_uw_service_popularity' => '5',
      'field_uw_service_summary' => '<p>This service has a summary here. Additional services exist, but they are not currently active.</p>',
      'field_uw_service_available' => [
        ['value' => 'stuff'],
        ['value' => 'more stuff'],
      ],
      'field_uw_service_audience' => [
        ['target_id' => '2'],
        ['target_id' => '1'],
        ['target_id' => '24'],
        ['target_id' => '12'],
      ],
      'field_uw_service_notice' => 'day',
      'field_uw_service_length' => 'day',
      'field_uw_service_hours' => [
        // @codingStandardsIgnoreStart
        ['day' => '0', 'starthours' => '100', 'endhours' => '2300', 'comment' => ''],
        ['day' => '1', 'starthours' => '100', 'endhours' => '2300', 'comment' => ''],
        ['day' => '2', 'starthours' => '100', 'endhours' => '2300', 'comment' => ''],
        ['day' => '3', 'starthours' => '100', 'endhours' => '2300', 'comment' => ''],
        ['day' => '4', 'starthours' => '100', 'endhours' => '2300', 'comment' => ''],
        ['day' => '5', 'starthours' => '100', 'endhours' => '2300', 'comment' => ''],
        ['day' => '6', 'starthours' => '100', 'endhours' => '2300', 'comment' => ''],
        ['day' => '1643605200', 'starthours' => '-1', 'endhours' => '-1', 'comment' => NULL],
        ['day' => '1643691600', 'starthours' => '-1', 'endhours' => '-1', 'comment' => NULL],
        ['day' => '1642816800', 'starthours' => '2100', 'endhours' => '100', 'comment' => NULL],
        // @codingStandardsIgnoreEnd
      ],
      'field_uw_service_hours_notes' => 'test note',
      'field_uw_service_location' => [
        [
          'langcode' => NULL,
          'country_code' => 'CA',
          'administrative_area' => 'ON',
          'locality' => 'Waterloo',
          'dependent_locality' => NULL,
          'postal_code' => 'N2L 6R5',
          'sorting_code' => '',
          'address_line1' => '295 Hagey Boulevard',
          'address_line2' => '',
          'organization' => 'ACW - Accelerator Centre Waterloo',
          'given_name' => NULL,
          'additional_name' => NULL,
          'family_name' => NULL,
        ],
      ],
      'field_uw_service_owner' => [
        [
          'uri' => 'https://uwaterloo.ca',
          'title' => 'Test',
          'options' => [
            'attributes' => [],
          ],
        ],
      ],
      'field_uw_service_contacts' => [
        [
          'uri' => 'https://google.ca',
          'title' => 'Contact',
          'options' => [
            'attributes' => [],
          ],
        ],
      ],
    ]);
    $service_category = $service->get('field_uw_service_category')->entity;
    $this->assertEquals('Desktop and Portable Computing', $service_category->label());
    // Geolocation data.
    $this->assertSame(43.477310, $service->get('field_uw_service_location_coord')->lat);
    $this->assertSame(-80.548960, $service->get('field_uw_service_location_coord')->lon);

    $this->assertNode(28, [
      'title' => 'Retired service',
      'type' => [['target_id' => 'uw_ct_service']],
      'field_uw_service_status' => 'retired',
      'field_uw_service_category' => [
        ['target_id' => '57'],
        ['target_id' => '58'],
      ],
    ]);

    // "Project members" paragraphs.
    $this->executeMigration('uw_para_project_members');
    $this->assertSame('4', $this->getEntityCount('paragraph'));

    $paragraph = $this->assertParagraph(1, [
      'type' => [['target_id' => 'uw_para_project_members']],
      'field_uw_project_name' => 'Joe Kwan',
      'field_uw_project_members_link' => [],
    ]);
    $project_role = $paragraph->get('field_uw_project_role')->entity;
    $this->assertEquals('guy in charge', $project_role->label());

    $this->assertParagraph(2, [
      'type' => [['target_id' => 'uw_para_project_members']],
      'field_uw_project_name' => 'Kevin Paxman',
      'field_uw_project_members_link' => [
        [
          'uri' => 'https://google.ca',
          'title' => 'Kevin Paxman',
          'options' => ['attributes' => []],
        ],
      ],
    ]);
    $this->assertParagraph(3, [
      'type' => [['target_id' => 'uw_para_project_members']],
      'field_uw_project_name' => NULL,
      'field_uw_project_members_link' => [
        [
          'uri' => 'https://google.ca',
          'title' => '',
          'options' => ['attributes' => []],
        ],
      ],
    ]);

    // Content items of uw_ct_project type.
    $this->executeMigration('uw_ct_project');
    $this->assertSame('31', $this->getEntityCount('node'));

    $project1 = $this->assertNode(29, [
      'title' => 'WCMS 3.0',
      'type' => [['target_id' => 'uw_ct_project']],
      'field_uw_project_members' => [
        [
          'target_id' => '1',
          'target_revision_id' => '1',
        ],
        [
          'target_id' => '2',
          'target_revision_id' => '2',
        ],
        [
          'target_id' => '3',
          'target_revision_id' => '3',
        ],
      ],
      'field_uw_project_timeline' => [
        [
          'value' => '1646110800',
          'end_value' => '1646110800',
          'duration' => '0',
          'rrule' => NULL,
          'rrule_index' => NULL,
          'timezone' => 'America/Toronto',
        ],
      ],
      'field_uw_project_summary' => 'This project has a description, and this is it.',
    ]);

    $project1_status = $project1->get('field_uw_project_status')->entity;
    $this->assertEquals('Current projects', $project1_status->label());

    $project1_topics = $project1->get('field_uw_project_topics')->referencedEntities();
    $this->assertEquals('wcms', $project1_topics[0]->label());
    $this->assertEquals('web', $project1_topics[1]->label());

    $project2 = $this->assertNode(31, [
      'title' => 'Massive project',
      'type' => [['target_id' => 'uw_ct_project']],
      'field_uw_project_members' => [
        [
          'target_id' => '4',
          'target_revision_id' => '4',
        ],
      ],
      'field_uw_project_timeline' => [],
      'field_uw_project_summary' => 'Seriously, it&amp;#39;s big.',
    ]);
    $project2_status = $project2->get('field_uw_project_status')->entity;
    $this->assertEquals('Current demonstrators', $project2_status->label());

    // Call to action paragraphs.
    $this->executeMigration('uw_para_call_to_action');
    $this->assertSame('6', $this->getEntityCount('paragraph'));
    $this->assertParagraph(5, [
      'type' => [['target_id' => 'uw_para_call_to_action']],
      'field_uw_cta_link' => [
        [
          'uri' => 'https://uwaterloo.ca/',
          'title' => 'Check out UWaterloo!',
          'options' => ['attributes' => []],
        ],
      ],
      'field_uw_cta_text_details' => [
        [
          'text_value' => 'Check out UWaterloo!',
          'style' => 'big',
          'icon' => NULL,
        ],
      ],
      'field_uw_cta_theme' => 'org-eng',
    ]);
    $this->assertParagraph(6, [
      'type' => [['target_id' => 'uw_para_call_to_action']],
      'field_uw_cta_link' => [
        [
          'uri' => 'https://uwaterloo.ca/',
          'title' => 'University of Waterloo',
          'options' => ['attributes' => []],
        ],
      ],
      'field_uw_cta_text_details' => [
        [
          'text_value' => 'UWaterloo',
          'style' => 'small',
          'icon' => NULL,
        ],
        [
          'text_value' => 'University of Waterloo',
          'style' => 'big',
          'icon' => NULL,
        ],
      ],
      'field_uw_cta_theme' => 'org-default',
    ]);

    // Call to action blocks.
    $this->executeMigrations(['uw_cbl_call_to_action']);
    $this->assertSame('2', $this->getEntityCount('block_content'));
    $this->assertBlockContent(1, [
      'type' => [['target_id' => 'uw_cbl_call_to_action']],
      'info' => 'Uwaterloo CTA',
      'field_uw_cta_details' => [
        [
          'target_id' => '5',
          'target_revision_id' => '5',
        ],
      ],
    ]);

    // Timeline paragraphs.
    $this->executeMigrations(['uw_para_timeline']);
    $this->assertSame('12', $this->getEntityCount('paragraph'));
    $this->assertParagraph(7, [
      'type' => [['target_id' => 'uw_para_timeline']],
      'field_uw_timeline_content' => [
        [
          'value' => "<p>A nice sink.</p>\r\n",
          'format' => 'uw_tf_basic',
        ],
      ],
      'field_uw_timeline_date' => '2021-03-15',
      'field_uw_timeline_headline' => 'Sink 1',
      'field_uw_timeline_link' => [
        [
          'uri' => 'https://uwaterloo.ca',
          'title' => 'Sink 1',
          'options' => ['attributes' => []],
        ],
      ],
      'field_uw_timeline_photo' => [['target_id' => '18']],
    ]);
    $this->assertParagraph(9, [
      'type' => [['target_id' => 'uw_para_timeline']],
      'field_uw_timeline_date' => '2021-04-14',
      'field_uw_timeline_headline' => 'Sink 3',
      'field_uw_timeline_photo' => [['target_id' => '16']],
    ]);

    // Timeline blocks.
    $this->executeMigrations(['uw_cbl_timeline']);
    $this->assertSame('4', $this->getEntityCount('block_content'));
    $this->assertBlockContent(3, [
      'type' => [['target_id' => 'uw_cbl_timeline']],
      'info' => 'Sinks',
      'field_uw_timeline_style' => 'vertical-month',
      'field_uw_timeline_sort' => 'desc',
      'field_uw_timeline' => [
        ['target_id' => '7', 'target_revision_id' => '7'],
        ['target_id' => '8', 'target_revision_id' => '8'],
        ['target_id' => '9', 'target_revision_id' => '9'],
      ],
    ]);

    // Blockquote blocks.
    $this->executeMigrations(['uw_cbl_blockquote']);
    $this->assertSame('5', $this->getEntityCount('block_content'));
    $this->assertBlockContent(5, [
      'type' => [['target_id' => 'uw_cbl_blockquote']],
      'info' => 'Blockquote',
      'field_uw_bq_quote_text' => "<p>I made 'True Detective' like it was going to be the only thing I ever made for television. So put in everything and the kitchen sink. Everything.</p>",
      'field_uw_bq_quote_attribution' => '<p><strong>Nic Pizzolatto</strong></p>',
    ]);

    // Google Maps blocks.
    $this->executeMigrations(['uw_cbl_google_maps']);
    $this->assertSame('7', $this->getEntityCount('block_content'));
    $this->assertBlockContent(6, [
      'type' => [['target_id' => 'uw_cbl_google_maps']],
      'info' => 'Google map',
      'field_gmaps_embedded_url' => [
        [
          'uri' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2895.440085581775!2d-80.54704628450811!3d43.47228537912795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882bf6ad02edccff%3A0xdd9df23996268e17!2sUniversity+of+Waterloo!5e0!3m2!1sen!2sca!4v1546616328927',
          'title' => NULL,
          'options' => [],
        ],
      ],
      'field_gmaps_height' => '150',
    ]);
    $this->assertBlockContent(7, [
      'field_gmaps_embedded_url' => [
        [
          'uri' => 'https://www.google.com/maps/d/u/1/embed?mid=1DA0z0wGn-xGfpm_UW6DmaZdhgoJUiNn8&usp=sharing',
          'title' => NULL,
          'options' => [],
        ],
      ],
      'field_gmaps_height' => '200',
    ]);

    // Mailman subscription blocks.
    $this->executeMigrations(['uw_cbl_mailman']);
    $this->assertSame('9', $this->getEntityCount('block_content'));
    $this->assertBlockContent(8, [
      'type' => [['target_id' => 'uw_cbl_mailman']],
      'info' => 'uwweb',
      'field_uw_mm_servername' => 'uwweb',
      'field_uw_mm_server' => 'lists.uwaterloo.ca',
    ]);

    // PowerBi blocks.
    $this->executeMigrations(['uw_cbl_powerbi']);
    $this->assertSame('10', $this->getEntityCount('block_content'));
    $this->assertBlockContent(10, [
      'type' => [['target_id' => 'uw_cbl_powerbi']],
      'info' => 'PowerBi',
      'field_uw_powerbi_url' => 'https://app.powerbi.com/reportEmbed?reportId=135a5c3f-454c-4f1b-acb8-bf965952c9e1&autoAuth=true&ctid=723a5a87-f39a-4a22-9247-3fc240c01396&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLWNhbmFkYS1jZW50cmFsLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9',
    ]);

    // Media remote videos.
    $this->executeMigrations(['uw_mt_remote_video']);
    $this->assertSame('32', $this->getEntityCount('media'));
    $this->assertMedia(30, [
      'bundle' => [['target_id' => 'uw_mt_remote_video']],
      'field_media_oembed_video' => [['value' => 'https://www.youtube.com/watch?v=LJ_JiJ1G3-E']],
    ]);
    $this->assertMedia(31, [
      'bundle' => [['target_id' => 'uw_mt_remote_video']],
      'field_media_oembed_video' => [['value' => 'https://vimeo.com/42845254']],
    ]);

    // Remote video blocks.
    $this->executeMigrations(['uw_cbl_remote_video']);
    $this->assertSame('13', $this->getEntityCount('block_content'));
    $this->assertBlockContent(12, [
      'type' => [['target_id' => 'uw_cbl_remote_video']],
      'info' => 'Remote video',
      'field_uw_remote_video' => [['target_id' => '31']],
    ]);
    $this->assertBlockContent(13, [
      'field_uw_remote_video' => [['target_id' => '32']],
    ]);

    // Tableau visualization blocks.
    $this->executeMigrations(['uw_cbl_tableau']);
    $this->assertSame('16', $this->getEntityCount('block_content'));
    $this->assertBlockContent(14, [
      'type' => [['target_id' => 'uw_cbl_tableau']],
      'info' => 'Tableau visualization',
      'field_uw_tbl_tableau_name' => 'University of Waterloo',
      'field_uw_tbl_tableau_height' => '150',
      'field_uw_tbl_site_name' => 'https://uwaterloo.ca/',
      'field_uw_tbl_server' => 'public',
    ]);
    $this->assertBlockContent(15, [
      'field_uw_tbl_tableau_name' => 'SPINTERIM_ExperientialLearning/Incoming-IND',
      'field_uw_tbl_tableau_height' => '650',
      'field_uw_tbl_server' => 'public',
      'field_uw_tbl_site_name' => NULL,
    ]);

    $this->executeMigrations(['uw_para_fact_figure']);
    $this->assertSame('20', $this->getEntityCount('paragraph'));
    $this->assertParagraph(13, [
      'type' => [['target_id' => 'uw_para_fact_figure']],
      'field_uw_ff_info' => [
        [
          'text_value' => 'People who like kitchen sinks',
          'style' => 'small',
          'icon' => NULL,
        ],
      ],
    ]);
    $this->assertParagraph(17, [
      'type' => [['target_id' => 'uw_para_fact_figure']],
      'field_uw_ff_info' => [
        [
          'text_value' => 'Dogs that approve of kitchen sinks',
          'style' => 'medium',
          'icon' => '28',
        ],
      ],
    ]);

    $this->executeMigration('uw_para_ff');
    $this->assertSame('22', $this->getEntityCount('paragraph'));
    $this->assertParagraph(21, [
      'type' => [['target_id' => 'uw_para_ff']],
      'field_uw_ff_dbg_color' => 'grey',
      'field_uw_ff_def_color' => 'org-default',
      'field_uw_ff_show_bubbles' => '1',
      'field_uw_ff_textalign' => 'center',
      'field_uw_fact_figure' => [
        ['target_id' => '13', 'target_revision_id' => '13'],
        ['target_id' => '14', 'target_revision_id' => '14'],
        ['target_id' => '15', 'target_revision_id' => '15'],
        ['target_id' => '16', 'target_revision_id' => '16'],
        ['target_id' => '17', 'target_revision_id' => '17'],
      ],
    ]);

    $this->executeMigrations(['uw_cbl_facts_and_figures']);
    $this->assertSame('18', $this->getEntityCount('block_content'));
    $this->assertBlockContent(17, [
      'type' => [['target_id' => 'uw_cbl_facts_and_figures']],
      'field_uw_ff_fact_figure' => [
        ['target_id' => '21', 'target_revision_id' => '21'],
      ],
    ]);

    // Related links blocks.
    $this->executeMigrations(['uw_cbl_related_links']);
    $this->assertSame('21', $this->getEntityCount('block_content'));
    $this->assertBlockContent(19, [
      'type' => [['target_id' => 'uw_cbl_related_links']],
      'info' => 'Related links',
      'field_uw_rl_related_link' => [
        [
          'uri' => 'https://uwaterloo.ca/',
          'title' => 'University of Waterloo',
          'options' => ['attributes' => []],
        ],
        [
          'uri' => 'https://lib.uwaterloo.ca/web/index.php',
          'title' => 'Library ',
          'options' => ['attributes' => []],
        ],
        [
          'uri' => 'https://en.wikipedia.org/wiki/Main_Page',
          'title' => 'Wikipedia',
          'options' => ['attributes' => []],
        ],
      ],
    ]);
    $this->assertBlockContent(20, [
      'type' => [['target_id' => 'uw_cbl_related_links']],
      'info' => 'Related links',
      'field_uw_rl_related_link' => [
        [
          'uri' => 'https://uwaterloo.ca/',
          'title' => 'University of Waterloo ',
          'options' => ['attributes' => []],
        ],
        [
          'uri' => 'https://www.google.com/',
          'title' => 'Google',
          'options' => ['attributes' => []],
        ],
      ],
    ]);

    // Image gallery.
    $this->executeMigration('uw_cbl_image_gallery');
    $this->assertSame('21', $this->getEntityCount('block_content'));

    // Copy text blocks.
    $this->executeMigrations(['uw_cbl_copy_text']);
    $this->assertSame('93', $this->getEntityCount('block_content'));

    $this->executeMigration('uw_field_body_no_summary_promo_item');
    // We should have one more text block (promo item).
    $this->assertSame('94', $this->getEntityCount('block_content'));
    $this->assertBlockContent(94, [
      'type' => [['target_id' => 'uw_cbl_copy_text']],
      'field_uw_copy_text' => [
        [
          'value' => "<p>We've got a new promotional item here! Come check out the University at www.uwaterloo.ca</p>",
          'format' => 'uw_tf_standard',
        ],
      ],
    ]);

    // Layout builder sections and blocks.
    $this->executeMigrations([
      'uw_layout_builder_sections',
      'uw_layout_builder_components',
    ]);
    $node = Node::load(18);
    /** @var \Drupal\layout_builder\SectionListInterface $layout */
    $layout = $node->get('layout_builder__layout');
    $this->assertCount(1, $layout->getSections());

    $section = $layout->getSection(0);
    $this->assertCount(7, $section->getComponents());

    // "Related links" block should be appended to the top.
    $this->executeMigration('uw_cbl_related_links_lb');
    $components = $section->getComponents();
    $this->assertCount(8, $components);
    $this->assertSame('inline_block:uw_cbl_related_links', end($components)->getPluginId());

    // Finally, promo item blocks should be appended to the top.
    $this->executeMigration('uw_field_body_no_summary_promo_item_lb');
    $components = Node::load(18)->get('layout_builder__layout')->getSection(0)->getComponents();
    $this->assertCount(9, $components, print_r($components, 1));
    $this->assertSame('inline_block:uw_cbl_copy_text', end($components)->getPluginId());

    // "Custom listing" pages should have "Profile list (manual)" as the last
    // section component.
    $this->executeMigration('uw_layout_builder_profile_list');
    $components = Node::load(11)->get('layout_builder__layout')->getSection(0)->getComponents();
    $profile_list_component = end($components);
    $this->assertSame('uw_cbl_profiles_manual', $profile_list_component->getPluginId());

    // Verify web page with many sections.
    $node = Node::load(10);
    /** @var \Drupal\layout_builder\SectionListInterface $layout */
    $layout = $node->get('layout_builder__layout');
    $this->assertCount(12, $layout->getSections());

    // Check random sections.
    $this->assertSame('uw_2_column', $layout->getSection(1)->getLayoutId());
    $this->assertSame([
      'label' => '',
      'column_class' => 'larger-right',
    ], $layout->getSection(4)->getLayoutSettings());
    $this->assertSame('uw_3_column', $layout->getSection(10)->getLayoutId());
    $this->assertSame([
      'label' => '',
      'column_class' => 'even-split',
    ], $layout->getSection(10)->getLayoutSettings());
    $this->assertSame('uw_1_column', $layout->getSection(11)->getLayoutId());

    // Load resulting layout.
    $node = Node::load(21);
    $layout = $node->get('layout_builder__layout');
    // Test that it has 1 section.
    $this->assertCount(1, $layout->getSections());
    // Test that the section has 1 block of type uw_cbl_copy_text.
    $components = $layout->getSection(0)->getComponents();
    $this->assertCount(1, $components, print_r($components, 1));
    $this->assertSame('inline_block:uw_cbl_copy_text', end($components)->getPluginId());

    // Webform migrations.
    $this->executeMigration('uw_webform');
    $this->assertWebform('webform_49', [
      'title' => 'Adopt an animal',
    ]);
    $this->assertWebform('webform_50', [
      'title' => 'Program change form',
    ]);
    $this->assertWebform('webform_102', [
      'title' => 'Contact us webform',
    ]);

    $this->executeMigration('uw_webform_submission');
    $this->assertWebformSubmission(1, 'webform_49', [
      'adoption_type' => '2',
      'animals_to_choose_from' => 'tiger',
      'hidden_email_21' => 'njennings@example.com',
    ]);
    $this->assertWebformSubmission(3, 'webform_50', [
      'current_department_school' => 'Waterloo',
      'current_program' => 'History',
      'date' => '2012-10-31',
      'effective_term_for_new_program' => 'Winter 2013',
      'first_names' => 'testfirst',
      'last_names' => 'testlast',
      'new_program' => 'Math',
      'reason_for_change' => "It's old school.",
      'student_id_number' => '12345678',
    ]);
    $this->assertWebformSubmission(4, 'webform_102', [
      'address' => [
        'address' => 'ffg',
        'address_2' => 'hjjj',
        'city' => 'Kitchener',
        'country' => 'CA',
        'postal_code' => 'N1N 1N1',
        'state_province' => 'ON',
      ],
      'email' => 'Thedog@gmail.com',
      'phone_number' => '5194952956',
    ]);

    // Run layout builder migration for webform blocks.
    $this->executeMigration('uw_webform_lb');
    $node = Node::load(8);
    /** @var \Drupal\layout_builder\SectionListInterface $layout */
    $layout = $node->get('layout_builder__layout');
    $components = $layout->getSection(0)->getComponents();
    $this->assertCount(1, $components);
    $component = end($components);
    $this->assertSame([
      'id' => 'webform_block',
      'label' => 'Adopt an animal',
      'provider' => 'webform',
      'label_display' => 0,
      'webform_id' => 'webform_49',
    ], $component->get('configuration'));

    // Special alert.
    $this->executeMigration('uw_cbl_special_alert');
    $block = Block::load('specialalert');
    $this->assertSame([
      'id' => 'uw_cbl_special_alert',
      'label' => 'Special alert',
      'label_display' => '0',
      'provider' => 'uw_custom_blocks',
      'display_option' => '1',
      'message' => [
        'value' => '<p>Come check out this special alert!</p>',
        'format' => 'uw_tf_basic',
      ],
    ], $block->get('settings'));

    $this->stopCollectingMessages();
  }

  /**
   * {@inheritdoc}
   */
  protected function getFixtureFilePath() {
    return __DIR__ . '/../../fixtures/drupal7.php';
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileMigrationInfo() {
    return [
      'path' => 'public://sites/default/files/cube.jpeg',
      'size' => '3620',
      'base_path' => 'public://',
      'plugin_id' => 'd7_file',
    ];
  }

  /**
   * Tests a single entity values.
   */
  protected function assertEntity(EntityInterface $entity, $values) {
    $message = "Failure for " . $entity->getEntityTypeId() . " entity type with id of '" . $entity->id() . "'";
    foreach ($values as $field_name => $expected_value) {
      $actual_value = $entity->get($field_name);

      if ($entity instanceof ContentEntityInterface) {
        if (is_array($expected_value)) {
          $actual_value = $entity->get($field_name)->getValue();
        }
        else {
          $actual_value = $entity->get($field_name)->value;
        }
      }
      $this->assertSame($expected_value, $actual_value, $message);
    }
    return $entity;
  }

  /**
   * Tests a single media entity.
   */
  protected function assertMedia($id, $values) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = Media::load($id);
    $this->assertInstanceOf(MediaInterface::class, $media);
    return $this->assertEntity($media, $values);
  }

  /**
   * Tests a single user entity.
   */
  protected function assertUser($id, $values) {
    /** @var \Drupal\user\UserInterface $user */
    $user = User::load($id);
    $this->assertInstanceOf(UserInterface::class, $user);
    return $this->assertEntity($user, $values);
  }

  /**
   * Tests a single node entity.
   */
  protected function assertNode($id, $values) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = Node::load($id);
    $this->assertInstanceOf(NodeInterface::class, $node);
    return $this->assertEntity($node, $values);
  }

  /**
   * Tests a single term entity.
   */
  protected function assertTerm($id, $values) {
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = Term::load($id);
    $this->assertInstanceOf(TermInterface::class, $term);
    return $this->assertEntity($term, $values);
  }

  /**
   * Tests a single paragraph entity.
   */
  protected function assertParagraph($id, $values) {
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = Paragraph::load($id);
    $this->assertInstanceOf(ParagraphInterface::class, $paragraph);
    return $this->assertEntity($paragraph, $values);
  }

  /**
   * Tests a single paragraph entity.
   */
  protected function assertBlockContent($id, $values) {
    /** @var \Drupal\block_content\BlockContentInterface $block */
    $block = BlockContent::load($id);
    $this->assertInstanceOf(BlockContentInterface::class, $block);
    return $this->assertEntity($block, $values);
  }

  /**
   * Tests a single webform entity.
   */
  protected function assertWebform($id, $values) {
    /** @var \Drupal\webform\Entity\Webform $webform */
    $webform = Webform::load($id);
    $this->assertInstanceOf(Webform::class, $webform);
    return $this->assertEntity($webform, $values);
  }

  /**
   * Tests a single webform submission entity.
   */
  protected function assertWebformSubmission($id, $webform_id, $data) {
    /** @var \Drupal\webform\Entity\WebformSubmission $submission */
    $submission = WebformSubmission::load($id);
    $this->assertInstanceOf(WebformSubmission::class, $submission);

    $this->assertSame($webform_id, $submission->getWebform()->id());
    $this->assertSame($data, $submission->getData());
    return $submission;
  }

  /**
   * Returns amount of entity of the given entity type.
   */
  protected function getEntityCount($entity_type) {
    $query = \Drupal::entityTypeManager()->getStorage($entity_type)->getQuery();
    return $query->accessCheck(FALSE)->count()->execute();
  }

  /**
   * Prepare the file migration for running.
   */
  protected function fileMigrationSetup() {
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);

    $migration = $this->getMigration('uw_file');

    $source = $migration->getSourceConfiguration();
    $source['constants']['source_base_path'] = __DIR__ . '/../../fixtures';
    $migration->set('source', $source);
    $this->executeMigration($migration);
  }

}
