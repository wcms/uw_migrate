<?php

namespace Drupal\Tests\uw_migrate\Kernel\process;

use Drupal\KernelTests\KernelTestBase;
use Drupal\uw_migrate\Plugin\migrate\process\UwSummary;

/**
 * Tests the uw_taxonomy_term_lookup custom plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwSummary
 * @group uw_migrate
 */
class UwSummaryTest extends KernelTestBase {

  // @todo Make this work with $strictConfigSchema and remove this block.
  // phpcs:disable DrupalPractice.Objects.StrictSchemaDisabled.StrictConfigSchema
  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system', 'migrate', 'text', 'filter', 'file', 'media', 'simplify_menu',
    'editor', 'linkit', 'ckeditor', 'token', 'media_embed_extra', 'node',
    'uw_cfg_common', 'taxonomy', 'user', 'content_moderation', 'workflows',
    'path', 'path_alias', 'features', 'config_update', 'layout_builder',
  ];

  /**
   * The source row.
   *
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * The migrate mock object.
   *
   * @var \Drupal\migrate\MigrateExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $migrateExecutable;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig('filter');
    $this->installConfig('text');
    // Install uw_cfg_common to create "uw_tf_standard" text format.
    $this->installConfig('uw_cfg_common');

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();
    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Lookup for the existing term by name.
   *
   * @covers ::transform
   * @dataProvider providerTransform
   */
  public function testTransform($source, $original, $trimmed) {
    $plugin = new UwSummary(['trimmed' => FALSE], 'uw_summary', []);
    $actual = $plugin->transform($source, $this->migrateExecutable, $this->row, 'destination');
    $this->assertSame($original, $actual);

    $plugin_trimmed = new UwSummary(['trimmed' => TRUE], 'uw_summary', []);
    $actual_trimmed = $plugin_trimmed->transform($source, $this->migrateExecutable, $this->row, 'destination');
    $this->assertSame($trimmed, $actual_trimmed);
  }

  /**
   * Provider for self::testTransform().
   */
  public function providerTransform() {
    return [
      'One line of text.' => [
        [
          'value' => '<p>Body text.</p>',
          'summary' => '<p>Summary text.</p>',
        ],
        '<p>Summary text.</p>',
        'Summary text.',
      ],
      'Multiple lines with some extra markup.' => [
        [
          'value' => '<h2>Stuff</h2>
<p>The stuff is <a href=https://google.ca>found here</a>.</p>
<p>It is really great stuff and here I am describing it for what will end up being more than 160 characters, at least I hope &ndash; it&rsquo;s not like I&rsquo;m keeping track. (enough characters to take us to say, 595 characters), and so on.</p>',
          'summary' => '',
        ],
        "<h2>Stuff</h2>
<p>The stuff is <a href=\"https://google.ca\">found here</a>.</p>
<p>It is really great stuff and here I am describing it for what will end up being more than 160 characters, at least I hope – it’s not like I’m keeping track. (enough characters to take us to say, 595 characters), and so on.</p>",
        'Stuff
The stuff is found here.',
      ],
      'Multiple lines with teaser break.' => [
        [
          'value' => '<h2>Stuff</h2>
<p>The stuff is <a href=https://google.ca>found here</a>.<!--break--></p>Some other text.',
        ],
        '<h2>Stuff</h2>
<p>The stuff is <a href="https://google.ca">found here</a>.</p>',
        'Stuff
The stuff is found here.',
      ],
    ];
  }

}
