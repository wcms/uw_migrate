<?php

namespace Drupal\Tests\uw_migrate\Kernel\process;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\uw_migrate\Plugin\migrate\process\UwTermLookup;

/**
 * Tests the uw_taxonomy_term_lookup custom plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwTermLookup
 * @group uw_migrate
 */
class UwTermLookupTest extends KernelTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate',
    'system',
    'text',
    'filter',
    'user',
    'taxonomy',
  ];

  /**
   * The source row.
   *
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * The migrate mock object.
   *
   * @var \Drupal\migrate\MigrateExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $migrateExecutable;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('taxonomy_term');

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();
    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Lookup for the existing term by name.
   *
   * @covers ::transform
   */
  public function testTermLookup() {
    $vocabulary1 = $this->createVocabulary();
    $vocabulary2 = $this->createVocabulary();
    $term = $this->createTerm($vocabulary1, ['name' => 'Test term']);

    $plugin = new UwTermLookup([], 'uw_taxonomy_term_lookup', [], \Drupal::entityTypeManager());

    $actual = $plugin->transform(['Test term', $vocabulary1->id()], $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertSame($term->id(), $actual);

    $actual = $plugin->transform(['Test', $vocabulary1->id()], $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertNull($actual);

    $actual = $plugin->transform(['Test', $vocabulary2->id()], $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertNull($actual);
  }

}
