<?php

namespace Drupal\Tests\uw_migrate\Kernel\process;

use Drupal\KernelTests\KernelTestBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\uw_migrate\Plugin\migrate\process\UwBlockLookup;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the uw_block_lookup custom plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwBlockLookup
 * @group uw_migrate
 */
class UwBlockLookupTest extends KernelTestBase {

  use ProphecyTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'system',
    'migrate',
    'migrate_drupal',
    'block_content',
    'uw_migrate',
    'migrate_plus',
    'migrate_scanner',
    'path_alias',
    'pathauto',
    'token',
  ];

  /**
   * Tests plugin constructor and dynamic configuration.
   */
  public function testDynamicConfiguration() {
    $migrate_lookup = \Drupal::service('migrate.lookup');
    $migrate_stub = \Drupal::service('migrate.stub');
    /** @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager */
    $migration_plugin_manager = \Drupal::service('plugin.manager.migration');
    $migration_plugin = $this->prophesize(MigrationInterface::class);

    $plugin = new UwBlockLookup(['field' => 'body'], 'uw_taxonomy_term_lookup', [], $migration_plugin->reveal(), $migrate_lookup, $migrate_stub, $migration_plugin_manager);
    $actual_configuration = $plugin->getConfiguration();

    // Verify available migrations.
    $this->assertArrayHasKey('migration', $actual_configuration);
    $this->assertContains('uw_cbl_copy_text:body', $actual_configuration['migration']);
    $this->assertContains('uw_cbl_google_maps:body', $actual_configuration['migration']);
    $this->assertContains('uw_cbl_facts_and_figures', $actual_configuration['migration']);
    $this->assertContains('uw_field_body_no_summary_promo_item', $actual_configuration['migration']);
    $this->assertCount(12, $actual_configuration['migration']);

    // Verify source ids.
    $this->assertArrayHasKey('source_ids', $actual_configuration);
    $this->assertArrayHasKey('uw_cbl_copy_text:body', $actual_configuration['source_ids']);
    $this->assertEquals(['entity_id', 'delta'], $actual_configuration['source_ids']['uw_cbl_copy_text:body']);
    $this->assertArrayHasKey('uw_cbl_blockquote:body', $actual_configuration['source_ids']);
    $this->assertEquals(['entity_id', 'delta'], $actual_configuration['source_ids']['uw_cbl_blockquote:body']);

    $this->assertTrue($actual_configuration['no_stub']);
  }

}
