<?php

namespace Drupal\Tests\uw_migrate\Kernel\process;

use Drupal\KernelTests\KernelTestBase;
use Drupal\uw_migrate\Plugin\migrate\process\UwSmartDate;

/**
 * Tests the uw_smartdate custom plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwSmartDate
 * @group uw_migrate
 */
class UwSmartDateTest extends KernelTestBase {

  /**
   * The source row.
   *
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * The migrate mock object.
   *
   * @var \Drupal\migrate\MigrateExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $migrateExecutable;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'node',
    'migrate',
    'system',
    'smart_date',
    'smart_date_recur',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installEntitySchema('smart_date_rule');
    $this->installEntitySchema('smart_date_override');

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();
    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();

    $config = \Drupal::service('config.factory');
    $config->getEditable('system.date')
      ->set('timezone.default', 'America/Toronto')
      ->save();
  }

  /**
   * Tests plugin constructor and dynamic configuration.
   *
   * @covers ::transform
   * @dataProvider providerTransform
   */
  public function testTransform($actual, $expected) {
    $plugin = new UwSmartDate([], 'uw_smartdate', []);
    $actual = $plugin->transform($actual, $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertSame($expected, $actual);
  }

  /**
   * Provider for self::testTransform().
   */
  public function providerTransform() {
    return [
      [
        [
          [
            'value' => '2011-10-31T14:00:00',
            'value2' => '2011-10-31T14:00:00',
            'timezone' => 'America/Toronto',
            'rrule' => NULL,
          ],
        ],
        [
          [
            'value' => 1320084000,
            'end_value' => 1320084000,
            'duration' => 0,
            'timezone' => 'America/Toronto',
          ],
        ],
      ],
      [
        [
          [
            'value' => '2011-06-05T00:00:00',
            'value2' => '2011-06-08T00:00:00',
            'timezone' => 'America/Toronto',
            'rrule' => NULL,
          ],
        ],
        [
          [
            'value' => 1307246400,
            'end_value' => 1307505600,
            'duration' => 4320,
            'timezone' => 'America/Toronto',
          ],
        ],
      ],
      [
        [
          [
            'value' => '2011-06-05T00:00:00',
            'value2' => '2011-06-08T00:00:00',
            'timezone' => 'America/Toronto',
            'rrule' => 'RRULE:FREQ=MONTHLY;INTERVAL=1;BYMONTH=4;BYMONTHDAY=11;COUNT=2;WKST=SU',
          ],
        ],
        [
          [
            'value' => 1307764800,
            'end_value' => 1308024000,
            'duration' => 4320,
            'timezone' => 'America/Toronto',
            'repeat' => 'MONTHLY',
            'repeat-advanced' => [
              'interval' => 1,
              'which' => '11',
              'day' => '',
              'byday' => [],
              'byhour' => [],
              'byminute' => [],
            ],
            'interval' => 1,
            'repeat-end' => 'COUNT',
            'repeat-end-count' => '2',
            'repeat-end-date' => '',
            '_original_delta' => 0,
            'rrule' => '1',
            'rrule_index' => 1,
          ],
          [
            'value' => 1310356800,
            'end_value' => 1310616000,
            'duration' => 4320,
            'timezone' => 'America/Toronto',
            'repeat' => 'MONTHLY',
            'repeat-advanced' => [
              'interval' => 1,
              'which' => '11',
              'day' => '',
              'byday' => [],
              'byhour' => [],
              'byminute' => [],
            ],
            'interval' => 1,
            'repeat-end' => 'COUNT',
            'repeat-end-count' => '2',
            'repeat-end-date' => '',
            '_original_delta' => 0,
            'rrule' => '1',
            'rrule_index' => 2,
          ],
        ],
      ],
      [
        [
          [
            'value' => '2012-02-15T09:00:00',
            'value2' => '2012-02-15T23:00:00',
            'timezone' => 'America/Toronto',
            'rrule' => 'RRULE:FREQ=WEEKLY;INTERVAL=4;BYDAY=TU;UNTIL=20120622T035959Z;WKST=SU
EXDATE:20120313T040000Z,20120410T040000Z,20120508T040000Z,20120605T040000Z
RDATE:20120320T040000Z,20120417T040000Z,20120524T040000Z,20120621T040000Z',
          ],
        ],
        [
          [
            'value' => 1331643600,
            'end_value' => 1331694000,
            'duration' => 840,
            'timezone' => 'America/Toronto',
            'repeat' => 'WEEKLY',
            'repeat-advanced' => [
              'interval' => 4,
              'which' => '',
              'day' => '',
              'byday' => ['TU'],
              'byhour' => [],
              'byminute' => [],
            ],
            'interval' => 4,
            'repeat-end' => 'UNTIL',
            'repeat-end-count' => '',
            'repeat-end-date' => '20120622T035959Z',
            '_original_delta' => 0,
            'rrule' => '1',
            'rrule_index' => 1,
          ],
          [
            'value' => 1334062800,
            'end_value' => 1334113200,
            'duration' => 840,
            'timezone' => 'America/Toronto',
            'repeat' => 'WEEKLY',
            'repeat-advanced' => [
              'interval' => 4,
              'which' => '',
              'day' => '',
              'byday' => ['TU'],
              'byhour' => [],
              'byminute' => [],
            ],
            'interval' => 4,
            'repeat-end' => 'UNTIL',
            'repeat-end-count' => '',
            'repeat-end-date' => '20120622T035959Z',
            '_original_delta' => 0,
            'rrule' => '1',
            'rrule_index' => 2,
          ],
          [
            'value' => 1336482000,
            'end_value' => 1336532400,
            'duration' => 840,
            'timezone' => 'America/Toronto',
            'repeat' => 'WEEKLY',
            'repeat-advanced' => [
              'interval' => 4,
              'which' => '',
              'day' => '',
              'byday' => ['TU'],
              'byhour' => [],
              'byminute' => [],
            ],
            'interval' => 4,
            'repeat-end' => 'UNTIL',
            'repeat-end-count' => '',
            'repeat-end-date' => '20120622T035959Z',
            '_original_delta' => 0,
            'rrule' => '1',
            'rrule_index' => 3,
          ],
          [
            'value' => 1338901200,
            'end_value' => 1338951600,
            'duration' => 840,
            'timezone' => 'America/Toronto',
            'repeat' => 'WEEKLY',
            'repeat-advanced' => [
              'interval' => 4,
              'which' => '',
              'day' => '',
              'byday' => ['TU'],
              'byhour' => [],
              'byminute' => [],
            ],
            'interval' => 4,
            'repeat-end' => 'UNTIL',
            'repeat-end-count' => '',
            'repeat-end-date' => '20120622T035959Z',
            '_original_delta' => 0,
            'rrule' => '1',
            'rrule_index' => 4,
          ],
          [
            'value' => 1332234000,
            'end_value' => 1332284400,
            'duration' => 840,
            'timezone' => 'America/Toronto',
          ],
          [
            'value' => 1334653200,
            'end_value' => 1334703600,
            'duration' => 840,
            'timezone' => 'America/Toronto',
          ],
          [
            'value' => 1337850000,
            'end_value' => 1337900400,
            'duration' => 840,
            'timezone' => 'America/Toronto',
          ],
          [
            'value' => 1340269200,
            'end_value' => 1340319600,
            'duration' => 840,
            'timezone' => 'America/Toronto',
          ],
        ],
      ],
    ];
  }

}
