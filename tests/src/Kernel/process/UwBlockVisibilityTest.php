<?php

namespace Drupal\Tests\uw_migrate\Kernel\process;

use Drupal\KernelTests\KernelTestBase;
use Drupal\uw_migrate\Plugin\migrate\process\UwBlockVisibility;

/**
 * Tests the uw_block_visibility custom plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwBlockVisibility
 * @group uw_migrate
 */
class UwBlockVisibilityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate',
    'system',
  ];

  /**
   * The source row.
   *
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * The migrate mock object.
   *
   * @var \Drupal\migrate\MigrateExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $migrateExecutable;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();
    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Lookup for the existing term by name.
   *
   * @covers ::transform
   * @dataProvider providerTransform
   */
  public function testTransform($source, $expected) {
    $plugin = new UwBlockVisibility([], 'uw_block_visibility', []);
    $actual = $plugin->transform($source, $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertSame($expected, $actual);
  }

  /**
   * Provider for self::testTransform().
   */
  public function providerTransform() {
    return [
      [
        'news/*',
        [
          'request_path' => [
            'id' => 'request_path',
            'negate' => FALSE,
            'pages' => '/news/*',
          ],
        ],
      ],
      [
        "news/first\nnews/second",
        [
          'request_path' => [
            'id' => 'request_path',
            'negate' => FALSE,
            'pages' => "/news/first\n/news/second",
          ],
        ],
      ],
      [
        'not news/*',
        [
          'request_path' => [
            'id' => 'request_path',
            'negate' => TRUE,
            'pages' => '/news/*',
          ],
        ],
      ],
      [
        "news/*\nnot /news/special",
        [
          'request_path' => [
            'id' => 'request_path',
            'negate' => FALSE,
            'pages' => '/news/*',
          ],
        ],
      ],
    ];
  }

}
