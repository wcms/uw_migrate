<?php

namespace Drupal\Tests\uw_migrate\Unit;

use Drupal\uw_migrate\ContentTransformers\ExpandCollapseTransformer;
use Drupal\uw_migrate\UwContentParser;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Drupal\uw_migrate\UwContentParser
 * @group uw_migrate
 */
class UwMigrateContentParserTest extends TestCase {

  /**
   * Test isEmbeddedTag method.
   *
   * @covers::isEmbeddedTag
   */
  public function testIsEmbeddedTag() {
    $parser = new UwContentParser();
    $this->assertTrue($parser->isEmbeddedTag('<ckcalltoaction data-calltoaction-nid="123"/>'));
    $this->assertFalse($parser->isEmbeddedTag('<div>Text</div>'));
  }

  /**
   * @covers ::getContentSections
   * @dataProvider providerGetContentSections
   */
  public function testGetContentSections($content, array $expected, array $expected_all) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getContentSections($content, TRUE, FALSE));
    $this->assertSame($expected_all, $parser->getContentSections($content));
  }

  /**
   * Provider for self::testGetContentSections().
   */
  public function providerGetContentSections() {
    return [
      [
        '<div>Text</div>',
        ['<div>Text</div>'],
        ['<div>Text</div>'],
      ],
      [
        '<p><ckcalltoaction data-calltoaction-nid="906"/></p>',
        [],
        ['<ckcalltoaction data-calltoaction-nid="906"></ckcalltoaction>'],
      ],
      [
        '<a href="https://example.com">Test link</a>',
        ['<a href="https://example.com">Test link</a>'],
        ['<a href="https://example.com">Test link</a>'],
      ],
      [
        '<div><p>text</p></div><div><uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/></div>
         <ckcalltoaction data-calltoaction-nid="123"></ckcalltoaction>
         <ckcalltoaction data-calltoaction-nid="321"></ckcalltoaction>
         <p class="catchy-font">Extra text</p>
         <ckcalltoaction data-calltoaction-nid="567"></ckcalltoaction>
         Another text is here
         <blockquote><p>Quote text.</p></blockquote>',
        [
          '<div><p>text</p></div><div></div>',
          '<p class="catchy-font">Extra text</p>',
          'Another text is here',
        ],
        [
          '<div><p>text</p></div><div></div>',
          '<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"></uwvideo>',
          '<ckcalltoaction data-calltoaction-nid="123"></ckcalltoaction>',
          '<ckcalltoaction data-calltoaction-nid="321"></ckcalltoaction>',
          '<p class="catchy-font">Extra text</p>',
          '<ckcalltoaction data-calltoaction-nid="567"></ckcalltoaction>',
          'Another text is here',
          '<blockquote><p>Quote text.</p></blockquote>',
        ],
      ],
    ];
  }

  /**
   * @covers ::getHtmlSections
   * @dataProvider providerGetHtmlSections
   */
  public function testGetHtmlSections($content, array $expected, $regex = NULL) {
    $parser = new UwContentParser();
    $regex = $regex ?? $parser->getAllowedTags();
    $result = $parser->getHtmlSections($content, $regex);
    $this->assertSame($expected, $result);
  }

  /**
   * Provider for self::getHtmlSections().
   */
  public function providerGetHtmlSections() {
    return [
      [
        '<div><div class="col-50">some content</div><div class="col-50">another content here<div class="col-50"></div></div></div>',
        [
          '<div class="col-50">some content</div>',
          '<div class="col-50">another content here<div class="col-50"></div></div>',
        ],
      ],
      [
        '<div><p><ckcalltoaction data-calltoaction-nid="123"/></p></div>',
        ['<ckcalltoaction data-calltoaction-nid="123"></ckcalltoaction>'],
      ],
      [
        '<div><p>text</p></div><div></div>
         <ckcalltoaction data-calltoaction-nid="123"></ckcalltoaction>
         <ckcalltoaction data-calltoaction-nid="567"/>',
        [
          '<ckcalltoaction data-calltoaction-nid="123"></ckcalltoaction>',
          '<ckcalltoaction data-calltoaction-nid="567"></ckcalltoaction>',
        ],
      ],
      [
        '<test data-id="906"/>
         <cktimeline data-timeline-id="123" />
         <ckcalltoaction data-calltoaction-nid="123"/>
         <cktwitter value="text"></cktwitter>
         <ckcalltoaction data-calltoaction-nid="567"/>
         <uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>
         <blockquote><img src="test.png"><p>Some text.</p></blockquote>',
        [
          '<cktimeline data-timeline-id="123"></cktimeline>',
          '<ckcalltoaction data-calltoaction-nid="123"></ckcalltoaction>',
          '<cktwitter value="text"></cktwitter>',
          '<ckcalltoaction data-calltoaction-nid="567"></ckcalltoaction>',
          '<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"></uwvideo>',
          '<blockquote><img src="test.png"><p>Some text.</p></blockquote>',
        ],
      ],
    ];
  }

  /**
   * @covers ::getElementProperties
   * @dataProvider providerGetElementProperties
   */
  public function testGetElementProperties($content, $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getElementProperties($content));
  }

  /**
   * Provider for self::testGetElementProperties().
   */
  public function providerGetElementProperties() {
    return [
      [
        '<div><p><ckcalltoaction data-calltoaction-nid="123"/></p></div>',
        [
          'html_tag' => 'ckcalltoaction',
          'ref_id' => '123',
        ],
      ],
      ['<div><p>text</p></div><div></div>', []],
      [
        '<cktimeline data-timeline-nid="123" />',
        [
          'html_tag' => 'cktimeline',
          'ref_id' => '123',
        ],
      ],
      [
        '<ckcalltoaction data-timeline-id="123" />',
        ['html_tag' => 'ckcalltoaction'],
      ],
      [
        '<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>',
        [
          'html_tag' => 'uwvideo',
          'ref_id' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        ],
      ],
      [
        '<ckembeddedmaps data-height="500" data-src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d11584.51977733863!2d-80.44377735!3d43.45788424999999" data-type="googleMaps" />',
        [
          'html_tag' => 'ckembeddedmaps',
          'ref_id' => 'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d11584.51977733863!2d-80.44377735!3d43.45788424999999',
          'height' => '500',
        ],
      ],
    ];
  }

  /**
   * @covers ::getVideos
   * @dataProvider providerGetVideos
   */
  public function testGetVideos($content, $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getVideos($content));
  }

  /**
   * Provider for self::testGetVideos().
   */
  public function providerGetVideos() {
    return [
      [
        '<div><p><ckcalltoaction data-calltoaction-nid="123"/></p></div>',
        [],
      ],
      [
        '<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>',
        [
          [
            'html_tag' => 'uwvideo',
            'ref_id' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
          ],
        ],
      ],
      [
        '<ckvimeo data-url="https://vimeo.com/255086293" />
         <div>some markup</div>
         <div><uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/></div>
         <ckvimeo data-url="https://vimeo.com/259411563"></ckvimeo>',
        [
          [
            'html_tag' => 'ckvimeo',
            'ref_id' => 'https://vimeo.com/255086293',
          ],
          [
            'html_tag' => 'uwvideo',
            'ref_id' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
          ],
          [
            'html_tag' => 'ckvimeo',
            'ref_id' => 'https://vimeo.com/259411563',
          ],
        ],
      ],
    ];
  }

  /**
   * @covers ::getBlockquotes
   * @dataProvider providerGetBlockquotes
   */
  public function testGetBlockquotes($content, $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getBlockquotes($content));
  }

  /**
   * Provider for self::testGetBlockquotes().
   */
  public function providerGetBlockquotes() {
    return [
      [
        '<h2>Title</h2>
         <p>Some content</p>
         <blockquote>
           <p>Quote text.</p>
         </blockquote>',
        [
          [
            'content' => '<p>Quote text.</p>',
            'attribution' => '',
          ],
        ],
      ],
      [
        '<blockquote>
           <p>Quote text.</p>
           <footer><cite><strong>Name</strong>, Title</cite></footer>
         </blockquote>
         some text
         <blockquote>
           <p>Another quote</p>
         </blockquote>',
        [
          [
            'content' => '<p>Quote text.</p>',
            'attribution' => '<strong>Name</strong>, Title',
          ],
          [
            'content' => '<p>Another quote</p>',
            'attribution' => '',
          ],
        ],
      ],
    ];
  }

  /**
   * @covers ::getMaps
   */
  public function testGetMaps() {
    $parser = new UwContentParser();
    $original = '<ckembeddedmaps data-height="500" data-src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d11584.51977733863!2d-80.44377735!3d43.45788424999999" data-type="googleMaps" />';
    $expected = [
      [
        'html_tag' => 'ckembeddedmaps',
        'ref_id' => 'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d11584.51977733863!2d-80.44377735!3d43.45788424999999',
        'height' => '500',
      ],
    ];

    $this->assertSame($expected, $parser->getMaps($original));
  }

  /**
   * @covers ::getPowerBiTags
   */
  public function testGetPowerBiTags() {
    $parser = new UwContentParser();
    $original = '<ckpowerbi data-powerbi-id="https://app.powerbi.com/reportEmbed?reportId=135a5c3f-454c-4f1b-acb8-bf965952c9e1&amp;autoAuth=true&amp;ctid=723a5a87-f39a-4a22-9247-3fc240c01396" />';
    $expected = [
      [
        'html_tag' => 'ckpowerbi',
        'ref_id' => 'https://app.powerbi.com/reportEmbed?reportId=135a5c3f-454c-4f1b-acb8-bf965952c9e1&autoAuth=true&ctid=723a5a87-f39a-4a22-9247-3fc240c01396',
      ],
    ];
    $this->assertSame($expected, $parser->getPowerBiTags($original));
  }

  /**
   * @covers ::getMailmanTags
   */
  public function testGetMailmanTags() {
    $parser = new UwContentParser();
    $original = '<ckmailman data-listname="fakelist" data-servername="lists.uwaterloo.ca" /><ckmailman data-listname="another" data-servername="lists.uwaterloo.ca" />';
    $expected = [
      [
        'html_tag' => 'ckmailman',
        'ref_id' => 'fakelist',
        'server_name' => 'lists.uwaterloo.ca',
      ],
      [
        'html_tag' => 'ckmailman',
        'ref_id' => 'another',
        'server_name' => 'lists.uwaterloo.ca',
      ],
    ];
    $this->assertSame($expected, $parser->getMailmanTags($original));
  }

  /**
   * @covers ::getTwitterParts
   */
  public function testGetTwitterParts() {
    $parser = new UwContentParser();
    $tweet = 'https://twitter.com/name/status/1110073502101231';
    $expected = [
      'username' => 'name',
      'code' => '1110073502101231',
    ];
    $this->assertSame($expected, $parser->getTwitterParts($tweet));
  }

  /**
   * @covers ::clean
   * @dataProvider providerClean
   */
  public function testClean($content, $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->clean($content));
  }

  /**
   * Provider for self::testClean().
   */
  public function providerClean() {
    return [
      [
        '<div><p>text</p></div><div></div>',
        'text',
      ],
      [
        '<div><a href="#">links are kept</a></div><div>',
        '<a href="#">links are kept</a>',
      ],
      [
        '<div class="clearfix">&nbsp;</div>',
        '',
      ],
      [
        '<div class="clearfix"> </div>



    <div class="clearfix"> </div>',
        '',
      ],
    ];
  }

  /**
   * @covers ::getLayoutItems
   * @dataProvider providerGetLayoutItems
   */
  public function testGetLayoutItems($content, $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getLayoutItems($content));
  }

  /**
   * Provider for self::testGetLayoutItems().
   */
  public function providerGetLayoutItems() {
    return [
      [
        '<div><p>text</p></div><div></div>',
        [
          [
            'layout' => 'uw_1_column',
            'layout_settings' => [],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<div><p>text</p></div><div></div>',
              ],
            ],
          ],
        ],
      ],
      [
        '<p>Some 100% width markup before the 2-cols layout</p>
         <div class="field-item even" property="content:encoded">
           <h2>How-to video topics</h2>
           <div class="col-50 first">
             <h3 class="no-border">Create content types</h3>
           </div>
           <div class="col-50">
             <h3 class="no-border">Insert/upload</h3>
           </div>
           <div class="clearfix">&nbsp;</div>
           <div class="col-33">
             <h3 class="no-border">Edit an image</h3>
           </div>
           <div class="col-66">
             <h3>Drupal (WCMS) development</h3>
           </div>
           <p>Some text between layout sections</p>
           <div class="threecol-33">First column</div>
         </div>',
        [
          [
            'layout' => 'uw_1_column',
            'layout_settings' => [],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p>Some 100% width markup before the 2-cols layout</p>
         <div class="field-item even" property="content:encoded">
           <h2>How-to video topics</h2></div>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<h3 class="no-border">Create content types</h3>',
              ],
              [
                'region' => 'second',
                'content' => '<h3 class="no-border">Insert/upload</h3>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'larger-right',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<h3 class="no-border">Edit an image</h3>',
              ],
              [
                'region' => 'second',
                'content' => '<h3>Drupal (WCMS) development</h3>',
              ],
            ],
          ],
          [
            'layout' => 'uw_1_column',
            'layout_settings' => [],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p>Some text between layout sections</p>',
              ],
            ],
          ],
          [
            'layout' => 'uw_3_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => 'First column',
              ],
            ],
          ],
        ],
      ],
      [
        '<p>Starting content</p>
         <div class="col-50">some content here</div>
         <div class="col-50">
            <h4>Title here</h4>
            <div class="col-50">sub content</div>
            <p>Another content here</p>
            <div class="col-50">
                <div class="col-50">sub content</div>
            </div>
            <p>test 123</p>
         </div>
         <div class="col-33">
            <h4>Content here</h4>
            <div class="col-50">sub content</div>
            <p>Another content here</p>
            <div class="col-50">
                <div class="col-50">sub content</div>
            </div>
         </div>
        ',
        [
          [
            'layout' => 'uw_1_column',
            'layout_settings' => [],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p>Starting content</p>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => 'some content here',
              ],
              [
                'region' => 'second',
                'content' => '<h4>Title here</h4>            <div class="col-50">sub content</div>            <p>Another content here</p>            <div class="col-50">                <div class="col-50">sub content</div>            </div>            <p>test 123</p>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'larger-right',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<h4>Content here</h4>            <div class="col-50">sub content</div>            <p>Another content here</p>            <div class="col-50">                <div class="col-50">sub content</div>            </div>',
              ],
            ],
          ],
        ],
      ],
      [
        '<div class="threecol-33">
			<p class="last-visible">One fish, Two fish<br><img alt="Two fish stock photo. Image of animal, color, mirror, orange - 5087206" src="https://thumbs.dreamstime.com/b/two-fish-5087206.jpg" class="last-visible"></p>
		</div>

		<div class="threecol-33">
			<p class="last-visible">Red fish<br><img alt="Aquarium Fish Aggression | BeChewy" src="https://media-be.chewy.com/wp-content/uploads/betta-splendens-red.jpg" class="last-visible"></p>
		</div>

		<div class="threecol-33 last">
			<p class="last-visible">Blue fish<br><img alt="15 Stunning Blue Aquarium Fish Species | Build Your Aquarium" src="https://www.buildyouraquarium.com/wp-content/uploads/2020/04/blue-betta-fish.jpg" class="last-visible"></p>
		</div>

		<div class="clearfix last-visible">
			<div class="col-50 first">
				<p class="last-visible">Would you eat it on a log?<br><img alt=" log – WordReference Word of the Day" src="https://daily.wordreference.com/wp-content/uploads/2017/01/log.jpg" class="last-visible"></p>
			</div>

			<div class="col-50">
				<p class="last-visible">Would you eat it in a bog?<br><img alt="Bog - Wikipedia" src="https://upload.wikimedia.org/wikipedia/commons/e/e6/Lauhanvuori_suo.jpg" class="last-visible"></p>
			</div>

			<div class="clearfix last-visible">&nbsp;</div>
		</div>',
        [
          [
            'layout' => 'uw_3_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p class="last-visible">One fish, Two fish<br /><img alt="Two fish stock photo. Image of animal, color, mirror, orange - 5087206" src="https://thumbs.dreamstime.com/b/two-fish-5087206.jpg" class="last-visible" /></p>',
              ],
              [
                'region' => 'second',
                'content' => '<p class="last-visible">Red fish<br /><img alt="Aquarium Fish Aggression | BeChewy" src="https://media-be.chewy.com/wp-content/uploads/betta-splendens-red.jpg" class="last-visible" /></p>',
              ],
              [
                'region' => 'third',
                'content' => '<p class="last-visible">Blue fish<br /><img alt="15 Stunning Blue Aquarium Fish Species | Build Your Aquarium" src="https://www.buildyouraquarium.com/wp-content/uploads/2020/04/blue-betta-fish.jpg" class="last-visible" /></p>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p class="last-visible">Would you eat it on a log?<br /><img alt=" log – WordReference Word of the Day" src="https://daily.wordreference.com/wp-content/uploads/2017/01/log.jpg" class="last-visible" /></p>',
              ],
              [
                'region' => 'second',
                'content' => '<p class="last-visible">Would you eat it in a bog?<br /><img alt="Bog - Wikipedia" src="https://upload.wikimedia.org/wikipedia/commons/e/e6/Lauhanvuori_suo.jpg" class="last-visible" /></p>',
              ],
            ],
          ],
        ],
      ],
      [
        '
		<h3>One Author</h3>

		<div class="col-50 first">
			<p class="last-visible"><strong class="last-visible">Footnote or Endnote</strong></p>
		</div>

		<div class="col-50">
			<p class="last-visible"><strong class="last-visible">Corresponding Bibliography Entry</strong></p>
		</div>

		<div class="clearfix">
			<div class="col-50 first">
				<p><sup>1.</sup> First name Last name, <em class="last-visible">Title </em>(City: Publisher, Publication year), Page number.</p>

				<p class="last-visible"><sup>1. </sup>Ty Hawkins, <em class="last-visible">Reading Vietnam Amid the War on Terror </em>(New York: Palgrave Macmillan, 2012), 34.</p>
			</div>

			<div class="col-50">
				<p>Last name, First name. Title. City: Publisher, Publication year.</p>

				<p class="last-visible">Hawkins, Ty. Reading Vietnam Amid the War on Terror. New York: Palgrave Macmillan, 2012.</p>
			</div>
		</div>
		<h3><br class="last-visible">Multiple Authors</h3>
		<div class="col-50 first">
			<p class="last-visible"><strong class="last-visible">Footnote or Endnote</strong></p>
		</div>
		<div class="col-50">
			<p class="last-visible"><strong class="last-visible">Corresponding Bibliography Entry</strong></p>
		</div>
		<div class="col-50 first">
			<p><sup>5. </sup>First name Last name and First name Last name, <em class="last-visible">Title </em>(City: Publisher, Publication year), Page number.</p>

			<p class="last-visible"><sup>5. </sup>Rozsika Parker and Griselda Pollock, <em class="last-visible">Old Mistresses: Women, Art and Ideology </em>(New York: I.B. Tauris, 2013), 24.</p>
		</div>
		<div class="col-50">
			<p>Last name, First name, and First name Last name. <em class="last-visible">Title</em>. City: Publisher, Publication year.</p>

			<p class="last-visible">Parker, Rozsika, and Griselda Pollock. <em class="last-visible">Old Mistresses: Women, Art and Ideology</em>. New York: I.B. Tauris, 2013.</p>
		</div>
		',
        [
          // First group with 5 sections.
          [
            'layout' => 'uw_1_column',
            'layout_settings' => [],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<h3>One Author</h3>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p class="last-visible"><strong class="last-visible">Footnote or Endnote</strong></p>',
              ],
              [
                'region' => 'second',
                'content' => '<p class="last-visible"><strong class="last-visible">Corresponding Bibliography Entry</strong></p>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p><sup>1.</sup> First name Last name, <em class="last-visible">Title </em>(City: Publisher, Publication year), Page number.</p><p class="last-visible"><sup>1. </sup>Ty Hawkins, <em class="last-visible">Reading Vietnam Amid the War on Terror </em>(New York: Palgrave Macmillan, 2012), 34.</p>',
              ],
              [
                'region' => 'second',
                'content' => '<p>Last name, First name. Title. City: Publisher, Publication year.</p><p class="last-visible">Hawkins, Ty. Reading Vietnam Amid the War on Terror. New York: Palgrave Macmillan, 2012.</p>',
              ],
            ],
          ],
          // Second group with 5 sections.
          [
            'layout' => 'uw_1_column',
            'layout_settings' => [],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<h3><br class="last-visible" />Multiple Authors</h3>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p class="last-visible"><strong class="last-visible">Footnote or Endnote</strong></p>',
              ],
              [
                'region' => 'second',
                'content' => '<p class="last-visible"><strong class="last-visible">Corresponding Bibliography Entry</strong></p>',
              ],
            ],
          ],
          [
            'layout' => 'uw_2_column',
            'layout_settings' => [
              'column_class' => 'even-split',
            ],
            'regions' => [
              [
                'region' => 'first',
                'content' => '<p><sup>5. </sup>First name Last name and First name Last name, <em class="last-visible">Title </em>(City: Publisher, Publication year), Page number.</p><p class="last-visible"><sup>5. </sup>Rozsika Parker and Griselda Pollock, <em class="last-visible">Old Mistresses: Women, Art and Ideology </em>(New York: I.B. Tauris, 2013), 24.</p>',
              ],
              [
                'region' => 'second',
                'content' => '<p>Last name, First name, and First name Last name. <em class="last-visible">Title</em>. City: Publisher, Publication year.</p><p class="last-visible">Parker, Rozsika, and Griselda Pollock. <em class="last-visible">Old Mistresses: Women, Art and Ideology</em>. New York: I.B. Tauris, 2013.</p>',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @covers ::getImageGalleries
   */
  public function testGetImageGalleries() {
    $parser = new UwContentParser();
    $original = '<ckimagegallery data-gallerytype="Carousel" data-imagegallerynid="7" /><div>something<</div><ckimagegallery data-gallerytype="Thumbnails" data-imagegallerynid="10" />';
    $expected = [
      [
        'html_tag' => 'ckimagegallery',
        'ref_id' => '7',
        'gallery_type' => 'Carousel',
      ],
      [
        'html_tag' => 'ckimagegallery',
        'ref_id' => '10',
        'gallery_type' => 'Thumbnails',
      ],
    ];
    $this->assertSame($expected, $parser->getImageGalleries($original));
  }

  /**
   * Test expandable collapse items.
   *
   * @dataProvider providerExpandableItems
   */
  public function testGetExpandable($content, $expected_all, $content_expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected_all, $parser->getExpandCollapse($content));
    $this->assertSame($content_expected, $parser->getExpandCollapse($content, TRUE));
  }

  /**
   * Provider for self::getExpandCollapse().
   */
  public function providerExpandableItems() {
    return [
      [
        '<uw-ec-group data-group-index="0" title="Title expandable">Content of expandable...</uw-ec-group>
        <p>Anoher content here</p>
        <uw-ec-group title="Test adjacent 1" data-group-index="1">Content of first adjacent item</uw-ec-group>
        <uw-ec-group title="Test adjacent 2" data-group-index="1">Content of second adjacent item</uw-ec-group>
        <uw-ec-group title="Test adjacent 3" data-group-index="1">Content of third adjacent item</uw-ec-group>
        <h5>Anothe title here</h5>
        <div>Random content here</div>
        <uw-ec-group title="Last expandable item" data-group-index="2">Last expandable item.</uw-ec-group>
        <uw-ec-group title="Test with uwvideo embedded tags encapsulated" data-group-index="2">
            <uwvideo href="https://www.youtube.com/watch?v=Fxyoe6XfwnU"></uwvideo>
        </uw-ec-group>
        <uw-ec-group title="Test with blockquote embedded tags encapsulated" data-group-index="2">
            <blockquote>
                <p>We should never forget how lucky we were to have men like Professor Tutte in our darkest hour and the extent to which their work not only helped protect Britain itself but [saved] countless lives.</p>
                <footer>
                    <cite><strong>David Cameron</strong>, British Prime Minister, in a 2012 letter to Tutte’s remaining family in Newmarket, England</cite>
                </footer>
            </blockquote>
        </uw-ec-group>
        <uw-ec-group title="Timeline tag">
            <cktimeline data-timeline-nid="74"></cktimeline>
        </uw-ec-group>
        <uw-ec-group title="CTA" data-group-index="0">
          <ckcalltoaction data-calltoaction-nid="38"></ckcalltoaction>
        </uw-ec-group>
        <uw-ec-group title="Facts and Figures" data-group-index="0">
          <ckfactsfigures data-factsfigures-nid="23" data-numberpercarousel="3" data-usecarousel="Yes"></ckfactsfigures>
        </uw-ec-group>
        <uw-ec-group title="Image Gallery" data-group-index="0">
          <ckimagegallery data-gallerytype="Carousel" data-imagegallerynid="52"></ckimagegallery>
        </uw-ec-group>
        <uw-ec-group title="Maps" data-group-index="0">
          <ckembeddedmaps data-height="300" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2895.3996534802636!2d-80.541687!3d43.4731293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882bf401ad6dac05%3A0xbc0d83244f312792!2sUniversity%20of%20Waterloo!5e0!3m2!1sen!2sca!4v1669757034509!5m2!1sen!2sca" data-type="googleMaps"></ckembeddedmaps>
        </uw-ec-group>
        ',
        [
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'title' => 'Title expandable',
            'value' => 'Content of expandable...',
            'group' => 0,
            'has_embedded_tag' => FALSE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'title' => 'Test adjacent 1',
            'value' => 'Content of first adjacent item',
            'group' => 1,
            'has_embedded_tag' => FALSE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'title' => 'Test adjacent 2',
            'value' => 'Content of second adjacent item',
            'group' => 1,
            'has_embedded_tag' => FALSE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'title' => 'Test adjacent 3',
            'value' => 'Content of third adjacent item',
            'group' => 1,
            'has_embedded_tag' => FALSE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'title' => 'Last expandable item',
            'value' => 'Last expandable item.',
            'group' => 2,
            'has_embedded_tag' => FALSE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'ref_id' => 'https://www.youtube.com/watch?v=Fxyoe6XfwnU',
            'title' => 'Test with uwvideo embedded tags encapsulated',
            'value' => '<uwvideo href="https://www.youtube.com/watch?v=Fxyoe6XfwnU"/>',
            'group' => 2,
            'has_embedded_tag' => TRUE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'title' => 'Test with blockquote embedded tags encapsulated',
            'value' => '<blockquote>
                <p>We should never forget how lucky we were to have men like Professor Tutte in our darkest hour and the extent to which their work not only helped protect Britain itself but [saved] countless lives.</p>
                <footer>
                    <cite><strong>David Cameron</strong>, British Prime Minister, in a 2012 letter to Tutte’s remaining family in Newmarket, England</cite>
                </footer>
            </blockquote>',
            'group' => 2,
            'has_embedded_tag' => TRUE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'ref_id' => '74',
            'title' => 'Timeline tag',
            'value' => '<cktimeline data-timeline-nid="74"/>',
            'group' => 0,
            'has_embedded_tag' => TRUE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'ref_id' => '38',
            'title' => 'CTA',
            'value' => '<ckcalltoaction data-calltoaction-nid="38"/>',
            'group' => 0,
            'has_embedded_tag' => TRUE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'ref_id' => '23',
            'title' => 'Facts and Figures',
            'value' => '<ckfactsfigures data-factsfigures-nid="23" data-numberpercarousel="3" data-usecarousel="Yes"/>',
            'group' => 0,
            'has_embedded_tag' => TRUE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'ref_id' => '52',
            'gallery_type' => 'Carousel',
            'title' => 'Image Gallery',
            'value' => '<ckimagegallery data-gallerytype="Carousel" data-imagegallerynid="52"/>',
            'group' => 0,
            'has_embedded_tag' => TRUE,
          ],
          [
            'html_tag' => ExpandCollapseTransformer::EC_GROUP_TAG,
            'ref_id' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2895.3996534802636!2d-80.541687!3d43.4731293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882bf401ad6dac05%3A0xbc0d83244f312792!2sUniversity%20of%20Waterloo!5e0!3m2!1sen!2sca!4v1669757034509!5m2!1sen!2sca',
            'height' => '300',
            'title' => 'Maps',
            'value' => '<ckembeddedmaps data-height="300" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2895.3996534802636!2d-80.541687!3d43.4731293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882bf401ad6dac05%3A0xbc0d83244f312792!2sUniversity%20of%20Waterloo!5e0!3m2!1sen!2sca!4v1669757034509!5m2!1sen!2sca" data-type="googleMaps"/>',
            'group' => 0,
            'has_embedded_tag' => TRUE,
          ],
        ],
        [
          'Content of expandable...',
          'Content of first adjacent item',
          'Content of second adjacent item',
          'Content of third adjacent item',
          'Last expandable item.',
          '<uwvideo href="https://www.youtube.com/watch?v=Fxyoe6XfwnU"/>',
          '<blockquote>
                <p>We should never forget how lucky we were to have men like Professor Tutte in our darkest hour and the extent to which their work not only helped protect Britain itself but [saved] countless lives.</p>
                <footer>
                    <cite><strong>David Cameron</strong>, British Prime Minister, in a 2012 letter to Tutte’s remaining family in Newmarket, England</cite>
                </footer>
            </blockquote>',
          '<cktimeline data-timeline-nid="74"/>',
          '<ckcalltoaction data-calltoaction-nid="38"/>',
          '<ckfactsfigures data-factsfigures-nid="23" data-numberpercarousel="3" data-usecarousel="Yes"/>',
          '<ckimagegallery data-gallerytype="Carousel" data-imagegallerynid="52"/>',
          '<ckembeddedmaps data-height="300" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2895.3996534802636!2d-80.541687!3d43.4731293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882bf401ad6dac05%3A0xbc0d83244f312792!2sUniversity%20of%20Waterloo!5e0!3m2!1sen!2sca!4v1669757034509!5m2!1sen!2sca" data-type="googleMaps"/>',
        ],
      ],
    ];
  }

}
