<?php

namespace Drupal\Tests\uw_migrate\Unit;

use Drupal\file\Upload\FileUploadHandler;
use Drupal\Tests\RandomGeneratorTrait;
use Drupal\uw_migrate\Helpers\UrlValidator;
use PHPUnit\Framework\TestCase;

/**
 * Test UrlValidator class.
 */
class UwUrlValidatorHelperTest extends TestCase {

  use RandomGeneratorTrait;

  /**
   * Test custom url validator.
   *
   * @covers \Drupal\uw_migrate\Helpers\UrlValidator
   * @dataProvider providerUrlsForTest
   */
  public function testExternalUrls($url, $expected) {
    $this->assertSame(UrlValidator::validateUrl($url), $expected);
  }

  /**
   * Provider array of urls to be tested.
   *
   * @return string[]
   *   Urls and expected result to be tested.
   */
  public function providerUrlsForTest() {
    return [
      [
        'teaching-tips/planning-courses/tips-teaching-asshttps://uwaterloo.ca/centre-for-teaching-excellence/teaching-assistant-checklististants/teaching-assistant',
        FALSE,
      ],
      [
        'https://uwaterloo.ca/centre-for-teaching-excellence/teaching-assistant-checklististants/teaching-assistant',
        TRUE,
      ],
      [
        'teaching-tips/planning-courses/tips-teaching-ass',
        TRUE,
      ],
      [
        'uwaterloo.ca/economics/current-graduate-students/ma-degree-requirements',
        FALSE,
      ],
      [
        'https://google.com',
        TRUE,
      ],
      [
        'https://google',
        TRUE,
      ],
      [
        'https:google.com',
        FALSE,
      ],
      [
        'https:google.com',
        FALSE,
      ],
      [
        'https://',
        FALSE,
      ],
      [
        'https://somehosthere.com/path broken',
        FALSE,
      ],
      [
        'https://somehosthere.com/path-ok?with-query invalid here',
        FALSE,
      ],
      [
        '<none>',
        TRUE,
      ],
      [
        '<front>',
        TRUE,
      ],
      [
        'sites/default/files/somefile.gif',
        TRUE,
      ],
      [
        'internal-path-with-query-string?some-param-with-url=https://google.com',
        TRUE,
      ],
      [
        'internal-path-with-query-string?some-param-with-url=https://broke valu as url.com',
        FALSE,
      ],
      [
        'https://example.com/sites/default/files/somefile.gif',
        TRUE,
      ],
      ...array_map(function ($ext) {
        return [
          $this->randomMachineName() . '.' . $ext,
          TRUE,
        ];
      }, explode(' ', FileUploadHandler::DEFAULT_EXTENSIONS)),
    ];
  }

}
