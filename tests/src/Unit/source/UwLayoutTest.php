<?php

namespace Drupal\Tests\uw_migrate\Unit\source;

use Drupal\uw_migrate\Plugin\migrate\source\UwLayout;
use Drupal\uw_migrate\UwContentParser;
use Drupal\uw_migrate\UwContentTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Cover if the layout structure will be created correctly.
 *
 * Considering columns and expand collapse items.
 *
 * @covers \Drupal\uw_migrate\Plugin\migrate\source\UwLayout
 * @group uw_migrate
 */
class UwLayoutTest extends TestCase {

  /**
   * Provider for testBuildLayout.
   *
   * @return array[]
   *   Array with fake rows and expected result.
   */
  public function providerTestBuildLayout() {
    // Simulate UwEmbeddedContent process calling UwContentTransform.
    $items = array_map(function ($page) {
      $page['body_value'] = (new UwContentTransformer())->transform($page['body_value']);
      return [
        'row' => $page,
        'expected' => $page['expected'],
      ];
    }, self::getRows());

    return [
      [
        array_column($items, 'row'),
        array_column($items, 'expected'),
      ],
    ];
  }

  /**
   * Test layout build structure.
   *
   * @dataProvider providerTestBuildLayout
   */
  public function testBuildLayout($content, $expected) {
    $rows = UwLayout::buildLayoutComponents(
      $content,
      'body_value',
      new UwContentParser(),
    );

    foreach ($expected as $expectedRow) {
      $entity_rows = array_filter($rows, function ($row) use ($expectedRow) {
        return $expectedRow[0]['entity_id'] == $row['entity_id'];
      });
      $this->assertCount(count($expectedRow), $entity_rows);
      foreach (array_values($entity_rows) as $index => $entity_row) {
        $this->assertEquals($expectedRow[$index]['page_section_delta'], $entity_row['page_section_delta'], 'Page section delta not matching');
        $this->assertEquals($expectedRow[$index]['component_delta'], $entity_row['component_delta'], 'component delta not matching');
        $this->assertEquals($expectedRow[$index]['component_section_delta'], $entity_row['component_section_delta'], 'component section delta not matching');
        $this->assertEquals($expectedRow[$index]['entity_id'], $entity_row['entity_id'], 'entity id wrong');
      }
    }
  }

  /**
   * Array of existing scenarios rows in migration content.
   *
   * @return array
   *   Array with fake rows but replicating real scenarios on migration.
   */
  protected static function getRows() {
    return [
      // Testing e/c at first position followed by simple content.
      [
        'entity_id' => '1',
        'bundle' => 'uw_web_page',
        'body_value' => '
          <div class="expandable">
            <h2>Title 1</h2>
            <div class="expandable-content">
              <p>Some content here</p>
            </div>
          </div>
          <div>Following the expandable content.</div>
        ',
        'expected' => [
          [
            'page_section_delta' => 0,
            'component_delta' => 0,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '1',
          ],
          [
            'page_section_delta' => 0,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'layout' => 'uw_1_column',
            'entity_id' => '1',
          ],
          [
            'page_section_delta' => 0,
            'component_delta' => 1,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '1',
          ],
        ],
      ],
      // Testing e/c at second position.
      [
        'entity_id' => '2',
        'bundle' => 'uw_web_page',
        'body_value' => '
          <div>Following the expandable content.</div>
          <div class="expandable">
            <h2>Title 1</h2>
            <div class="expandable-content">
              <p>Some content here</p>
            </div>
          </div>
        ',
        'expected' => [
          [
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '2',
          ],
          [
            'page_section_delta' => 1,
            'component_delta' => 1,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '2',
          ],
          [
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'layout' => 'uw_1_column',
            'entity_id' => '2',
          ],
        ],
      ],
      // Testing more than e/c group on the page.
      [
        'entity_id' => '3',
        'bundle' => 'uw_web_page',
        'body_value' => '
          <div>Following the expandable content.</div>
          <div class="expandable">
            <h2>Title 1</h2>
            <div class="expandable-content">
              <p>Some content here</p>
            </div>
          </div>
          <div>Content between two expand collapse items.</div>
          <div class="expandable">
            <h2>Title 2</h2>
            <div class="expandable-content">
              <p>Some content here</p>
            </div>
          </div>
        ',
        'expected' => [
          [
            'page_section_delta' => 2,
            'component_delta' => 0,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '3',
          ],
          [
            'page_section_delta' => 2,
            'component_delta' => 1,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '3',
          ],
          [
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'layout' => 'uw_1_column',
            'entity_id' => '3',
          ],
          [
            'page_section_delta' => 2,
            'component_delta' => 2,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '3',
          ],
          [
            'page_section_delta' => 2,
            'component_delta' => 3,
            'component_section_delta' => -1,
            'layout' => 'uw_1_column',
            'entity_id' => '3',
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'layout' => 'uw_1_column',
            'entity_id' => '3',
          ],
        ],
      ],
      // Testing e/c collapse with columns as content.
      [
        'entity_id' => '4',
        'bundle' => 'uw_web_page',
        'body_value' => '
          <div>Initial content</div>
          <div class="expandable">
            <h2>Title 1</h2>
            <div class="expandable-content">
              <div class="col-50">First row + First column.</div>
              <div class="col-50">First row + Second column.</div>
              <div class="threecol-33">second row + first column.</div>
              <div class="threecol-33">second row + second column.</div>
              <div class="threecol-33">second row + third column.</div>
            </div>
          </div>
          <div>Last content.</div>
        ',
        'expected' => [
          [
            'entity_id' => '4',
            'layout' => 'uw_1_column',
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => -1,
          ],
          [
            'entity_id' => '4',
            'layout' => 'uw_1_column',
            'page_section_delta' => 3,
            'component_delta' => 1,
            'component_section_delta' => -1,
          ],
          [
            'entity_id' => '4',
            'layout' => 'uw_2_column',
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => 0,
          ],
          [
            'entity_id' => '4',
            'layout' => 'uw_3_column',
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => 1,
          ],
          [
            'entity_id' => '4',
            'layout' => 'uw_1_column',
            'page_section_delta' => 3,
            'component_delta' => 2,
            'component_section_delta' => -1,
          ],
        ],
      ],
      // Testing a page with only e/c.
      [
        'entity_id' => '5',
        'bundle' => 'uw_web_page',
        'body_value' => '
        <div class="expandable">
          <h2>First expand.</h2>
          <div class="expandable-content">
            Content here
          </div>
        </div>
        <div class="expandable">
          <h2>Second expand.</h2>
          <div class="expandable-content">
            Content here
          </div>
        </div>
        <div class="expandable">
          <h2>Third expand.</h2>
          <div class="expandable-content">
            Content here
          </div>
        </div>
        ',
        'expected' => [
          [
            'entity_id' => '5',
            'layout' => 'uw_1_column',
            'page_section_delta' => 4,
            'component_delta' => 0,
            'component_section_delta' => -1,
          ],
          [
            'entity_id' => '5',
            'layout' => 'uw_1_column',
            'page_section_delta' => 0,
            'component_delta' => 0,
            'component_section_delta' => 0,
          ],
          [
            'entity_id' => '5',
            'layout' => 'uw_1_column',
            'page_section_delta' => 0,
            'component_delta' => 1,
            'component_section_delta' => 0,
          ],
          [
            'entity_id' => '5',
            'layout' => 'uw_1_column',
            'page_section_delta' => 0,
            'component_delta' => 2,
            'component_section_delta' => 0,
          ],
        ],
      ],
      // Testing layout with normal components.
      [
        'entity_id' => '6',
        'bundle' => 'uw_web_page',
        'body_value' => '
          <p>Starting with paragraph</p>
          <ul>
            <li>First li content</li>
            <li>Second li content</li>
          </ul>
          <div class="col-50">
            Content inside the first column.
          </div>
          <div class="col-50">
            Content inside the second column.
          </div>
          <p>Another paragraph</p>
          <div class="col-33">another content</div>
          <div class="col-66">another content 1</div>
        ',
        'expected' => [
          [
            'entity_id' => '6',
            'layout' => 'uw_1_column',
            'page_section_delta' => 5,
            'component_delta' => 0,
            'component_section_delta' => -1,
          ],
          [
            'entity_id' => '6',
            'layout' => 'uw_2_column',
            'page_section_delta' => 5,
            'component_delta' => 1,
            'component_section_delta' => -1,
          ],
          [
            'entity_id' => '6',
            'layout' => 'uw_1_column',
            'page_section_delta' => 5,
            'component_delta' => 2,
            'component_section_delta' => -1,
          ],
          [
            'entity_id' => '6',
            'layout' => 'uw_2_column',
            'page_section_delta' => 5,
            'component_delta' => 3,
            'component_section_delta' => -1,
          ],
        ],
      ],
    ];
  }

}
