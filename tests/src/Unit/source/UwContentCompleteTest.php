<?php

namespace Drupal\Tests\uw_migrate\Unit\source;

use Drupal\uw_migrate\Plugin\migrate\source\UwContentComplete;
use Drupal\uw_migrate\UwContentParser;
use Drupal\uw_migrate\UwContentTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Cover source content complete.
 *
 * Validate if sections + plus regions will be added in the correct position.
 *
 * @covers \Drupal\uw_migrate\Plugin\migrate\source\UwContentComplete
 * @group uw_migrate
 */
class UwContentCompleteTest extends TestCase {

  /**
   * Test if the content will be added in the correct section + region.
   *
   * @dataProvider providerContentSection
   */
  public function testContentSection($row, $expected) {
    $configuration = [
      'content_column' => 'body_value',
      'content_col' => 'body_value',
      'format_col' => '',
    ];
    $parser = new UwContentParser();

    $row['body_value'] = (new UwContentTransformer())->transform($row[$configuration['content_column']]);

    $result = UwContentComplete::buildContentLayoutRow(
      $row,
      $configuration,
      $parser,
    );

    $map = array_map(function ($item) {
      $data = [
        'page_section_delta' => $item['page_section_delta'],
        'component_delta' => $item['component_delta'],
        'component_section_delta' => $item['component_section_delta'],
        'region' => $item['region'],
        'layout' => $item['layout'],
        'section' => $item['section'],
      ];

      if (isset($item['group'])) {
        $data['group'] = $item['group'];
      }

      return $data;
    }, $result);

    $this->assertSame($expected, $map);
  }

  /**
   * Data provider for testContentSection.
   *
   * @return array[]
   *   Array with fake rows but replicating real scenarios on migration.
   */
  public function providerContentSection() {
    return [
      [
        [
          'entity_id' => 1,
          'body_value' => '
            <h2>Title x</h2>
            <p>Some paragraph</p>
            <div class="col-50">first column</div>
            <div class="col-50">second column</div>
            <p>Another paragraph</p>
            <div class="threecol-33">column 1/3</div>
            <div class="threecol-33">column 2/3</div>
            <div class="threecol-33">column 3/3</div>
            <p>last paragraph</p>
          ',
          'body_format' => NULL,
        ],
        [
          // First item.
          [
            'page_section_delta' => 0,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
          ],
          // Second section with 2 columns.
          [
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_2_column',
            'section' => 1,
          ],
          [
            'page_section_delta' => 1,
            'component_delta' => 1,
            'component_section_delta' => 0,
            'region' => 'second',
            'layout' => 'uw_2_column',
            'section' => 1,
          ],
          // Third columns with paragraph.
          [
            'page_section_delta' => 2,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 2,
          ],
          // Forth section with 3 columns.
          [
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_3_column',
            'section' => 3,
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 1,
            'component_section_delta' => 0,
            'region' => 'second',
            'layout' => 'uw_3_column',
            'section' => 3,
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 2,
            'component_section_delta' => 0,
            'region' => 'third',
            'layout' => 'uw_3_column',
            'section' => 3,
          ],
          // Last section.
          [
            'page_section_delta' => 4,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 4,
          ],
        ],
      ],
      [
        [
          'entity_id' => 2,
          'body_value' => '
            <div class="expandable">
              <h2>Expandable 1 - group 0</h2>
              <div class="expandable-content">
                ec - 0
              </div>
            </div>
            <div class="expandable">
              <h2>Expandable 2 - group 0</h2>
              <div class="expandable-content">
                ec - 1
              </div>
            </div>
            <div class="expandable">
              <h2>Expandable 3 - group 0</h2>
              <div class="expandable-content">
                ec - 3
              </div>
            </div>
            <p>some content</p>
            <div class="expandable">
              <h2>Expandable 4 - group 1</h2>
              <div class="expandable-content">
                ec - 3
              </div>
            </div>
            <p>another some content</p>
            <div class="expandable">
              <h2>Expandable 2 - group 0</h2>
              <div class="expandable-content">
                ec - 1
              </div>
            </div>
            <div class="expandable">
              <h2>Expandable 3 - group 0</h2>
              <div class="expandable-content">
                ec - 3
              </div>
            </div>
          ',
          'body_format' => NULL,
        ],
        [
          // Group of content.
          [
            'page_section_delta' => 0,
            'component_delta' => 0,
            'component_section_delta' => -1,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => -1,
          ],
          [
            'page_section_delta' => 0,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => 0,
          ],
          [
            'page_section_delta' => 0,
            'component_delta' => 1,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => 0,
          ],
          [
            'page_section_delta' => 0,
            'component_delta' => 2,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => 0,
          ],
          // Simple content.
          [
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 1,
          ],
          // Second group content.
          [
            'page_section_delta' => 2,
            'component_delta' => 0,
            'component_section_delta' => -1,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 2,
            'group' => -1,
          ],
          [
            'page_section_delta' => 2,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => 1,
          ],
          // Another content.
          [
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 3,
          ],
          // Third group of ec.
          [
            'page_section_delta' => 4,
            'component_delta' => 0,
            'component_section_delta' => -1,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 4,
            'group' => -1,
          ],
          [
            'page_section_delta' => 4,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => 2,
          ],
          [
            'page_section_delta' => 4,
            'component_delta' => 1,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => 2,
          ],
        ],
      ],
      [
        [
          'entity_id' => 4,
          'body_value' => '
            <h2>Title x</h2>
            <div class="col-50">first column</div>
            <div class="col-50">second column</div>
            <p>Another paragraph</p>
            <div class="expandable">
              <h2>Title of expandable</h2>
              <div class="expandable-content">
                <div class="threecol-33">column 1/3</div>
                <div class="threecol-33">column 2/3</div>
                <div class="threecol-33">column 3/3</div>
              </div>
            </div>
            <div class="expandable">
              <h2>Title of expandable</h2>
              <div class="expandable-content">
                content
              </div>
            </div>
          ',
          'body_format' => NULL,
        ],
        [
          [
            'page_section_delta' => 0,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
          ],
          [
            'page_section_delta' => 1,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_2_column',
            'section' => 1,
          ],
          [
            'page_section_delta' => 1,
            'component_delta' => 1,
            'component_section_delta' => 0,
            'region' => 'second',
            'layout' => 'uw_2_column',
            'section' => 1,
          ],
          [
            'page_section_delta' => 2,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 2,
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => -1,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 3,
            'group' => -1,
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_3_column',
            'section' => 0,
            'group' => 0,
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'second',
            'layout' => 'uw_3_column',
            'section' => 0,
            'group' => 0,
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 0,
            'component_section_delta' => 0,
            'region' => 'third',
            'layout' => 'uw_3_column',
            'section' => 0,
            'group' => 0,
          ],
          [
            'page_section_delta' => 3,
            'component_delta' => 1,
            'component_section_delta' => 0,
            'region' => 'first',
            'layout' => 'uw_1_column',
            'section' => 0,
            'group' => 0,
          ],
        ],
      ],
    ];
  }

}
