<?php

namespace Drupal\Tests\uw_migrate\Unit\process;

use Drupal\migrate\MigrateSkipProcessException;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use Drupal\uw_migrate\Plugin\migrate\process\UwSkipOnEmpty;

/**
 * Tests the uw_skip_on_empty custom process plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwSkipOnEmpty
 * @group uw_migrate
 */
class UwSkipOnEmptyTest extends MigrateProcessTestCase {

  /**
   * @covers ::process
   * @dataProvider providerTestProcessSkipsOnEmpty
   */
  public function testProcessSkipsOnEmpty($value) {
    $configuration['method'] = 'process';
    $this->expectException(MigrateSkipProcessException::class);
    (new UwSkipOnEmpty($configuration, 'uw_skip_on_empty', []))
      ->transform($value, $this->migrateExecutable, $this->row, 'destination_property');
  }

  /**
   * Provider for self::testProcessSkipsOnEmpty().
   */
  public function providerTestProcessSkipsOnEmpty() {
    return [
      [''],
      [FALSE],
      [0],
      [NULL],
      [['', FALSE, NULL]],
    ];
  }

  /**
   * @covers ::process
   * @dataProvider providerProcessNotEmpty
   */
  public function testProcessNotEmpty($value) {
    $configuration['method'] = 'process';
    $actual = (new UwSkipOnEmpty($configuration, 'uw_skip_on_empty', []))
      ->transform($value, $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertSame($value, $actual);
  }

  /**
   * Provider for self::testProcessNotEmpty().
   */
  public function providerProcessNotEmpty() {
    return [
      ['hello'],
      [123],
      [['', NULL, 456, FALSE]],
    ];
  }

}
