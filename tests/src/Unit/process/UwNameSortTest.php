<?php

namespace Drupal\Tests\uw_migrate\Unit\process;

use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use Drupal\uw_migrate\Plugin\migrate\process\UwNameSort;

/**
 * Test the uw_name_sort custom process plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwNameSort
 * @group uw_migrate
 */
class UwNameSortTest extends MigrateProcessTestCase {

  /**
   * Test name for sort purposes transformation.
   *
   * @dataProvider providerTestProcessNameSort
   */
  public function testProcessNameSort(string $input, string $expected) {
    $actual = (new UwNameSort([], 'uw_name_sort', []))
      ->transform($input, $this->migrateExecutable, $this->row, 'output');

    $this->assertSame($actual, $expected);
  }

  /**
   * Data provider for ::testProcessNameSort().
   */
  public function providerTestProcessNameSort(): array {
    return [
      [
        'input' => '',
        'expected' => '',
      ],
      [
        'input' => 'Madonna',
        'expected' => 'Madonna',
      ],
      [
        'input' => 'John Doe',
        'expected' => 'Doe, John',
      ],
      [
        'input' => 'Jack Ryan Jr.',
        'expected' => 'Ryan, Jack',
      ],
      [
        'input' => 'Mr. David Davis',
        'expected' => 'Davis, David',
      ],
      [
        'input' => 'Paul A. McBane',
        'expected' => 'McBane, Paul',
      ],
      [
        'input' => 'Arjun Daniel (on secondment)',
        'expected' => 'Daniel, Arjun',
      ],
      [
        'input' => 'De Lorenzo Y Gutierez, Mr. Juan Martinez (Martin) Jr.',
        'expected' => 'De Lorenzo Y Gutierez, Juan',
      ],
    ];
  }

}
