<?php

namespace Drupal\Tests\uw_migrate\Unit\process;

use Drupal\migrate\Row;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use Drupal\uw_migrate\Plugin\migrate\process\UwExtractEntityIdFromURI;

/**
 * Test extract nid process.
 */
class UwExtractEntityIdFromURITest extends MigrateProcessTestCase {

  /**
   * @covers ::process
   * @dataProvider providerUrisToTest
   */
  public function testTransform($value, $expected) {
    $configuration = [];
    $result = (new UwExtractEntityIdFromURI($configuration, 'uw_extract_entity_id_from_uri', []))
      ->transform($value, $this->migrateExecutable, new Row(['source' => $value]), 'redirect');
    $this->assertSame($result, $expected);
  }

  /**
   * Provider tests for testTransform.
   *
   * @return array
   *   Array of value to be tested abd expected result.
   */
  public function providerUrisToTest() {
    return [
      ['node/1', '1'],
      ['taxonomy/term/123', '123'],
      ['another-path', NULL],
      ['node/invalid-path', NULL],
    ];
  }

}
