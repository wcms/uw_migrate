<?php

namespace Drupal\Tests\uw_migrate\Unit\process;

use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use Drupal\uw_migrate\Plugin\migrate\process\UwMath;

/**
 * Tests the uw_math custom process plugin.
 *
 * @coversDefaultClass \Drupal\uw_migrate\Plugin\migrate\process\UwMath
 * @group uw_migrate
 */
class UwMathTest extends MigrateProcessTestCase {

  /**
   * Test uw_math transform method.
   *
   * @covers ::transform
   */
  public function testUwMath() {
    $value = 'Text [expression] \[x = {-a \pm \sqrt{b^2-4ac} \over 2a}\] Another (text) \({x = {-b \pm \over 2a}\)';
    $expected_value = 'Text [expression] <div><span class="math-tex">x = {-a \pm \sqrt{b^2-4ac} \over 2a}</span></div> Another (text) <span class="math-tex">{x = {-b \pm \over 2a}</span>';

    $plugin = new UwMath([], 'uw_math', []);
    $actual = $plugin->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');
    $this->assertSame($expected_value, $actual);
  }

}
