<?php

namespace Drupal\Tests\uw_migrate\Unit\ContentTransformers;

use PHPUnit\Framework\TestCase;
use Drupal\uw_migrate\ContentTransformers\TidyFormat;

/**
 * @coversDefaultClass \Drupal\uw_migrate\UwContentTransformer
 * @group uw_migrate
 */
class TidyFormatTest extends TestCase {

  /**
   * @covers ::tidyFormatHtml
   * @dataProvider providerTidyFormatHtml
   */
  public function testTidyFormatHtml($content, $expected) {
    $tidyFormat = new TidyFormat();
    $updatedHtml = $tidyFormat->transform($content);
    $this->assertSame($expected, $updatedHtml);
  }

  /**
   * Provider for self::testTidyFormatHtml().
   */
  public function providerTidyFormatHtml() {
    return [
      [
        <<<EOF
        <h3>Title</h3>
        <div class="col-50 first">
          <p><strong>Column 1 with quotes codes.</strong></p>
        </div>
        <div class="col-50">
          <p><strong>Column 2 with special code for ...</strong></p>
        </div>
        <div class="clearfix additional-class">
          <div class="col-50 first">
            <p>&ldquo;Content Column 1 inside clearfix.&rdquo; Here&rsquo;s is an expression containing apostrophe using rsquo code.</p>
          </div>
          <div class="col-50">
            <p>Content Column 2 inside clearfix&hellip;</p>
          </div>
          <div class="clearfix last-visible additional-class">&nbsp;</div>
        </div>
        EOF,
        <<<EOF
        <h3>Title</h3>
        <div class="col-50 first">
          <p><strong>Column 1 with quotes codes.</strong></p>
        </div>
        <div class="col-50">
          <p><strong>Column 2 with special code for ...</strong></p>
        </div>
        <div class="clearfix additional-class">
          <div class="col-50 first">
            <p>“Content Column 1 inside clearfix.” Here’s is an expression containing apostrophe using rsquo code.</p>
          </div>
          <div class="col-50">
            <p>Content Column 2 inside clearfix…</p>
          </div>
          <div class="clearfix last-visible additional-class">
            &nbsp;
          </div>
        </div>
        EOF,
      ],
      [
        <<<EOF
        <h3>Title</h3>
        <div class="col-50 first">
          <p><strong>Column 1 with special quotes.</strong></p>
        </div>
        <div class="col-50">
          <p><strong>Column 2 with special code for ...</strong></p>
        </div>
        <div class="clearfix additional-class">
          <div class="col-50 first">
            <p>“Content Column 1 inside clearfix.” Here’s is an expression containing apostrophe using rsquo code.</p>
          </div>
          <div class="col-50">
            <p>Content Column 2 containing Ellipsis…</p>
          </div>
        </div>
        EOF,
        <<<EOF
        <h3>Title</h3>
        <div class="col-50 first">
          <p><strong>Column 1 with special quotes.</strong></p>
        </div>
        <div class="col-50">
          <p><strong>Column 2 with special code for ...</strong></p>
        </div>
        <div class="clearfix additional-class">
          <div class="col-50 first">
            <p>“Content Column 1 inside clearfix.” Here’s is an expression containing apostrophe using rsquo code.</p>
          </div>
          <div class="col-50">
            <p>Content Column 2 containing Ellipsis…</p>
          </div>
        </div>
        EOF,
      ],
      [
        <<<EOF
        <h3>Test encoding of special codes</h3>
        <div>
          <p>&cent; &pound; &sect; &copy; &laquo; &raquo; &reg;</p>
        </div>
        <div>
          <p>&deg; &plusmn; &para; &middot; &frac12; &ndash; &mdash;</p>
        </div>
        <div>
          <p>&lsquo; &rsquo; &sbquo; &ldquo; &rdquo; &bdquo; &dagger; &Dagger;</p>
        </div>
        <div>
          <p>&bull; &hellip; &prime; &Prime; &euro; &trade; &asymp;</p>
        </div>
        EOF,
        <<<EOF
        <h3>Test encoding of special codes</h3>
        <div>
          <p>¢ £ § © « » ®</p>
        </div>
        <div>
          <p>° ± ¶ · ½ – —</p>
        </div>
        <div>
          <p>‘ ’ ‚ “ ” „ † ‡</p>
        </div>
        <div>
          <p>• … ′ ″ € ™ ≈</p>
        </div>
        EOF,
      ],
      [
        <<<EOF
        <h3>Test&nbsp;encoding when tag symbols < > are concatenated with "NBSP"</h3>
        <div><p>&lt;&nbsp; test</p></div>
        <div><p>&gt;&nbsp; test</p></div>
        EOF,
        <<<EOF
        <h3>Test&nbsp;encoding when tag symbols &lt; &gt; are concatenated with "NBSP"</h3>
        <div>
          <p>&lt;&nbsp; test</p>
        </div>
        <div>
          <p>&gt;&nbsp; test</p>
        </div>
        EOF,
      ],
      [
        <<<EOF
        <h3>Test&nbsp;encoding when text and codes are concatenated with "NBSP"</h3>
        <p>&lsquo;&nbsp;&rsquo;&nbsp;&sbquo;&nbsp;&ldquo;&nbsp;&rdquo;&nbsp;&bdquo;&nbsp;&dagger;&nbsp;&Dagger;</p>
        EOF,
        <<<EOF
        <h3>Test&nbsp;encoding when text and codes are concatenated with "NBSP"</h3>
        <p>‘&nbsp;’&nbsp;‚&nbsp;“&nbsp;”&nbsp;„&nbsp;†&nbsp;‡</p>
        EOF,
      ],
      [
        <<<EOF
        <h3>Test&nbsp;encoding when there is text separated by more than one "NBSP"</h3>
        <p>Testing&nbsp;&nbsp;&nbsp;&nbsp;string</p>
        EOF,
        <<<EOF
        <h3>Test&nbsp;encoding when there is text separated by more than one "NBSP"</h3>
        <p>Testing&nbsp;&nbsp;&nbsp;&nbsp;string</p>
        EOF,
      ],
      [
        <<<EOF
        <h3>Test encoding when there is text right after the open tag symbol &lt;</h3>
        <p>&lt;test string</p>
        EOF,
        <<<EOF
        <h3>Test encoding when there is text right after the open tag symbol &lt;</h3>
        <p>&lt;test string</p>
        EOF,
      ],
    ];
  }

}
