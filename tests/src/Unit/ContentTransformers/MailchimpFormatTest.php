<?php

namespace Drupal\Tests\uw_migrate\Unit\ContentTransformers;

use PHPUnit\Framework\TestCase;
use Drupal\uw_migrate\ContentTransformers\MailchimpFormat;

/**
 * @coversDefaultClass \Drupal\uw_migrate\UwContentTransformer
 * @group uw_migrate
 */
class MailchimpFormatTest extends TestCase {

  /**
   * @covers ::mailchimpFormatHtml
   * @dataProvider providerMailchimpFormatHtml
   */
  public function testMailchimpFormatHtml($content, $expected) {
    // Transform content to mailchimp format.
    $mailchimpFormat = new MailchimpFormat();
    $updatedHtml = $mailchimpFormat->transform($content);

    // Prepare to compare.
    $expectedDom = new \DOMDocument();
    $expectedDom->loadHTML($expected);
    $xpath = new \DOMXPath($expectedDom);
    $expectedBody = $xpath->query('//body')->item(0)->nodeValue;

    $actualDom = new \DOMDocument();
    $actualDom->loadHTML($updatedHtml);
    $xpath = new \DOMXPath($actualDom);
    $actualBody = $xpath->query('//body')->item(0)->nodeValue;

    $this->assertEquals($expectedBody, $actualBody);
  }

  /**
   * Provider for self::testMailchimpFormatHtml().
   */
  public function providerMailchimpFormatHtml() {
    return [
      [
        <<<EOF
        <div id=mc_embed_signup>
          <form action="https://uwaterloo.us17.list-manage.com/subscribe/post?u=dfe24b8b69028fca25a4a5bc5&amp;id=3309fe99a0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
            <div id=mc_embed_signup_scroll>
              <h2>Join our community</h2>
              <div class=indicates-required><span class=asterisk>*</span> indicates required</div>
              <div class=mc-field-group>
                <label for=mce-FNAME>First Name  <span class=asterisk>*</span></label>
                <input type=text value= name=FNAME class=required id=mce-FNAME>
              </div>
              <div class=mc-field-group>
                <label for=mce-LNAME>Last Name  <span class=asterisk>*</span></label>
                <input type=text value= name=LNAME class=required id=mce-LNAME>
              </div>
            </div>
            <div style=position: absolute; left: -5000px; aria-hidden=true><input type=text name=b_dfe24b8b69028fca25a4a5bc5_3309fe99a0 tabindex=-1 value=></div>
          </form>
        </div>
        EOF,
        <<<EOF
        <div id="mc_embed_signup">
          <form action="https://uwaterloo.us17.list-manage.com/subscribe/post?u=dfe24b8b69028fca25a4a5bc5&amp;id=3309fe99a0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
            <div id="mc_embed_signup_scroll">
              <h2>Join our community</h2>
              <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
              <div class="mc-field-group">
                <label for="mce-FNAME">First Name  <span class="asterisk">*</span></label>
                <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
              </div>
              <div class="mc-field-group">
                <label for="mce-LNAME">Last Name  <span class="asterisk">*</span></label>
                <input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
              </div>
            </div>
            <div style="position: absolute; left: -5000px;"aria-hidden="true"><input type="text" name="b_dfe24b8b69028fca25a4a5bc5_3309fe99a0" tabindex="-1" value=""></div>
          </form>
        </div>\n
        EOF
      ],
    ];
  }

}
