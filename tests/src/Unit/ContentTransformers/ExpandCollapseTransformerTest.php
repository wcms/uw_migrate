<?php

namespace Drupal\Tests\uw_migrate\Unit\ContentTransformers;

use Drupal\uw_migrate\ContentTransformers\ExpandCollapseTransformer;
use Drupal\uw_migrate\ContentTransformers\TidyFormat;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @coversDefaultClass \Drupal\uw_migrate\ContentTransformers\ExpandCollapseTransformer
 * @group uw_migrate
 */
class ExpandCollapseTransformerTest extends TestCase {

  /**
   * @covers ::htmlReplaceDom
   * @dataProvider providerTestTransform
   */
  public function testTransform($content, $expected) {
    $transformer = new ExpandCollapseTransformer();
    $replacements = $transformer->transform($content);

    $tidy = new \tidy();
    $tidy->parseString($expected, TidyFormat::TIDY_OPTIONS);
    $tidy->cleanRepair();
    $formatted_html = tidy_get_output($tidy);

    $tidy = new \tidy();
    $tidy->parseString($replacements, TidyFormat::TIDY_OPTIONS);
    $tidy->cleanRepair();
    $formatted_html1 = tidy_get_output($tidy);

    $this->assertSame($formatted_html, $formatted_html1);
  }

  /**
   * Provide for testTransform.
   *
   * @return array[]
   *   Array with HTML content and expected content after transform function.
   */
  public function providerTestTransform() {
    return [
      [
        <<<EOF
        <p>Some content here</p>
        <div class="col-50">
          another content here.
        </div>
        <div class="expandable">
          <h2>What is Lorem Ipsum?</h2>
          <div class="expandable-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
        </div>
        <hr>
        <p>End content</p>
        <div class="expandable">
          <h2>What is Lorem Ipsum 2?</h2>
          <div class="expandable-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 2</div>
        </div>
        EOF,
        <<<EOF
        <p>Some content here</p>
        <div class="col-50">
          another content here.
        </div>
        <uw-ec-group-block data-ec-section-index="0"><uw-ec-group title="What is Lorem Ipsum?" data-group-index="0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</uw-ec-group></uw-ec-group-block>
        <hr>
        <p>End content</p>
        <uw-ec-group-block data-ec-section-index="1"><uw-ec-group title="What is Lorem Ipsum 2?" data-group-index="1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 2</uw-ec-group></uw-ec-group-block>
        EOF,
      ],
      [
        <<<EOF
        <div class="expandable">
          <h2>What is Lorem Ipsum 1</h2>
          <div class="expandable-content">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            <div class="expandable">
              <h2>Nested expandable collapse</h2>
              <div class="expandable-content">
                Nested expandable collapse content
              </div>
            </div>
          </div>
        </div>
        <div class="expandable">
          <h2>What is Lorem Ipsum 2</h2>
          <div class="expandable-content">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
          </div>
        </div>
        <p>any content here to simulate two groups</p>
        <div class="expandable">
          <h2>What is Lorem Ipsum 3</h2>
          <div class="expandable-content">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
          </div>
        </div>
        EOF,
        <<<EOF
        <uw-ec-group-block data-ec-section-index="0"><uw-ec-group title="What is Lorem Ipsum 1" data-group-index="0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        <div class="expandable">
          <h2>Nested expandable collapse</h2>
          <div class="expandable-content">
            Nested expandable collapse content
          </div>
        </div></uw-ec-group><uw-ec-group title="What is Lorem Ipsum 2" data-group-index="0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</uw-ec-group></uw-ec-group-block>
        <p>any content here to simulate two groups</p>
        <uw-ec-group-block data-ec-section-index="1"><uw-ec-group title="What is Lorem Ipsum 3" data-group-index="1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</uw-ec-group></uw-ec-group-block>
        EOF,
      ],
      [
        <<<EOF
          <div class="">
              nothing to migrate
          </div>
        EOF,
        <<<EOF
          <div class="">
              nothing to migrate
          </div>
        EOF,
      ],
    ];
  }

  /**
   * Test title format.
   *
   * @dataProvider providerTestTitleFormat
   */
  public function testFormatTitle($title, $expected) {
    $transform = new ExpandCollapseTransformer();
    $this->assertSame($transform->formatTitle($title), $expected);
  }

  /**
   * Provider expand collapse title formats.
   *
   * @return array[]
   *   Array with title test scenarios.
   */
  public function providerTestTitleFormat() {
    return [
      [
        'Some title here',
        'Some title here',
      ],
      [
        'Title with quantity of chars great than 255. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab',
        'Title with quantity of chars great than 255. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
      ],
    ];
  }

  /**
   * Test expand collapse nested components were not created.
   *
   * @dataProvider providerIsNestedComponent
   */
  public function testIsNestedComponent($htmlContent, $expected) {
    $transform = new ExpandCollapseTransformer();
    $crawler = $this->helperHtmlToCrawler($htmlContent);
    $response = [];
    $crawler->filter('.' . ExpandCollapseTransformer::EXPAND_COLLAPSE_CLASS_NAME)
      ->each(function ($item) use (&$response, $transform) {
        $response[] = $transform->isNestedComponent($item);
      });
    $this->assertSame($expected, $response);
  }

  /**
   * Provider for testIsNestedComponent.
   *
   * @return array[]
   *   Array with html content and expected result.
   */
  public function providerIsNestedComponent() {
    return [
      [
        "<body><div class='expandable'>....</div></body>",
        [FALSE],
      ],
      [
        "<div class='expandable'><div class='expandable'>nested expandable</div></div>",
        [FALSE, TRUE],
      ],
    ];
  }

  /**
   * Test expand collapse components ajacent groups.
   *
   * @dataProvider providerHasAdjacent
   */
  public function testHasAdjacent($htmlContent, $expected) {
    $crawler = $this->helperHtmlToCrawler($htmlContent);
    $transform = new ExpandCollapseTransformer();
    $response = [];
    $crawler->filter('.' . ExpandCollapseTransformer::EXPAND_COLLAPSE_CLASS_NAME)
      ->each(function ($item) use (&$response, $transform) {
        $response[] = $transform->hasAdjacentComponent($item->nextAll());
      });
    $this->assertSame($expected, $response);
  }

  /**
   * Test provider fo adjacent expand collapse scenarios.
   *
   * @return array[]
   *   Array with content and expected result.
   */
  public function providerHasAdjacent() {
    return [
      [
        "<div class='expandable'>....</div>",
        [FALSE],
      ],
      [
        "<div class='expandable'>....</div><div class='expandable'>....</div><p>text</p><div class='expandable'>....</div>",
        [TRUE, FALSE, FALSE],
      ],
    ];
  }

  /**
   * Helper function to load html on crawler component.
   *
   * @param string $htmlContent
   *   Html content.
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   *   A crawler instance with html content loaded.
   */
  private function helperHtmlToCrawler(string $htmlContent) {
    $document = new \DOMDocument();
    @$document->loadHTML($htmlContent);
    return new Crawler($document);
  }

}
