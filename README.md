## Steps to run migrations locally

Migrations on Pantheon are done using [wcms-pantheon-scripts](https://git.uwaterloo.ca/wcms-automation/wcms-pantheon-scripts/).

1. Add to settings.php:
```
if (php_sapi_name() == 'cli') {
  ini_set('memory_limit', '1024M');
}

// Configuration of database to store migration source.
$databases['migrate']['default'] = $databases['default']['default'];
$databases['migrate']['default']['host'] = 'migrate';

// The location of the source files. Update with the URL of the source site:
$settings['uw_migrate_source'] = 'https://uwaterloo.ca/example';
// This can also be set to a path to migrate from a local site. This allows
migration of private files without configuring a login cookie.
// $settings['uw_migrate_source'] = '/var/www/docroot';

// Update with file path of source site:
$settings['uw_migrate_site_path'] = 'sites/ca.example/files';

// Login to the source site as an admin user and put the login cookie here.
// Without this, private files will not migrate. Note that the cookie name does
// not change until the site is re-installed. The value changes with every login
// and you must use a current one.
// $settings['uw_migrate_cookies'] = ['SSESSdcf053736230aae183b84256752dbde1'
 => 'mlTzHWV8pEEjuWEXFv6nj0njzfEEjqI9rny1Pt8fd6c'];
```

2. Create a migration source database and populate it
   from the database dump file.
	1. Get the database dump file: Run the db_dump job
	   in Jenkins or login to `wms-aux1` and as user `wcmsadmi` run `db-dump.php`
		 from `uw_wcms_tools`. For the Pool, use "staging". Put the
		 resulting `sqldump_filtered` file into your VM.
		 The filename of this will be referred to as `SQL_DUMP_FILE`.
	2. Run:
	```
	mysql -udrupal -pdrupal -hmigrate drupal < SQL_DUMP_FILE
	```

3. Run Drush commands using Drush 10:
```
drush en uw_migrate -y
drush mim --group=uw --continue-on-failure
```

4. Manually set the site name, colors, and other settings
  that do not get migrated.

## Commands

* `drush ms --group=uw`: Display available UW migrations.
* `drush mim --group=uw --continue-on-failure`: Run available UW migrations.
* `drush migrate:rollback --group=uw`: Undo a migration.
   This doesn't completely undo the migration. For a full test,
	 you need to reinstall the site or restore to a database
	 dump of the site before the migration.
* `drush mim uw_ct_site_footer --continue-on-failure`:
   Manually run one migration. This doesn't take into consideration
	 dependencies.
* `php run-tests.php http://nginx --suppress-deprecations --module uw_migrate`:
   Run the tests in uw_migrate.

## Tips
  * `skip_on_empty` process plugin [doesn't work correctly with multiple values]
	   (https://www.drupal.org/project/drupal/issues/2905929#comment-13537872).
    There is a workaround to use `single_value` plugin before `skip_on_empty`.
  * By default, `changed` property will be overridden when the migration is
    executed with `--update` flag (e.g. `drush mim uw_sidebar --update`).
    `preserve_changed` module was enabled to avoid this behavior.
