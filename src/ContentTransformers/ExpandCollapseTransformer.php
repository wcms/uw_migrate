<?php

namespace Drupal\uw_migrate\ContentTransformers;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Transformer to convert expandable components in to uw-ec-group-block.
 *
 * The following html will be transformed to special tag.
 *
 * <div class='expandable'>....</div>
 * The above component will be
 * <uw-ec-group-block group=0>
 *  <uw-ec-group title='Title of content'>content...</uw-ec-group>
 * </uw-ec-group-block>
 *
 * Also ajdacents expand collapses will be wrapped together in the group.
 * <div class='expandable'>first component</div>
 * <div class='expandable'>second component</div>
 * <uw-ec-group-block group=1>
 *  <uw-ec-group title='First component title'>...</uw-ec-group>
 *  <uw-ec-group title='Second component title'>...</uw-ec-group>
 * </uw-ec-group-block>
 */
class ExpandCollapseTransformer extends BaseContentTransformer {

  /**
   * Fallback content when the expandable collapse item does not have content.
   */
  const CONTENT_TO_BE_DEFINE_VALUE = 'Content to be defined';

  /**
   * Class name used on expand collapse html components.
   */
  const EXPAND_COLLAPSE_CLASS_NAME = 'expandable';

  /**
   * Class name used on expand collapse content.
   */
  const EXPAND_COLLAPSE_CONTENT_CLASS = 'expandable-content';

  /**
   * Class name used on expand collapse title element.
   */
  const EXPAND_COLLAPSE_TITLE_CLASS = 'h2';

  /**
   * Tag name for expand collapse group.
   */
  const EC_GROUP_TAG = 'uw-ec-group';

  /**
   * Tag name for expand collapse group block.
   */
  const EC_GROUP_BLOCK_TAG = 'uw-ec-group-block';

  /**
   * Dom Document instance to build the new HTML structure.
   *
   * @var \DOMDocument
   */
  protected $document;

  /**
   * Construct method.
   */
  public function __construct() {
    $this->document = new \DOMDocument();
  }

  /**
   * Expand collapse title must have less than 255 chars.
   *
   * @param string $title
   *   The title to be formatted.
   *
   * @return string
   *   Return the title formatted.
   */
  public function formatTitle(string $title): string {
    return substr($title, 0, 255);
  }

  /**
   * Strip left and right whitespace of the string.
   *
   * @param string $content
   *   String to be cleaned.
   *
   * @return string
   *   Return a string without whitespace in the end and beginning.
   */
  public function ecTrim($content): string {
    return rtrim(ltrim($content));
  }

  /**
   * Check if node are inside an expand collapse component.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $node
   *   The node to be validated.
   *
   * @return bool
   *   True if is inside a component. Otherwise, false.
   */
  public function isNestedComponent(Crawler $node): bool {
    return (bool) $node->parents()->closest('.' . self::EXPAND_COLLAPSE_CLASS_NAME);
  }

  /**
   * Check if the node has adjacent expand collapse component.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $node
   *   Node to be validated.
   *
   * @return bool
   *   True is has another expand collapse component. Otherwise, false.
   */
  public function hasAdjacentComponent(Crawler $node): bool {
    return $node->getNode(0) !== NULL && in_array(self::EXPAND_COLLAPSE_CLASS_NAME, explode(' ', $node->attr('class')));
  }

  /**
   * Remove "ghost" body tag returned by crawler instance.
   *
   * Symfony crawler wrap the content inside a body tag.
   * In our case we don't need this tag.
   * Just remove it.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *   The crawler instance.
   *
   * @return string
   *   The html content without the body tag.
   */
  public function removeBodyTag(Crawler $crawler): string {
    // Prepare html entities.
    $response = html_entity_decode($crawler->html());
    // Remove ghost body tag to crawler work better on filters.
    $response = str_replace('<body>', '', $response);
    return str_replace('</body>', '', $response);
  }

  /**
   * Replace e/c content to custom <uw-ec-group/> tag.
   *
   * Every tag html with class the expandable will be processed.
   * For each component found will be replaced by the custom tag <uw-ec-group/>.
   * The tag will have the attribute title with the text extracted from H2 tag.
   * Inside the tag all content extract from expandable-content.
   * Nested e/c component will not consider a new tag <uw-ec-group/>.
   * They will be kept as content on a/c component parent.
   *
   * @param string $htmlContent
   *   The HTML content.
   *
   * @return string
   *   The new html content with <uw-ec-group/> tags instead of div e/c content.
   */
  public function transform(string $htmlContent): string {

    if (empty($htmlContent)) {
      return $htmlContent;
    }

    @$this->document->loadHTML($htmlContent);
    $crawler = new Crawler($this->document);
    $items = $crawler->filter('.' . self::EXPAND_COLLAPSE_CLASS_NAME);
    // Check if any expandable exists on the html.
    // If not return the current html without changes.
    if ($items->count() === 0) {
      return $htmlContent;
    }

    $blockGroup = 0;
    $replacementsData = [];
    $items->each(function (Crawler $node) use (&$blockGroup, &$replacementsData) {
      // Check if is current e/c component is nested.
      // If yes do not import as new block.
      if ($this->isNestedComponent($node)) {
        return FALSE;
      }

      $ecTitle = $node->filter(self::EXPAND_COLLAPSE_TITLE_CLASS);
      $title = $ecTitle->count() > 0 ? $this->formatTitle($ecTitle->text(NULL, TRUE)) : NULL;
      // Do not migrate expandable components without title.
      if (empty($title)) {
        return FALSE;
      }

      $eContent = $node->filter('.' . self::EXPAND_COLLAPSE_CONTENT_CLASS);
      $replacements = [
        'title' => $title,
        'content' => $eContent->count() > 0 ? $this->ecTrim($eContent->html()) : self::CONTENT_TO_BE_DEFINE_VALUE,
        'group' => $blockGroup,
      ];

      $uwceTag = $this->createExpandCollapseTag($replacements);
      $replacementsData[$blockGroup][] = [
        $node,
        $uwceTag,
      ];

      $nextElement = $node->nextAll();
      // Check if the next element of the current element is not
      // e/c component and create a new section of blocks.
      if (!$this->hasAdjacentComponent($nextElement)) {
        $blockGroup++;
      }
    });

    // Iterate by the e/c tags and wrap than into e/c block tag.
    // It's necessary to group to put them in the correct order on the page.
    foreach ($replacementsData as $group => $ecItems) {
      $groupTag = $this->createExpandCollapseGroupTag($group);
      foreach ($ecItems as $ecItem) {
        $groupTag->appendChild($ecItem[1]);
      }
      $node = array_shift($ecItems);
      $node = current($node);
      $node->getNode(0)->parentNode->replaceChild($groupTag, $node->getNode(0));
      foreach ($ecItems as $ec) {
        $node = $ec[0];
        $node->getNode(0)->parentNode->removeChild($node->getNode(0));
      }
    }
    return parent::transform($this->removeBodyTag($crawler));
  }

  /**
   * Create expand collapse group tag based on parameters.
   *
   * @param array $ecData
   *   Array with expand collapse information.
   *
   * @return \DOMElement|false
   *   A new dom element create with success. Otherwise, false.
   */
  public function createExpandCollapseTag(array $ecData) {
    $uwceTag = $this->document->createElement(self::EC_GROUP_TAG, $ecData['content']);
    $uwceTag->setAttribute('title', $ecData['title']);
    $uwceTag->setAttribute('data-group-index', $ecData['group']);
    return $uwceTag;
  }

  /**
   * Create expand collapse group block tag.
   *
   * This tag wrap expand collapse items.
   *
   * @param int $group_index
   *   The group index on the page.
   *
   * @return \DOMElement
   *   The tag uw-ec-group-block with attributes.
   */
  public function createExpandCollapseGroupTag(int $group_index) {
    $uwecGroupBlock = $this->document->createElement(self::EC_GROUP_BLOCK_TAG, '');
    $uwecGroupBlock->setAttribute('data-ec-section-index', $group_index);
    return $uwecGroupBlock;
  }

}
