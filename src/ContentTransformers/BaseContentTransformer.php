<?php

namespace Drupal\uw_migrate\ContentTransformers;

/**
 * The base class for all Content Transformers.
 */
abstract class BaseContentTransformer implements ContentTransformerInterface {

  /**
   * The next transformer in the chain of commands.
   *
   * @var \Drupal\uw_migrate\ContentTransformers\ContentTransformerInterface
   */
  private $nextContentTransformer;

  /**
   * {@inheritdoc}
   */
  public function setNextTransformer(ContentTransformerInterface $transformer): ContentTransformerInterface {
    $this->nextContentTransformer = $transformer;

    // Returning a $transformer from here will let us link $transformer in a
    // convenient way like this:
    // $transformer1->setNext($transformer2)->setNext($transformer3);.
    return $transformer;
  }

  /**
   * {@inheritdoc}
   */
  public function transform(string $html): string {
    if ($this->nextContentTransformer) {
      return $this->nextContentTransformer->transform($html);
    }

    return $html;
  }

}
