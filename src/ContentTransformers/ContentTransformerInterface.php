<?php

namespace Drupal\uw_migrate\ContentTransformers;

/**
 * Interface for the Content Transformers.
 */
interface ContentTransformerInterface {

  /**
   * Sets the next transformer class to be used in the chain of commands.
   *
   * @param ContentTransformerInterface $transformer
   *   The next transformer in the chain of commands.
   *
   * @return ContentTransformerInterface
   *   The transformer being set as Next in the chain of commands.
   */
  public function setNextTransformer(ContentTransformerInterface $transformer): ContentTransformerInterface;

  /**
   * Transforms an HTML string.
   *
   * @param string $html
   *   The HTML string to be transformed.
   *
   * @return string|null
   *   The transformed/updated string.
   */
  public function transform(string $html): string;

}
