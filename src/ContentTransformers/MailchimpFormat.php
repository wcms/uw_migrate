<?php

namespace Drupal\uw_migrate\ContentTransformers;

/**
 * Transformer for format and fix the HTML content in mailchimp migration.
 */
class MailchimpFormat extends BaseContentTransformer {

  /**
   * {@inheritdoc}
   */
  public function transform(string $html): string {
    // Check value and style attributes.
    $html = preg_replace('/(<input[^>]+)(value= )(name=[^>]+>)/i', '$1value="" $3', $html);
    $html = preg_replace('/(<div[^>]+style=)([^>]+;)([^>]+>)/i', '$1"$2"$3', $html);

    $dom = new \DOMDocument();
    $dom->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

    $xpath = new \DOMXPath($dom);

    // Get all elements with an attribute.
    $elements = $xpath->query('//*[@*]');
    foreach ($elements as $element) {
      foreach ($element->attributes as $attribute) {
        $element->setAttribute($attribute->name, htmlspecialchars($attribute->value, ENT_QUOTES));
      }
    }

    $formatted_html = $dom->saveHTML();

    return parent::transform($formatted_html);
  }

}
