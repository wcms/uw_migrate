<?php

namespace Drupal\uw_migrate\ContentTransformers;

/**
 * Transformer responsible for formatting the HTML content using Tidy.
 *
 * As part of the transform encoding is also fixed.
 * Special characters are encoded properly and returned in UTF-8.
 * The remaining html entities in the formatted html are:
 * - &nbsp; (whitespace)
 * - &gt; (>)
 * - &lt; (<)
 */
class TidyFormat extends BaseContentTransformer {

  /**
   * See https://tidy.sourceforge.net/docs/quickref.html.
   */
  const TIDY_OPTIONS = [
    'output-xhtml' => TRUE,
    'char-encoding' => 'utf8',
    'show-body-only' => TRUE,
    'show-errors' => 0,
    'show-warnings' => 0,
    'wrap' => 0,
    'wrap-sections' => FALSE,
    // 2 is equivalent to 'auto', which seems to be ignored
    // by PHP-html-tidy extension
    'indent' => 2,
    'indent-spaces' => 2,
    'omit-optional-tags' => TRUE,
    'literal-attributes' => TRUE,
    'fix-backslash' => FALSE,
    'fix-bad-comments' => FALSE,
    // HTML5 workarounds.
    'preserve-entities' => TRUE,
    'quote-nbsp' => FALSE,
    'doctype' => 'omit',
    'new-blocklevel-tags' => '
      article,
      aside,
      canvas,
      dialog,
      embed,
      figcaption,
      figure,
      footer,
      header,
      hgroup,
      nav,
      output,
      progress,
      section,
      video,
      ckcalltoaction,
      ckcodepen,
      ckembeddedmaps,
      ckfacebook,
      ckfactsfigures,
      ckhootsuite,
      ckimagegallery,
      cklivestream,
      ckmailchimp,
      ckmailman,
      ckpowerbi,
      cktableau,
      cktimeline,
      cktint,
      cktwitter,
      ckvimeo,
      uwvideo,
      cksocialintents
    ',
    'new-inline-tags' => '
      em,
      sup,
      audio,
      bdi,
      command,
      datagrid,
      datalist,
      details,
      keygen,
      mark,
      meter,
      rp,
      rt,
      ruby,
      source,
      summary,
      time,
      track,
      wbr
    ',
  ];

  /**
   * {@inheritdoc}
   */
  public function transform(string $html): string {
    // Remove Narrow No-Break Space.
    $html = str_replace("\xE2\x80\xAF", '', $html);

    // Replace some special chars that are not working well.
    $html = $this->replaceSpecialChars($html);
    // Replace &lt; entities by a placeholder to avoid tidy stripping them
    // when there are consecutive &nbsp;.
    // e.g. "&lt;test" would be "<test></test>" after $tidy->parseString() and
    // removed after $tidy->cleanRepair() as it is not a valid tag.
    $html = str_replace('&lt;', '___LT___', $html);

    // Replaces HTML entities with their respective value in UTF-8.
    $html = html_entity_decode($html, ENT_NOQUOTES | ENT_XHTML, 'UTF-8');

    // Formats the html string using tidy.
    $tidy = new \tidy();
    $tidy->parseString($html, self::TIDY_OPTIONS);
    $tidy->cleanRepair();
    $formatted_html = tidy_get_output($tidy);

    // Restore entity codes.
    $formatted_html = $this->replaceSpecialChars($formatted_html, TRUE);

    return parent::transform($formatted_html);
  }

  /**
   * Replace some chars for new markers.
   *
   * @param string $html
   *   The HTML content to be changed.
   * @param bool $reverse
   *   Reverse to back the entity on the markers.
   *
   * @return string
   *   The HTML content with the chars changed.
   */
  protected function replaceSpecialChars($html, $reverse = FALSE) {
    $map = [
      ['___LT___', '&lt;'],
      ['___NBSP___', '&nbsp;'],
      ['___RDQUO___', '&rdquo;'],
      ['___LDQUO___', '&ldquo;'],
      ['___RSQUO___', '&rsquo;'],
    ];
    foreach ($map as $item) {
      [$to, $from] = !$reverse ? $item : array_reverse($item);
      $html = str_replace($from, $to, $html);
    }
    return $html;
  }

}
