<?php

namespace Drupal\uw_migrate;

use Drupal\uw_migrate\ContentTransformers\ExpandCollapseTransformer;
use Drupal\uw_migrate\ContentTransformers\TidyFormat;

/**
 * Transform the content before processing it for migration.
 */
class UwContentTransformer {

  /**
   * The chain of transform commands.
   *
   * @var \Drupal\uw_migrate\ContentTransformers\BaseContentTransformer
   */
  protected $transformersChain;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $tidyFormat = new TidyFormat();
    $expandCollapseTransform = new ExpandCollapseTransformer();

    $tidyFormat
      ->setNextTransformer($expandCollapseTransform);
    $this->transformersChain = $tidyFormat;

    // If you want to add extras transformers to the chain you will need to
    // 1. Create a new class extending BaseContentTransformer in
    // src/ContentTransformers
    // 2. Instantiate the new class above.
    // 3. Update the line $this->transformChain = $tidyFormat; to
    // $tidyFormat->setNextTransformer($newTransformer)->setNextTransformer($newTransformer2);
    // $this->transformChain = $tidyFormat;
    // Read more:
    // https://refactoring.guru/design-patterns/chain-of-responsibility/php/example
  }

  /**
   * Transforms the content before migration processing.
   *
   * @param string $content
   *   The content/HTML to be transformed.
   *
   * @return string
   *   The content transformed and ready to be processed.
   */
  public function transform(string $content) {
    return $this->transformersChain->transform($content);
  }

}
