<?php

namespace Drupal\uw_migrate\Helpers;

use Drupal\Component\Utility\UrlHelper;
use Drupal\file\Upload\FileUploadHandler;

/**
 * Helper class to validate urls.
 */
class UrlValidator {

  /**
   * Validate url checking for.
   *
   * Internal: validate if the path is valid.
   * Validate if it has scheme or host in an inappropriate position in the url.
   * Validate if it has unexpected dots.
   * Validate if it has query mall formed.
   *
   * External: validate scheme and host.
   *
   * @param string $uri
   *   The uri to be validated.
   *
   * @return bool
   *   True if uri is valid. Otherwise, false.
   */
  public static function validateUrl($uri) {
    if (UrlHelper::isExternal($uri)) {
      return self::validateExternalUri($uri);
    }
    return self::validateInternalUri($uri);
  }

  /**
   * Validate internal uri.
   *
   * The validation consist in parse the uri.
   * And for each peace of the parse validate in agree of requirents.
   *
   * Example:
   *  path: cannot have a schema or a dot without an extension.
   *  query: must be a valid query.
   *
   * @param string $uri
   *   The uri to be valid.
   *
   * @return bool
   *   True for valid uri. False otherwise.
   */
  public static function validateInternalUri($uri) {
    $uri_parts = parse_url($uri);
    $isValid = TRUE;
    foreach ($uri_parts as $part => $value) {
      $method = "validate" . ucfirst($part);
      if (method_exists(self::class, $method)) {
        if (!self::{$method}($value)) {
          return FALSE;
        }
      }
    }
    return $isValid;
  }

  /**
   * Validate external uri checking for schema and host.
   *
   * @param string $uri
   *   The uri to be validated.
   *
   * @return bool
   *   True for valid uri. False otherwise.
   */
  public static function validateExternalUri($uri) {
    $url = parse_url($uri);
    // Url that can not be parsed is already invalid.
    if (empty($url)) {
      return FALSE;
    }
    // Valid urls has scheme and host.
    $missingScheme = !array_key_exists('scheme', $url);
    $missingHost = !array_key_exists('host', $url);
    if (filter_var($uri, FILTER_VALIDATE_URL) === FALSE || $missingHost || $missingScheme) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Validate the path of url.
   *
   * Invalid uris will consider is has:
   * - Dots
   * - Schemes
   * - Dot without any file extensions.
   *
   * @param string $uri
   *   Uri with a path to be validated.
   *
   * @return bool
   *   True for valid paths. False otherwise.
   */
  public static function validatePath($uri) {
    // Paths cannot have dots.
    $cannotHaveADot = stripos($uri, '.') !== FALSE;
    // Paths cannot have schemes.
    $cannotHaveScheme = stripos($uri, 'http://') !== FALSE;
    $cannotHaveSchemes = stripos($uri, 'https://') !== FALSE;

    // Check if path is internal file.
    if ($cannotHaveADot) {
      // Validating file the same way that file.module does.
      // @see file_validate_extensions.
      $regex = '/\.(' . preg_replace('/ +/', '|', preg_quote(FileUploadHandler::DEFAULT_EXTENSIONS)) . ')$/i';
      $cannotHaveADot = !preg_match($regex, $uri);
    }

    // Return false if any criteria was not attended.
    if ($cannotHaveSchemes || $cannotHaveADot || $cannotHaveScheme) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Validate if the query is valid.
   *
   * @param string $query
   *   Query to be validated.
   *
   * @return bool
   *   True if query is valid. False otherwise.
   */
  public static function validateQuery($query) {
    $fullUri = 'http://example.com?' . $query;
    return filter_var($fullUri, FILTER_VALIDATE_URL, [FILTER_FLAG_QUERY_REQUIRED]) !== FALSE;
  }

}
