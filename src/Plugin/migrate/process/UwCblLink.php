<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Writes the configuration is the format expected.
 *
 * @MigrateProcessPlugin(
 *   id = "uw_cbl_link"
 * )
 */
class UwCblLink extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $configuration = [
      'heading_level' => 'h2',
      'links' => [
        'top_icon' => NULL,
        'link_style' => 'related',
        'items' => $value,
      ],
    ];

    return $configuration;
  }

}
