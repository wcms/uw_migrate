<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Site\Settings;
use Drupal\migrate\Plugin\migrate\process\Download;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

/**
 * Custom download plugin to pass cookies jar into HTTP client requests.
 *
 * @MigrateProcessPlugin(
 *   id = "uw_download"
 * )
 */
class UwDownload extends Download {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, FileSystemInterface $file_system, Client $http_client) {
    $cookies = Settings::get('uw_migrate_cookies', []);
    $source = Settings::get('uw_migrate_source', '');

    if (!empty($cookies) && !empty($source)) {
      $domain = parse_url($source, PHP_URL_HOST);
      $jar = CookieJar::fromArray($cookies, $domain);
      $configuration += [
        'guzzle_options' => [
          'cookies' => $jar,
        ],
      ];
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $file_system, $http_client);
  }

}
