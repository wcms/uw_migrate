<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\MigrationLookup;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Custom lookup plugin with a predefined list of content migrations.
 *
 * Example:
 *
 * @code
 * process:
 *   nid:
 *     plugin: uw_node_lookup
 *     source: source_value
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_node_lookup"
 * )
 */
class UwNodeLookup extends MigrationLookup {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, $migrate_lookup, $migrate_stub = NULL) {
    $configuration['migration'] = [
      'uw_ct_blog',
      'uw_ct_catalog_item',
      'uw_ct_contact',
      'uw_ct_event',
      'uw_ct_news_item',
      'uw_ct_opportunity',
      'uw_ct_profile',
      'uw_ct_service',
      'uw_ct_project',
      'uw_ct_site_footer',
      'uw_ct_web_page',
    ];
    $configuration['no_stub'] = TRUE;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $migrate_lookup, $migrate_stub);
  }

}
