<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\MigrationLookup;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\uw_migrate\UwMigrateTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom lookup plugin for embedded blocks.
 *
 * Generates automatically a list of embedded blocks migrations to avoid lots
 * of duplicated code in yml files. Accepts 'field' configuration property to
 * restrict the migrations list.
 *
 * Example:
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_block_lookup
 *     field: body
 *     source: source_value
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_block_lookup"
 * )
 */
class UwBlockLookup extends MigrationLookup {

  use UwMigrateTrait;

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('migrate.lookup'),
      $container->get('migrate.stub'),
      $container->get('plugin.manager.migration'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, $migrate_lookup, $migrate_stub = NULL, MigrationPluginManagerInterface $migrate_plugin_manager = NULL) {
    $this->migrationPluginManager = $migrate_plugin_manager;

    $migrations = [
      'uw_cbl_copy_text' => [
        'entity_id',
        'page_section_delta',
        'component_delta',
        'component_section_delta',
        'region',
      ],
      'uw_cbl_blockquote' => [
        'entity_id',
        'page_section_delta',
        'component_delta',
        'component_section_delta',
        'region',
      ],
      'uw_cbl_call_to_action' => [
        'entity_id', 'ref_id',
      ],
      'uw_cbl_mailchimp' => [
        'entity_id',
        'page_section_delta',
        'component_delta',
        'component_section_delta',
        'region',
      ],
      'uw_cbl_facts_and_figures' => [],
      'uw_cbl_image_gallery' => [
        'entity_id', 'ref_id',
      ],
      'uw_cbl_google_maps' => [
        'entity_id', 'ref_id',
      ],
      'uw_cbl_mailman' => [
        'entity_id', 'ref_id',
      ],
      'uw_cbl_powerbi' => [
        'entity_id', 'ref_id',
      ],
      'uw_cbl_remote_video' => [
        'entity_id', 'ref_id',
      ],
      'uw_cbl_tableau' => [
        'entity_id', 'ref_id',
      ],
      'uw_cbl_timeline' => [],
      'uw_field_body_no_summary_promo_item' => [
        'entity_id', 'node_pos_id', 'delta',
      ],
    ];

    foreach ($migrations as $migration_name => $keys) {
      // Restrict derived migration to provided field.
      if (!empty($configuration['field'])) {
        $migrations = $this->migrationPluginManager->createInstances([$migration_name]);
        if (count($migrations) > 1) {
          $migration_name = $migration_name . ':' . $configuration['field'];
        }
      }
      $configuration['migration'][] = $migration_name;

      if (!empty($keys)) {
        $configuration['source_ids'][$migration_name] = $keys;
      }
    }

    $configuration['no_stub'] = TRUE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $migrate_lookup, $migrate_stub);
  }

  /**
   * Gets this plugin's configuration.
   *
   * @return array
   *   An array of this plugin's configuration.
   */
  public function getConfiguration() {
    return $this->configuration;
  }

}
