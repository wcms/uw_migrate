<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\FileCopy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom file copy plugin to swap download plugin with a custom one.
 *
 * @MigrateProcessPlugin(
 *   id = "uw_file_copy"
 * )
 */
class UwFileCopy extends FileCopy {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('stream_wrapper_manager'),
      $container->get('file_system'),
      $container->get('plugin.manager.migrate.process')->createInstance('uw_download', $configuration)
    );
  }

}
