<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\MigrateStubInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Lookup expand collapse items by original nid.
 *
 * @MigrateProcessPlugin(
 *   id = "uw_migration_ec_lookup"
 * )
 */
class UwMigrationECLookup extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The migration to be executed.
   *
   * @var \Drupal\migrate\Plugin\MigrationInterface
   */
  protected $migration;

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * The migrate stub service.
   *
   * @var \Drupal\migrate\MigrateStubInterface
   */
  protected $migrateStub;

  /**
   * Constructs a MigrationLookup object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The Migration the plugin is being used in.
   * @param \Drupal\migrate\MigrateLookupInterface $migrate_lookup
   *   The migrate lookup service.
   * @param \Drupal\migrate\MigrateStubInterface $migrate_stub
   *   The migrate stub service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, MigrateLookupInterface $migrate_lookup, MigrateStubInterface $migrate_stub) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migration = $migration;
    $this->migrateLookup = $migrate_lookup;
    $this->migrateStub = $migrate_stub;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('migrate.lookup'),
      $container->get('migrate.stub')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $lookup_migration_id = 'uw_ct_expand_collapse_group';
    $lookup_value = $value;
    if (isset($this->configuration['source_ids'][$lookup_migration_id])) {
      $lookup_value = array_values($row->getMultiple($this->configuration['source_ids'][$lookup_migration_id]));
    }
    $lookup_value = (array) $lookup_value;

    // Find the original nid.
    [$entity_id, $entity_type] = $lookup_value;
    $resp = $this->migrateLookup->lookup($this->getMigrationNameFromEntityType($entity_type), [$entity_id]);
    if (!isset($resp[0]['nid'])) {
      return [];
    }
    $originalNid = $resp[0]['nid'];

    // Re-throw any PluginException as a MigrateException so the executable.
    // Can shut down the migration.
    try {
      $destination_id_array = $this->migrateLookup->lookup($lookup_migration_id, $lookup_value);
    }
    catch (PluginNotFoundException $e) {
      $destination_id_array = [];
    }
    catch (MigrateException $e) {
      throw $e;
    }
    catch (\Exception $e) {
      throw new MigrateException(sprintf('A %s was thrown while processing this migration lookup', gettype($e)), $e->getCode(), $e);
    }

    if (empty($destination_id_array)) {
      return [];
    }

    // Order e/c items.
    usort($destination_id_array, function ($a, $b) {
      return $a['nid'] > $b['nid'];
    });

    $data = [];
    $data[$originalNid] = [];
    $data[$originalNid][$row->getSourceProperty('page_section_delta')] = array_column($destination_id_array, 'nid');

    return $data;
  }

  /**
   * Return the migration name based on entity type.
   *
   * @param string $entity_type
   *   The entity type id.
   *
   * @return string
   *   The migration name.
   */
  protected function getMigrationNameFromEntityType($entity_type) {
    $map = [
      'uw_web_page' => 'uw_ct_web_page',
    ];
    return $map[$entity_type] ?? NULL;
  }

}
