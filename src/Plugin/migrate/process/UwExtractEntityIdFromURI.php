<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Process extract entity id from uri.
 *
 * Transform only canonical urls.
 * Example:
 *  node/1
 *  node/2
 *  taxonomy/term/1
 *
 * @code
 * process:
 *   redirect_source/path:
 *     plugin: uw_extract_entity_id_from_uri
 *     source:
 *       - redirect_source__path
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_extract_entity_id_from_uri"
 * )
 */
class UwExtractEntityIdFromURI extends ProcessPluginBase {

  use UwMigrateTrait;

  /**
   * Transforms value from node/1 to 1.
   *
   * Valid strings:
   *   /node/1
   *   /node/2
   *   /taxonomy/term/1
   * Invalid strings:
   *   /node/another-path
   *   /node/test
   *   /any-path-without-node-slash-number.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $entity_data = $this->extractEntityInfoFromUri($row->getSourceProperty('source'));
    if (!empty($entity_data)) {
      return $entity_data['id'];
    }
    return NULL;
  }

}
