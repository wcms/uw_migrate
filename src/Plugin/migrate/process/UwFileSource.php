<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Custom process plugin to contact (or not) file path pieces based on scheme.
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_file_source
 *     source:
 *       - source_base_path
 *       - value
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_file_source"
 * )
 */
class UwFileSource extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $file_uri = $row->getSourceProperty('uri');

    // Append "system/files" for private files.
    if (strpos($file_uri, 'private://') === 0) {
      array_splice($value, 1, 0, 'system/files');
      // Make sure there are no extra slashes.
      $value = array_map(static function ($item) {
        return trim($item, '/');
      }, $value);
    }
    return implode('/', $value);
  }

}
