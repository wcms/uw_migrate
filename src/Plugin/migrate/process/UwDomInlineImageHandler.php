<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\Html;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Takes inline images from rich text and makes them into media entities.
 *
 * Example:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: dom
 *       method: import
 *       source: 'body/0/value'
 *     -
 *       plugin: uw_dom_inline_image_handler
 *     -
 *       plugin: dom
 *       method: export
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_dom_inline_image_handler"
 * )
 */
class UwDomInlineImageHandler extends UwFileDomProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Initialize DOM handling on this value.
    $this->init($value, $destination_property);

    // Loop through images to make into file/media entities and replace.
    /** @var \DOMElement $html_node */
    foreach ($this->xpath->query('//img') as $html_node) {
      // Get attributes from the image.
      $image_src = $html_node->getAttribute('src');
      $file_uri = $this->getFileUri($image_src);
      $image = $this->loadFileByUri($file_uri);

      if ($image instanceof MediaInterface) {
        // Create a new DOM element for the image in the text.
        $new_node = $this->document->createElement('drupal-media', '');
        $image_classes = explode(' ', $html_node->getAttribute('class'));

        // Initial media attributes.
        $attributes = [
          'data-entity-uuid' => $image->uuid(),
          'data-entity-type' => 'media',
          'alt' => $html_node->getAttribute('alt'),
        ];

        // Determine and set image align and width settings based on the
        // available CSS classes in source <img> tag.
        $classes_to_attributes = [
          'data-width' => [
            'image-sidebar-220px-wide' => '220',
            'image-body-500px-wide' => '500',
            'image-wide-body-750px-wide' => '750',
          ],
          'data-align' => [
            'image-left' => 'left',
            'image-right' => 'right',
            'image-center' => 'center',
          ],
        ];

        foreach ($classes_to_attributes as $media_attribute => $class_mapping) {
          $intersect = array_intersect($image_classes, array_keys($class_mapping));
          foreach ($intersect as $image_class) {
            $attributes[$media_attribute] = $class_mapping[$image_class];
          }
        }

        // Image width and height should have higher priority.
        if ($width = $html_node->getAttribute('width')) {
          $attributes['data-width'] = $width;
        }
        if ($height = $html_node->getAttribute('height')) {
          $attributes['data-height'] = $height;
        }

        foreach ($attributes as $attr => $val) {
          if (empty($val)) {
            continue;
          }
          $dom_att = $this->document->createAttribute($attr);
          $dom_att->value = Html::escape($val);
          $new_node->appendChild($dom_att);
        }
        // Replace the <img> with <drupal-media>.
        $html_node->parentNode->replaceChild($new_node, $html_node);
      }
      elseif ($image instanceof FileInterface) {
        $html_node->setAttribute('src', $this->fileUrlGenerator->generateString($image->getFileUri()));
      }
    }

    return $this->document;
  }

}
