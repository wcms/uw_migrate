<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Custom process plugin to reformat math expression markup.
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_math
 *     source: value
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_math"
 * )
 */
class UwMath extends ProcessPluginBase {

  /**
   * Regular expression of math sections.
   *
   * Four "\" are required to match single "\".
   *
   * @see https://regex101.com/r/uCIOlf/1
   */
  const MATH_EXPRESSION = '/(\\\\[[].*\\\\[]]|\\\\[(].*\\\\[)])/';

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $value = preg_replace_callback(self::MATH_EXPRESSION, function ($matches) {
      return strtr($matches[0], [
        '\[' => '<div><span class="math-tex">\(',
        '\]' => '\)</span></div>',
        '\(' => '<span class="math-tex">\(',
        '\)' => '\)</span>',
      ]);
    }, $value);

    return $value;
  }

}
