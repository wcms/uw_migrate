<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\office_hours\Plugin\migrate\process\OfficeHoursField;

/**
 * Custom process plugin for Office Hours field value.
 *
 * Appends other field values (closed dates, hours changes) as office hours
 * exceptions.
 *
 * @MigrateProcessPlugin(
 *   id = "uw_office_hours"
 * )
 */
class UwOfficeHours extends OfficeHoursField {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $timezone = new \DateTimeZone('EST');
    // Input value consist from 3 sources fields: office hours, closed dates and
    // hours change.
    // Office hours is retrieved from original plugin.
    [$office_hours, $closed_dates, $changed_hours] = $value;

    $aggregated_value = parent::transform($office_hours, $migrate_executable, $row, $destination_property);

    // Append "Closed" dates to the list.
    if (!empty($closed_dates)) {
      foreach ($closed_dates as $closed_date_range) {
        $start_date = new \DateTime($closed_date_range['value'], $timezone);

        // If end date is missing, append a start date and move one.
        if (empty($closed_date_range['value2'])) {
          $aggregated_value[] = [
            'day' => $start_date->getTimestamp(),
            'starthours' => -1,
            'endhours' => -1,
          ];
        }
        else {
          $end_date = new \DateTime($closed_date_range['value2'], $timezone);
          // Define a period of specified dates and traverse through dates.
          foreach (new \DatePeriod($start_date, new \DateInterval('P1D'), $end_date) as $date) {
            $aggregated_value[] = [
              'day' => $date->getTimestamp(),
              'starthours' => -1,
              'endhours' => -1,
            ];
          }

          // "End date" is not included in the period by default - append it
          // manually.
          if ($start_date != $end_date) {
            $aggregated_value[] = [
              'day' => $end_date->getTimestamp(),
              'starthours' => -1,
              'endhours' => -1,
            ];
          }
        }
      }
    }

    // Append "Hours Change" to the list.
    if (!empty($changed_hours)) {
      foreach ($changed_hours as $changed_hour) {
        $start_date = new \DateTime($changed_hour['value'], $timezone);
        $end_date = new \DateTime($changed_hour['value2'], $timezone);

        $aggregated_value[] = [
          'day' => $start_date->getTimestamp(),
          'starthours' => $start_date->format('Hi'),
          'endhours' => $end_date->format('Hi'),
        ];
      }
    }

    return $aggregated_value;
  }

}
