<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Custom process plugin to get and match values from catalog field.
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_process_field_catalog_tabs_display
 *     source: value
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_process_field_catalog_tabs_display"
 * )
 */
class UwProcessCatalogTabsField extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Original values.
    $values = [
      1 => 'New',
      2 => 'Popular',
      3 => 'Category',
      4 => 'Audience',
      5 => 'Faculty',
    ];
    return $value['value'] ? $values[$value['value']] : NULL;
  }

}
