<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\Core\Database\Database;

/**
 * Custom process plugin to reformat math expression markup.
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_role
 *     source: value
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_role"
 * )
 */
class UwRole extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $role = NULL;
    $uid = $row->getSourceProperty('uid');

    // Match old rol id with new one.
    switch ($value) {
      case 3:
        $role = 'administrator';
        break;

      case 4:
        $role = 'uw_role_content_author';
        break;

      case 5:
        $role = 'uw_role_content_editor';
        break;

      case 6:
        $role = 'uw_role_site_manager';
        break;

      case 7:
        $role = 'uw_role_form_editor';
        break;

      case 8:
        $role = 'uw_role_form_results_access';
        break;

    }

    // Get D7 database connection.
    Database::setActiveConnection('migrate');
    $migrateDB = Database::getConnection();

    // Search role expiration in d7.
    $roleExpire = $migrateDB->select('role_expire', 'r')
      ->fields('r', ['rid', 'uid', 'expiry_timestamp'])
      ->condition('uid', $uid)
      ->condition('rid', $value)
      ->execute()->fetchObject();

    // Restore default database connection.
    Database::setActiveConnection();
    $database = Database::getConnection();

    // Validate if the role exist and has an expiration value.
    if ($roleExpire && $roleExpire->expiry_timestamp && $role) {
      // Create and expiration value or update it if already exist.
      $database->merge('role_expire')
        ->key([
          'uid' => $uid,
          'rid' => $role,
        ])
        ->fields([
          'expiry_timestamp' => $roleExpire->expiry_timestamp,
        ])->execute();
    }

    return $role;
  }

}
