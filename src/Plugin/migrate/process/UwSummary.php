<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\Html;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Custom process plugin to prepare the summary value from original body.
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_summary
 *     trimmed: true
 *     max_length: 200
 *     source: body
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_summary",
 *   handle_multiples = TRUE
 * )
 */
class UwSummary extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $new_value = $value[0] ?? $value;
    $trimmed = $this->configuration['trimmed'] ?? FALSE;

    // Define settings based on the type of summary.
    $format = 'uw_tf_standard';
    $max_length = NULL;
    if ($trimmed) {
      $format = 'plain_text';
      $max_length = $this->configuration['max_length'] ?? 160;
    }

    if (isset($new_value['summary']) && (string) $new_value['summary'] !== '') {
      $summary = $new_value['summary'];
    }
    else {
      $summary = $new_value['value'] ?? '';
    }

    // Remove extra spaces, if there are any.
    $summary = trim($summary);

    $filter_types_to_skip = [];
    if ($trimmed) {
      // Remove "filter_autop" filter to not append extra <p> tags.
      $filter_types_to_skip[] = 'filter_autop';
    }

    // Strip tags because trimmed version should not have any HTML markup.
    // Temporary replace <!--break--> string with custom tokens to preserve
    // teaser breaks (HTML comments are always stripped by default).
    // We replace it back after check_markup() call, because it can be stripped
    // there as well.
    $summary = str_replace('<!--break-->', '[uw-teaser-break]', $summary);
    if ($trimmed) {
      $summary = strip_tags($summary);
    }

    // Process text via configured text format filters to make sure the value
    // is safe and valid.
    $summary = (string) check_markup($summary, $format, '', $filter_types_to_skip);
    $summary = str_replace('[uw-teaser-break]', '<!--break-->', $summary);

    // Now retrieve the actual summary, trimmed if necessary.
    $summary = text_summary($summary, $format, $max_length);

    // Normalize HTML to fix unclosed tags.
    // Usually, it's done by text_summary(), but it exits earlier if the teaser
    // break is presented.
    $summary = Html::normalize($summary);

    return $summary;
  }

}
