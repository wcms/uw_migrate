<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\StaticMap;
use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Provides ready mapping between custom HTML tags and corresponding blocks.
 *
 * It only requires to specify a plugin and source property, mapping will be
 * added automatically. Example:
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_tags_map
 *     source: tag
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_tags_map"
 * )
 */
class UwTagsMap extends StaticMap {

  use UwMigrateTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration['map'] = $this->getTagsMapping();
  }

}
