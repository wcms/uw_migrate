<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\uw_migrate\Helpers\UrlValidator;
use Drupal\uw_migrate\UwMigrateTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Combines "d7_path_redirect" and "migration_lookup" plugins into one.
 *
 * @see \Drupal\redirect\Plugin\migrate\process\d7\PathRedirect
 * @see \Drupal\migrate\Plugin\migrate\process\MigrationLookup
 *
 * @MigrateProcessPlugin(
 *   id = "uw_migration_lookup_path"
 * )
 */
class UwMigrationLookupPath extends UwNodeLookup {

  use UwMigrateTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $migration);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['url'])) {
      // The following line is required due to a bug in phpcs.
      // phpcs:ignore DrupalPractice.CodeAnalysis.VariableAnalysis.UnusedVariable
      $uri = &$value['url'];
    }
    elseif (is_array($value) && isset($value[0])) {
      // phpcs:ignore DrupalPractice.CodeAnalysis.VariableAnalysis.UnusedVariable
      $uri = &$value[0];
    }
    else {
      $uri = &$value;
    }

    // Check if the redirect value is valid.
    if (!UrlValidator::validateUrl($uri)) {
      // If the URL is no valid, we return NULL, so in this case, each migration
      // can handle this scenario individually.
      return NULL;
    }

    // Parse migration source settings, caching result.
    static $uw_migrate_settings;
    if (!isset($uw_migrate_settings)) {
      $uw_migrate_settings['source'] = parse_url(Settings::get('uw_migrate_source'));
      $uw_migrate_settings['source']['host'] = $uw_migrate_settings['source']['host'] ?? NULL;
      // Normalize by removing trailing slash, if any.
      $uw_migrate_settings['source']['path'] = $uw_migrate_settings['source']['path'] ?? NULL;
      $uw_migrate_settings['source']['path'] = rtrim($uw_migrate_settings['source']['path'], '/');

      $uw_migrate_settings['site_path'] = Settings::get('uw_migrate_site_path');
      // Normalize by removing leading and trailing slashes, if any.
      $uw_migrate_settings['site_path'] = trim($uw_migrate_settings['site_path'], '/');
    }

    $file = NULL;
    // If the URL is external and points to this site, convert to file or
    // internal path. The protocol and host are optional, allowing relative
    // URLs. If $uri does not match this site, leave it as-is.
    if (preg_match(',^((?:(?:https?:)?//' . $uw_migrate_settings['source']['host'] . ')?' . $uw_migrate_settings['source']['path'] . ')?/(' . $uw_migrate_settings['site_path'] . '/)?(.*)$,', $uri, $matches)) {
      $path = $matches[3];
      // Path is to a file if this hunk, "sites/.../files", matches this site.
      if ($matches[2]) {
        $file = $path;
      }
      // If not a file but the URL is this site, convert $uri to internal path.
      elseif ($matches[1]) {
        // When empty, the link is to the homepage.
        $uri = $path ?: '<front>';
      }
    }

    // If it is a file path, try to load a File object with it. If it works, set
    // $uri to the URL of that file.
    if ($file) {
      $file = $this->entityTypeManager
        ->getStorage('file')
        ->loadByProperties(['uri' => 'public://' . $file]);
      $file = array_pop($file);
      // Update $uri only if the file exists.
      if ($file instanceof FileInterface) {
        $uri = $file->createFileUrl();
      }
    }
    // Check if the url begins with http(s).
    // UrlHelper::isExternal returns FALSE for "uwaterloo.ca" url.
    elseif (!UrlHelper::isExternal($uri) && strpos($uri, '.') === FALSE) {
      // Mark link as internal.
      $uri = $uri ? 'internal:/' . ltrim($uri, '/') : 'route:<nolink>';

      // If the URL is canonical, then try to lookup in the existing migrations.
      if ($entityInfo = $this->extractEntityInfoFromUri($uri)) {
        $destination_id = $this->lookupPath($entityInfo, $migrate_executable, $row, $destination_property);
        if (!empty($destination_id)) {
          $uri = "internal:/{$entityInfo['entity_type']}/{$destination_id}";
        }
        else {
          // Destination node wasn't found, so the link is either broken or
          // outdated or linking to unpublished content that is not migrated.
          return NULL;
        }
      }

      // Convert entity URIs to the entity scheme, if the path matches a route
      // of the form "entity.$entity_type_id.canonical".
      // @see \Drupal\Core\Url::fromEntityUri()
      $url = Url::fromUri($uri);
      if ($url->isRouted()) {
        $route_name = $url->getRouteName();
        foreach (array_keys($this->entityTypeManager->getDefinitions()) as $entity_type_id) {
          if ($route_name === "entity.$entity_type_id.canonical" && isset($url->getRouteParameters()[$entity_type_id])) {
            $uri = "entity:$entity_type_id/" . $url->getRouteParameters()[$entity_type_id];
          }
        }
      }
      else {
        // Destination path wasn't found, so the path is either broken or
        // outdated or linking to unpublished content that is not migrated.
        return NULL;
      }

    }

    // Check if there are options.
    if (is_array($value) && !empty($value[1])) {
      // Check if there is a query.
      $options = unserialize($value[1]);
      if (!empty($options['query'])) {
        // Add it to the end of the url.
        $uri .= '?' . http_build_query($options['query']);
      }
      if (!empty($options['fragment'])) {
        $uri .= '#' . $options['fragment'];
      }
    }

    return $value;
  }

  /**
   * Do lookup migration based on entity type.
   *
   * @param array $entity_info
   *   Entity info to do the lookup.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   Migration executable.
   * @param \Drupal\migrate\Row $row
   *   Current row.
   * @param string $destination_property
   *   Destination property.
   *
   * @return string|null
   *   Return the destination id or null.
   */
  protected function lookupPath(array $entity_info, MigrateExecutableInterface $migrate_executable, Row $row, string $destination_property) {
    if ($entity_info['entity_type'] == 'node') {
      $destination_id = parent::transform($entity_info['id'], $migrate_executable, $row, $destination_property);
    }
    else {
      $destination_id = $this->migrateLookup->lookup(['uw_taxonomy_term'], [$entity_info['id']]);
      if (!empty($destination_id)) {
        $destination_id = current($destination_id)['tid'];
      }
    }
    return $destination_id;
  }

}
