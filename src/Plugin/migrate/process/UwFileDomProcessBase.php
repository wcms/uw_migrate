<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\file\FileInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\migrate_plus\Plugin\migrate\process\DomProcessBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom base class for file-to-media process plugins.
 */
abstract class UwFileDomProcessBase extends DomProcessBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * File url generator object.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, FileUrlGenerator $file_url_generator, FileUsageInterface $file_usage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileUrlGenerator = $file_url_generator;
    $this->fileUsage = $file_usage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('file_url_generator'),
      $container->get('file.usage')
    );
  }

  /**
   * Converts source file path into expected destination URI.
   *
   * @param string $src
   *   Original source file path.
   *
   * @return string
   *   The File URI to search in destination.
   */
  protected function getFileUri($src) {
    $uri = UrlHelper::parse($src)['path'];

    // Remove the source site from the path.
    $source = Settings::get('uw_migrate_source', '');
    // Remove base url if the path is absolute.
    $source = str_replace('https://uwaterloo.ca', '', $source);

    if (!empty($source)) {
      $uri = str_replace($source, '', $uri);
      // The image path usually starts from the /site-name base url.
      // It should be removed.
      $parts = parse_url($source);
      if (!empty($parts['path'])) {
        $uri = str_replace($parts['path'], '', $uri);
      }
    }

    // Remove site path.
    $uri = preg_replace('/.*sites\/.*\/files/U', '', $uri);
    // Remove image style path prefix.
    $uri = preg_replace('/.*styles\/.*\/public/U', '', $uri);
    // Replace /uploads/files for uploads/documents.
    $uri = preg_replace('/uploads\/files/', 'uploads/documents', $uri);
    // Trim remaining slashes and append scheme.
    $uri = 'public://' . urldecode(trim($uri, '/'));

    return $uri;
  }

  /**
   * Loads media or file entity by file URI.
   *
   * @param string $uri
   *   The File URI to search in destination.
   *
   * @return \Drupal\media\MediaInterface|\Drupal\file\FileInterface|false
   *   Media entity or FALSE if not found.
   */
  public function loadFileByUri($uri) {
    $files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $uri]);

    // Report the following cases:
    // - there are no files at all;
    // - there is more than 1 file.
    if (empty($files)) {
      // @todo Add log entry about not found file.
      return FALSE;
    }
    $file = current($files);

    // Get file usage to see if there is a media entity.
    // If there is a media, then return media entity.
    // if there is a file, then return the file.
    if ($file instanceof FileInterface) {
      $file_usages = $this->fileUsage->listUsage($file);
      if (!empty($file_usages['file']['media'])) {
        /** @var \Drupal\media\MediaInterface $media */
        $media = $this->entityTypeManager
          ->getStorage('media')
          ->load(key($file_usages['file']['media']));
        return !empty($media) ? $media : $file;
      }
      return $file;
    }
    return FALSE;
  }

}
