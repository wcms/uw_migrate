<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Core\Database\Database;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Process plugin to get the D7 pathauto setting to auto generate url.
 *
 * @MigrateProcessPlugin(
 *   id = "uw_migrate_pathauto_generate",
 * )
 */
class PathautoGenerate extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Query the db to get the pathauto_persist value.
    $db = Database::getConnection('default', 'migrate');
    $query = $db->select('url_alias', 'u')->fields('u');
    $query->condition('u.source', 'node/' . $value);
    $alias = $query->execute()->fetchObject();
    return $alias ? "/" . $alias->alias : NULL;
  }

}
