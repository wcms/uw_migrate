<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Converts D7 visibility rules into supported format.
 *
 * @MigrateProcessPlugin(
 *   id = "uw_block_visibility"
 * )
 */
class UwBlockVisibility extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $visibility = [];

    $source_paths = preg_split("(\r\n?|\n)", $value);
    $paths = $negate_paths = [];
    $negate = FALSE;

    foreach ($source_paths as $path) {
      if (strpos($path, 'not ') !== FALSE) {
        $path = str_replace('not ', '', $path);
        $negate_paths[] = $this->normalizePath($path);
      }
      else {
        $paths[] = $this->normalizePath($path);
      }
    }

    // Migrate negate paths only if there no other paths.
    if (!empty($negate_paths) && empty($paths)) {
      $paths = $negate_paths;
      $negate = TRUE;
    }
    elseif (!empty($negate_paths) && !empty($paths)) {
      // In this case, negate paths are ignored, so we need some log entry.
      (new MigrateMessage())->display($this->t('Some negate rules were ignored during Special alert migration: @rules.', [
        '@rules' => implode(',', $negate_paths),
      ]));
    }

    $visibility['request_path'] = [
      'id' => 'request_path',
      'negate' => $negate,
      'pages' => implode("\n", $paths),
    ];

    return $visibility;
  }

  /**
   * Helper method to clean-up and mormalize given path.
   */
  protected function normalizePath($path) {
    $path = trim($path);
    return $path === '<front>' ? $path : '/' . ltrim($path, '/');
  }

}
