<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\MigrationLookup;
use Drupal\migrate\Row;

/**
 * Custom lookup plugin for files in the webform submitted data.
 *
 * Example:
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_webform_file_lookup
 *     migration: uw_file
 *     source: webform_data
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_webform_file_lookup"
 * )
 */
class UwWebformFileLookup extends MigrationLookup {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // If no webform components found return value as is.
    if (!is_array($row->getSourceProperty('webform_components'))) {
      return $value;
    }

    $file_components = array_keys($row->getSourceProperty('webform_components'), 'file');

    // Lookup destination files in the specified migration.
    foreach ($file_components as $form_key) {
      $value[$form_key] = parent::transform($value[$form_key], $migrate_executable, $row, $destination_property);
    }
    return $value;
  }

}
