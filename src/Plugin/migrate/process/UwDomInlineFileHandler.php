<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\Html;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Updates the URL for inline links to files.
 *
 * Examples:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: dom
 *       method: import
 *       source: 'body/0/value'
 *     -
 *       plugin: uw_dom_inline_file_handler
 *     -
 *       plugin: dom
 *       method: export
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 * @see \Drupal\migrate_plus\Plugin\migrate\process\DomProcessBase
 *
 * @MigrateProcessPlugin(
 *   id = "uw_dom_inline_file_handler"
 * )
 */
class UwDomInlineFileHandler extends UwFileDomProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Get the destination site path from the pantheon_domain_masking module.
    $destination_site_path = $this->configFactory->get('pantheon_domain_masking.settings')->get('subpath');
    $destination_site_path = trim($destination_site_path, '/');

    $allowed_extensions = [
      'bib', 'csv', 'doc', 'docx', 'gz', 'key', 'm', 'mw', 'mp3', 'rtf', 'pdf',
      'pot', 'potx', 'pps', 'ppt', 'pptx', 'psd', 'r', 'tar', 'tex', 'txt',
      'wav', 'xls', 'xlsx', 'zip',
    ];

    // Initialize DOM handling on this value.
    $this->init($value, $destination_property);

    // Loop through images to make into file/media entities and replace.
    $links = $this->xpath->query('//a');
    foreach ($links as $html_node) {
      // Get href attribute from the <a>.
      $href = $html_node->getAttribute('href');

      // Try to retrieve file extension.
      $path_info = pathinfo($href);

      // Make sure the href is to a .pdf.
      if (empty($path_info['extension']) || !\in_array($path_info['extension'], $allowed_extensions, TRUE)) {
        continue;
      }

      $file_uri = $this->getFileUri($href);
      $file = $this->loadFileByUri($file_uri);

      // $file could be either MediaInterface or FileInterface. Load the file if
      // this is a MediaInterface.
      if ($file instanceof MediaInterface) {
        $file = $file->getSource()->getSourceFieldValue($file);
        $file = File::load($file);
      }

      // Do nothing if there is no file.
      if (!$file instanceof FileInterface) {
        continue;
      }

      // Create a new <a href> with an updated URL.
      $new_node = $this->document->createElement('a', $html_node->nodeValue);

      // Attributes for the 'a' element.
      $attributes = [
        // If a destination site path is configured, prepend it to the link.
        'href' => ($destination_site_path ? ('/' . $destination_site_path) : '') . $file->createFileUrl(),
      ];

      foreach ($attributes as $attr => $val) {
        if (empty($val)) {
          continue;
        }
        $dom_att = $this->document->createAttribute($attr);
        $dom_att->value = Html::escape($val);
        $new_node->appendChild($dom_att);
      }
      // Replace the old <a href> with the new.
      $html_node->parentNode->replaceChild($new_node, $html_node);
    }
    return $this->document;
  }

}
