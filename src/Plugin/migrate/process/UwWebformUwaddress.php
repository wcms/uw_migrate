<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\MigrationLookup;
use Drupal\migrate\Row;

/**
 * Custom plugin to migrate Webform component uwaddress.
 *
 * Example:
 *
 * @code
 * process:
 *   type:
 *     plugin: uw_webform_uwaddress
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_webform_uwaddress"
 * )
 */
class UwWebformUwaddress extends MigrationLookup {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    // If no webform components found return value as is.
    if (!is_array($row->getSourceProperty('webform_components'))) {
      return $value;
    }

    // Get form keys for all uwaddress components.
    $address_components = array_keys($row->getSourceProperty('webform_components'), 'uwaddress');

    // Map the address subcomponents to the keys used in Webform.
    $address_mapping = [
      'address' => 'addr_1',
      'address_2' => 'addr_2',
      'city' => 'city',
      'state_province' => 'province',
      'postal_code' => 'postal_code',
      'country' => 'country',
    ];
    foreach ($address_components as $form_key) {
      $new_value = [];
      foreach ($address_mapping as $to => $from) {
        $new_value[$to] = $value[$form_key][$from];
      }
      $value[$form_key] = $new_value;
    }

    return $value;
  }

}
