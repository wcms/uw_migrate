<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use ADCI\FullNameParser\Parser;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Transform full name to sort format (last_name, fist_name).
 *
 * @code
 * process:
 *   destination_field:
 *     plugin: uw_name_sort
 *     source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_name_sort"
 * )
 */
class UwNameSort extends ProcessPluginBase {

  /**
   * Full name parser.
   *
   * @var \ADCI\FullNameParser\Parser
   */
  protected Parser $parser;

  /**
   * Default constructor.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->parser = new Parser([
      'mandatory_first_name' => FALSE,
      'mandatory_last_name' => FALSE,
      'throws' => FALSE,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $name = $this->parser->parse($value);

    if ($last_name = $name->getLastName()) {
      $value = $last_name . ', ' . $name->getFirstName();
    }

    return $value;
  }

}
