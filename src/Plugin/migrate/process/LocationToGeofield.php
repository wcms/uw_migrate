<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\Core\Database\Driver\sqlite\Connection as SQLiteConnection;
use Drupal\geofield\WktGeneratorInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process plugin that converts D7 location field values to D8|D9 geofield.
 *
 * Taken from "Location Migration" module with some minor changes.
 *
 * @see https://www.drupal.org/project/location_migration.
 *
 * @MigrateProcessPlugin(
 *   id = "location_to_geofield",
 *   handle_multiples = TRUE
 * )
 */
class LocationToGeofield extends LocationProcessPluginBase {

  /**
   * The geographical coordinate value that should be considered as empty.
   *
   * @var string
   */
  const COORDINATE_EMPTY_VALUE = '0.000000';

  /**
   * The empty coordinate value on SQLite.
   *
   * @var string
   */
  const COORDINATE_EMPTY_VALUE_SQLITE = '0.0';

  /**
   * The WKT format Generator service.
   *
   * @var \Drupal\geofield\WktGeneratorInterface
   */
  protected $wktGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, WktGeneratorInterface $wkt_generator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->wktGenerator = $wkt_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('geofield.wkt_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($lids = $this->getLocationIds($value, $row))) {
      // Empty field.
      return NULL;
    }

    $empty_value = $this->database instanceof SQLiteConnection
      ? self::COORDINATE_EMPTY_VALUE_SQLITE
      : self::COORDINATE_EMPTY_VALUE;
    $processed_values = [];
    foreach ($lids as $lid) {
      $location_data = $this->getLocationProperties($lid);
      $latitude = $location_data['latitude'] ?? $empty_value;
      $longitude = $location_data['longitude'] ?? $empty_value;
      $known_geolocation_source = !empty($location_data['source']);
      // The "0.000000" values are the default values in Drupal 7, but it is
      // also a valid coordinate. However, if the geolocation source is unknown
      // (so "$known_geolocation_source" is FALSE), it means that these
      // properties are empty.
      $source_is_not_empty = $latitude !== $empty_value || $longitude !== $empty_value || $known_geolocation_source;

      if ($source_is_not_empty) {
        $processed_values[] = [
          'value' => \Drupal::service('geofield.wkt_generator')->wktBuildPoint([
            trim($longitude),
            trim($latitude),
          ]),
        ];
      }
    }

    return $processed_values;
  }

}
