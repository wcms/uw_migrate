<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\Plugin\migrate\process\SkipOnEmpty;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Extended version of the SkipOnEmpty process plugin.
 *
 * Supports multiple source properties and uses AND conjunction.
 *
 * Example:
 *
 * @code
 * plugin: uw_skip_on_empty
 * method: row
 * source:
 *   - field_foo
 *   - field_bar
 * message: 'Fields field_foo and field_bar are empty.'
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 * @see \Drupal\migrate\Plugin\migrate\process\SkipOnEmpty
 *
 * @MigrateProcessPlugin(
 *   id = "uw_skip_on_empty"
 * )
 */
class UwSkipOnEmpty extends SkipOnEmpty {

  /**
   * {@inheritdoc}
   */
  public function row($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($this->isEmpty($value)) {
      $message = !empty($this->configuration['message']) ? $this->configuration['message'] : '';
      throw new MigrateSkipRowException($message);
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function process($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($this->isEmpty($value)) {
      throw new MigrateSkipProcessException();
    }
    return $value;
  }

  /**
   * Returns TRUE if the given value is empty and FALSE otherwise.
   *
   * @param mixed $value
   *   The input value.
   *
   * @return bool
   *   TRUE if empty, FALSE otherwise.
   */
  protected function isEmpty($value) {
    $is_empty = TRUE;
    $value = !is_array($value) ? [$value] : $value;

    foreach ($value as $value_item) {
      if (!empty($value_item)) {
        $is_empty = FALSE;
        break;
      }
    }
    return $is_empty;
  }

}
