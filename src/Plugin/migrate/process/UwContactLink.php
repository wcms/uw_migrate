<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Creates a link from entity reference.
 *
 * @code
 * process:
 *   destination_field:
 *     plugin: uw_contact_link
 *     source: source_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_contact_link"
 * )
 */
class UwContactLink extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if ($value) {
      $value = 'entity:node/' . $value;
    }

    return $value;
  }

}
