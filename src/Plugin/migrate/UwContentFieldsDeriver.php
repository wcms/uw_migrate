<?php

namespace Drupal\uw_migrate\Plugin\migrate;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for content migrations based on available text fields.
 */
class UwContentFieldsDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The base plugin ID this derivative is for.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * UwContentFieldsDeriver constructor.
   *
   * @param string $base_plugin_id
   *   The base plugin ID for the plugin ID.
   */
  public function __construct($base_plugin_id) {
    $this->basePluginId = $base_plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $fields = [
      'body',
      'field_body_no_summary',
      'field_image_gallery_bottom',
      'field_sidebar_content',
      'field_position_description',
      'field_project_description',
    ];

    foreach ($fields as $field_name) {
      $values = $base_plugin_definition;

      $field_table = "field_data_{$field_name}";
      $value_column = "{$field_name}_value";
      $format_column = "{$field_name}_format";

      $values['source']['table_name'] = $field_table;
      $values['source']['fields'][] = $value_column;
      $values['source']['fields'][] = $format_column;
      $values['source']['content_column'] = $value_column;
      $values['source']['format_column'] = $format_column;

      // Add dependencies.
      if ($base_plugin_definition['id'] === 'uw_cbl_remote_video') {
        $values['migration_dependencies']['required'][] = "uw_mt_remote_video:{$field_name}";
      }

      // Specify paragraphs migration to do a lookup.
      if ($base_plugin_definition['id'] === 'uw_cbl_call_to_action') {
        $values['process']['pseudo_field_uw_cta_details']['migration'] = 'uw_para_call_to_action:' . $field_name;
      }

      /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
      $migration = \Drupal::service('plugin.manager.migration')->createStubMigration($values);
      $this->derivatives[$field_name] = $migration->getPluginDefinition();
    }

    return $this->derivatives;
  }

}
