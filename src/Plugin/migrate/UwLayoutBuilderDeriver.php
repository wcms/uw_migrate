<?php

namespace Drupal\uw_migrate\Plugin\migrate;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for content migrations based on available text fields.
 */
class UwLayoutBuilderDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The base plugin ID this derivative is for.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * UwContentFieldsDeriver constructor.
   *
   * @param string $base_plugin_id
   *   The base plugin ID for the plugin ID.
   */
  public function __construct($base_plugin_id) {
    $this->basePluginId = $base_plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    $base_plugin_id
  ) {
    return new static(
      $base_plugin_id,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $fields = [
      'body' => [
        'content_migrations' => [
          'uw_ct_expand_collapse_group',
          'uw_ct_web_page',
          'uw_ct_blog',
          'uw_ct_news_item',
          'uw_ct_event',
          'uw_ct_profile',
          'uw_ct_service',
          'uw_ct_catalog_item',
        ],
      ],
      'field_body_no_summary' => [
        'content_migrations' => [
          'uw_ct_site_footer',
        ],
        'overrides' => [
          'source' => [
            'conditions' => [
              [
                'field' => 'bundle',
                'value' => 'uw_promotional_item',
                'operator' => '!=',
              ],
            ],
          ],
        ],
      ],
      'field_sidebar_content' => [
        'content_migrations' => [
          'uw_ct_sidebar',
          'uw_ct_sidebar_promo_item',
        ],
      ],
      'field_position_description' => [
        'content_migrations' => [
          'uw_ct_opportunity',
        ],
      ],
      'field_project_description' => [
        'content_migrations' => [
          'uw_ct_project',
        ],
      ],
    ];

    foreach ($fields as $field_name => $data) {
      $values = $base_plugin_definition;
      $field_table = "field_data_{$field_name}";
      $value_column = "{$field_name}_value";

      $values['source']['table_name'] = $field_table;
      $values['source']['content_column'] = $value_column;
      $values['source']['fields'][] = $value_column;

      // Specify content migrations.
      $values['process']['nid'][0]['migration'] = $data['content_migrations'];
      $values['migration_dependencies']['optional'] += $data['content_migrations'];

      $values['process']['nid'][0]['source_ids'] = [];
      foreach ($data['content_migrations'] as $content_migration) {
        $values['process']['nid'][0]['source_ids'][$content_migration] = ['entity_id'];
        $keysToUpdate = $this->getSourceIdFieldsFromSource($content_migration);
        if (!empty($keysToUpdate)) {
          $values['process']['nid'][0]['source_ids'][$content_migration] = array_merge(
            $values['process']['nid'][0]['source_ids'][$content_migration],
            $keysToUpdate
          );
        }
      }

      if (isset($values['process']['bid'])) {
        $values['process']['bid']['field'] = $field_name;
      }

      if ($base_plugin_definition['id'] === 'uw_layout_builder_components' && isset($values['process']['section'])) {
        $values['process']['section']['migration'] = "uw_layout_builder_sections:{$field_name}";
        $values['migration_dependencies']['optional'][] = "uw_layout_builder_sections:{$field_name}";
      }

      // Other overrides.
      if (!empty($data['overrides'])) {
        $values = array_merge_recursive($values, $data['overrides']);
      }

      /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
      $migration = \Drupal::service('plugin.manager.migration')
        ->createStubMigration($values);
      $this->derivatives[$field_name] = $migration->getPluginDefinition();
    }

    return $this->derivatives;
  }

  /**
   * Get source id fields from migration.
   *
   * @param string $migration
   *   Migration name.
   *
   * @return string[]
   *   Array of source id fields excluding entity id.
   *   If wasn't defined return's empty array.
   */
  public function getSourceIdFieldsFromSource($migration) {
    $migrationData = load_uw_migrate_yml_migration($migration);
    if (empty($migrationData) || empty($migrationData["source"]["id_fields"])) {
      return [];
    }
    return array_diff(
      array_keys($migrationData["source"]["id_fields"]),
      ['entity_id']
    );
  }

}
