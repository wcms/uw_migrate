<?php

namespace Drupal\uw_migrate\Plugin\migrate\destination;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\uw_migrate\UwMigrateTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows migrating content blocks into layout builder components.
 *
 * Requires the following destination properties:
 *   - nid: ID of the host node entity;
 *   - bid: ID of the block content entity. In most cases, it'll be the result
 *     of migration_lookup or entity_lookup migrations;
 *   - section: The delta value (integer) of the layout builder section;
 *   - region: The region name (string) of the layout (e.g. first, content);
 *
 * @MigrateDestination(
 *   id = "uw_custom_block"
 * )
 */
class UwCustomBlock extends DestinationBase implements ContainerFactoryPluginInterface {

  use UwMigrateTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, EntityTypeManagerInterface $entity_type_manager, UuidInterface $uuid) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->entityTypeManager = $entity_type_manager;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
      'uuid' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {

    // Get id for the block.  Currently, that matches the previous node id.
    // This works for Related Link blocks, but may need to change for other
    // custom in-code blocks.
    $id = $row->getSourceProperty('nid');
    $destinationConfiguration = $this->configuration;

    if (!isset($destinationConfiguration['custom_block_name'])) {
      throw new MigrateSkipRowException('"custom_block_name" variable is not set in yaml destination', FALSE);
    }

    // The name of the property is call configuration.  If you are using this
    // code for another custom in-code block then you will need to call it
    // configuration.  You may need a custom process class and a transform
    // function to format it.
    $blockConfiguration = $this->formatConfiguration($row->getDestinationProperty('configuration'), $destinationConfiguration['custom_block_name']);
    $pluginConfiguration = [
      'id' => $id,
      'plugin' => $destinationConfiguration['custom_block_name'],
      'label' => 'Link',
      'label_display' => FALSE,
      'provider' => 'uw_custom_blocks',
      'settings' => $blockConfiguration,
    ];

    // Save the block.
    $block = $this->entityTypeManager->getStorage('block')->create($pluginConfiguration);
    $block->save();

    return [$block->id(), $block->uuid()];
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $node = $this->entityTypeManager->getStorage('node')->load($destination_identifier['id']);
    // Node is already gone, there is nothing to rollback.
    if (empty($node)) {
      return;
    }

    // Remove component from the layout builder section.
    /** @var \Drupal\layout_builder\Section $section */
    foreach ($node->get('layout_builder__layout')->getSections() as $section) {
      $section->removeComponent($destination_identifier['uuid']);
    }

    $node->save();
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    // There is nothing to describe here.
  }

  /**
   * Custom function to format the configuration.
   *
   * Each custom block will be different.
   *
   * @param array $values
   *   Configuration from yaml configuration variable.
   * @param string $custom_block_name
   *   Block name.
   *
   * @return array
   *   Array of values.
   */
  public function formatConfiguration(array $values, string $custom_block_name) {
    if ($custom_block_name == 'uw_cbl_links') {
      foreach ($values as &$value) {

        if (!UrlHelper::isExternal($value['uri'])) {
          // Path lookup
          // "entity:node/5".
          $value['uri'] = str_replace('entity:node', '/node', $value['uri']);
        }
      }
      return [
        'heading_level' => 'h2',
        'links' => [
          'top_icon' => NULL,
          'link_style' => 'related',
          'items' => $values,
        ],
      ];
    }
    else {
      return $values;
    }
  }

}
