<?php

namespace Drupal\uw_migrate\Plugin\migrate\destination;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\pathauto\PathautoState;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows migrating content blocks into layout builder components.
 *
 * Requires the following destination properties:
 *   - nid: ID of the host node entity;
 *   - layout: The layout identifier (e.g. uw_2_column);
 *   - layout_settings: The layout settings;
 *
 * @MigrateDestination(
 *   id = "layout_builder:section"
 * )
 */
class LayoutBuilderSection extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
      'delta' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->entityTypeManager->getStorage('node')
      ->load($row->getDestinationProperty('nid'));
    if (empty($node)) {
      throw new MigrateSkipRowException('Host node was not found.');
    }
    if (!$node->hasField(OverridesSectionStorage::FIELD_NAME)) {
      throw new MigrateSkipRowException('Host node does not support customized layout.');
    }

    /** @var \Drupal\layout_builder\SectionListInterface $layout */
    $layout = $node->get('layout_builder__layout');

    $section = new Section($row->getDestinationProperty('layout'), $row->getDestinationProperty('layout_settings'));
    $delta = $row->getSourceProperty('page_section_delta');
    // For expand collapse items it's considered the component section delta.
    // Once e/c content is a sub content and the positions starts from zero.
    if ($node->getType() === 'uw_ct_expand_collapse_group') {
      $delta = $row->getSourceProperty('component_section_delta');
    }
    if (isset($delta)) {
      $layout->insertSection($delta, $section);
    }
    else {
      $layout->appendSection($section);
      $delta = $layout->count() - 1;
    }

    $node->changed->preserve = TRUE;
    $node->path->pathauto = PathautoState::SKIP;
    $node->save();
    return [$node->id(), $delta];
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $node = $this->entityTypeManager->getStorage('node')->load($destination_identifier['id']);
    // Node is already gone, there is nothing to rollback.
    if (empty($node)) {
      return;
    }

    // Considering the deltas are changing on each save, we do not support
    // rollbacks of particular sections. Instead, we just remove all of them.
    $sections = $node->get('layout_builder__layout')->getSections();
    if (!empty($sections)) {
      $node->get('layout_builder__layout')->removeAllSections();
      $node->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    // There is nothing to describe here.
  }

}
