<?php

namespace Drupal\uw_migrate\Plugin\migrate\destination;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\pathauto\PathautoState;

/**
 * Custom destination class to preserve changed property during the migration.
 *
 * @see https://www.drupal.org/project/drupal/issues/2329253
 * @see https://www.drupal.org/project/preserve_changed
 */
class UwEntityContentBase extends EntityContentBase {

  /**
   * {@inheritdoc}
   */
  protected function save(ContentEntityInterface $entity, array $old_destination_id_values = []) {
    if ($entity->hasField('changed')) {
      $entity->changed->preserve = TRUE;
    }
    if ($entity->hasField('path')) {
      $entity->path->pathauto = PathautoState::SKIP;
    }
    return parent::save($entity, $old_destination_id_values);
  }

}
