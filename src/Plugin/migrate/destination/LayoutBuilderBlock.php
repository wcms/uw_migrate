<?php

namespace Drupal\uw_migrate\Plugin\migrate\destination;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\pathauto\PathautoState;
use Drupal\uw_migrate\UwMigrateTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows migrating content blocks into layout builder components.
 *
 * Requires the following destination properties:
 *   - nid: ID of the host node entity;
 *   - bid: ID of the block content entity. In most cases, it'll be the result
 *     of migration_lookup or entity_lookup migrations;
 *   - section: The delta value (integer) of the layout builder section;
 *   - region: The region name (string) of the layout (e.g. first, content);
 *
 * @MigrateDestination(
 *   id = "layout_builder:block"
 * )
 */
class LayoutBuilderBlock extends DestinationBase implements ContainerFactoryPluginInterface {

  use UwMigrateTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, EntityTypeManagerInterface $entity_type_manager, UuidInterface $uuid) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->entityTypeManager = $entity_type_manager;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
      'uuid' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    /** @var \Drupal\node\NodeInterface $node */
    $nid = $row->getDestinationProperty('nid');
    $node = $this->entityTypeManager->getStorage('node')
      ->load($nid);
    if (empty($node)) {
      throw new MigrateSkipRowException('Host node (' . $nid . ') was not found.', FALSE);
    }
    if (!$node->hasField(OverridesSectionStorage::FIELD_NAME)) {
      throw new MigrateSkipRowException('Host node (' . $nid . ') does not support customized layout.', FALSE);
    }

    if ($row->hasDestinationProperty('customPlugin')) {
      $configuration = $this->getCustomBlockConfiguration($row);
      if (empty($configuration)) {
        throw new MigrateSkipRowException('In-code block migration does not have destination configuration named configuration', FALSE);
      }
    }
    else {
      $configuration = $this->getComponentConfiguration($row);
      if (empty($configuration)) {
        $configuration = $this->getBlockContentConfiguration($row);
      }
    }

    /** @var \Drupal\layout_builder\Field\LayoutSectionItemList $node_layout */
    $node_layout = $node->get('layout_builder__layout');

    // Append a component to given section/region of page layout.
    $dest_section = $row->getDestinationProperty('section');
    $dest_layout = $row->getDestinationProperty('default_layout');

    if (isset($dest_section)) {
      // First value is entity ID, the second is section delta.
      if (is_array($dest_section)) {
        [, $section_delta] = $dest_section;
      }
      else {
        $section_delta = $dest_section;
      }
      /** @var \Drupal\layout_builder\Section $section */
      $section = $node_layout->getSection($section_delta);
    }

    if (empty($section) || !$this->isSectionApplicable($section, $row)) {
      // If section wasn't created during migration, we choose either one of the
      // existing sections or create a new one here.
      $sections = $node_layout->getSections();
      $section = end($sections);

      // Make sure the section has the expected layout. And if not, then create
      // a new one.
      if (empty($sections) || !$this->isSectionApplicable($section, $row)) {
        $section = new Section($dest_layout);

        // Finally, insert or append a new section into layout field.
        if (!empty($section_delta)) {
          $node_layout->insertSection($section_delta, $section);
        }
        else {
          $node_layout->appendSection($section);
        }
      }
    }

    $region = $row->getDestinationProperty('region');
    $component = new SectionComponent($this->uuid->generate(), $region, $configuration);

    // Insert a component to the given delta or append to the end if the delta
    // is missing.
    $delta = $row->getSourceProperty('component_region_delta');
    if (isset($delta)) {
      $section->insertComponent($delta, $component);
    }
    else {
      $section->appendComponent($component);
    }

    $node->changed->preserve = TRUE;
    $node->path->pathauto = PathautoState::SKIP;
    $node->save();
    return [$node->id(), $component->getUuid()];
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $node = $this->entityTypeManager->getStorage('node')->load($destination_identifier['id']);
    // Node is already gone, there is nothing to rollback.
    if (empty($node)) {
      return;
    }

    // Remove component from the layout builder section.
    /** @var \Drupal\layout_builder\Section $section */
    foreach ($node->get('layout_builder__layout')->getSections() as $section) {
      $section->removeComponent($destination_identifier['uuid']);
    }

    $node->save();
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    // There is nothing to describe here.
  }

  /**
   * Prepares component configuration for block_content items.
   */
  protected function getCustomBlockConfiguration(Row $row) {
    $bid = $row->getDestinationProperty('bid');

    // This works for Link blocks.  This may need to change for other custom
    // in-code block types.
    // $id = $row->getSourceProperty('nid');
    // $bid[0] is id and $bid[1] is uuid.
    $id = $bid[0];

    if (empty($bid) || empty($id)) {
      throw new MigrateSkipRowException('Block was not migrated yet.', FALSE);
    }

    $block_custom = $this->entityTypeManager->getStorage('block')->load($id);

    if (empty($block_custom)) {
      throw new MigrateSkipRowException('Custom block entity was not found.', FALSE);
    }

    $settings = $block_custom->get('settings');

    return $settings;
  }

  /**
   * Prepares component configuration for block_content items.
   */
  protected function getBlockContentConfiguration(Row $row) {

    $bid = $row->getDestinationProperty('bid');

    if (empty($bid)) {
      throw new MigrateSkipRowException('Block was not migrated yet.', FALSE);
    }

    $block_content = $this->entityTypeManager
      ->getStorage('block_content')
      ->load($bid);
    if (empty($block_content)) {
      throw new MigrateSkipRowException('Block content entity was not found.', FALSE);
    }

    // Enable block label for some block types.
    $label_display = 0;
    if ($block_content->bundle() === 'uw_cbl_related_links' || $row->getSourceProperty('label_display')) {
      $label_display = 'visible';
    }

    return [
      'id' => 'inline_block:' . $block_content->bundle(),
      'label' => $block_content->label(),
      'provider' => 'layout_builder',
      'label_display' => $label_display,
      'view_mode' => 'full',
      'block_revision_id' => $block_content->getRevisionId(),
    ];
  }

  /**
   * Returns configuration for layout builder component.
   */
  protected function getComponentConfiguration(Row $row) {
    $config = [];

    $tag = $row->getSourceProperty('html_tag');
    $blocks = $this->getTagsMapping();
    $config_blocks = $this->getConfigBlocks();
    $destNid = $row->getDestinationProperty('nid');

    // All expand collapses nids found.
    $expand_collapse_nids = $row->getDestinationProperty('expand_collapse_ids');
    // Get e/c by of the current node.
    $ecs = $expand_collapse_nids[$destNid][$row->getSourceProperty('page_section_delta')] ?? [];

    if (!empty($ecs)) {
      $config = [
        'id' => 'uw_cbl_expand_collapse',
        'provider' => 'uw_custom_blocks',
      ];
      $config['label'] = 'Expand/collapse groups';
      $config['label_display'] = 0;
      $config['heading_selector'] = 'h2';
      $config['ecs'] = [];
      foreach ($ecs as $nid) {
        $config['ecs'][] = [
          'group' => $nid,
        ];
      }
      return $config;
    }
    elseif (isset($blocks[$tag]) && $blocks[$tag] == 'uw_cbl_expand_collapse') {
      return NULL;
    }

    if (isset($blocks[$tag]) && in_array($blocks[$tag], $config_blocks, TRUE)) {
      $block_id = $blocks[$tag];

      // Default configuration of config blocks.
      $config = [
        'id' => $block_id,
        'provider' => 'uw_custom_blocks',
        'label_display' => 0,
      ];

      if ($block_id === 'uw_cbl_twitter') {
        $config['twitter_feed_type'] = $row->getSourceProperty('twitter_feed_type');
        $config['twitter_feed_type'] = $config['twitter_feed_type'] === 'tweet' ? 'embed-tweet' : $config['twitter_feed_type'];

        // Some widget types should be skipped, because they're not supported.
        // See: https://twitter.com/TwitterDev/status/1367174712397209602
        $accepted_tweet_types = ['embed-tweet', 'profile', 'list'];
        if (!in_array($config['twitter_feed_type'], $accepted_tweet_types)) {
          new MigrateSkipRowException($config['twitter_feed_type'] . ' Twitter feed type is not supported');
        }

        $config['label'] = 'Twitter';
        $config['twitter_listname'] = $row->getSourceProperty('twitter_listname');
        $config['twitter_username'] = $row->getSourceProperty('twitter_username');
        $config['twitter_tweet_code'] = $row->getSourceProperty('twitter_tweet_code');
      }
      elseif ($block_id === 'uw_cbl_facebook') {
        $config['label'] = 'Facebook';
        $config['fb_feed_type'] = $row->getSourceProperty('fb_feed_type');
        $config['fb_url'] = $row->getSourceProperty('fb_url');
        $config['fb_page'] = $row->getSourceProperty('fb_page');
      }
    }

    return $config;
  }

  /**
   * Checks whether given layout builder sections is applicable for the row.
   */
  protected function isSectionApplicable(Section $section, Row $row) {
    // No layout - no restrictions.
    if (!$row->hasDestinationProperty('default_layout')) {
      return TRUE;
    }
    return $section->getLayout()->getPluginId() === $row->getDestinationProperty('default_layout');
  }

}
