<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

/**
 * Source plugin for retrieving embedded maps from content value.
 *
 * @MigrateSource(
 *   id = "uw_map",
 *   source_module = "system"
 * )
 */
class UwMap extends UwEmbeddedContent {

  /**
   * {@inheritdoc}
   */
  protected function getItems($content) {
    return $this->parser->getMaps($content);
  }

}
