<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\migrate\Exception\RequirementsException;
use Drupal\migrate_plus\Plugin\migrate\source\Table;
use Drupal\Core\Database\Query\SelectInterface;

/**
 * Extended version of Table source plugin.
 *
 * @MigrateSource(
 *   id = "uw_table",
 *   source_module = "system"
 * )
 */
class UwTable extends Table {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = parent::query();

    // Extra columns in joined tables.
    if (isset($this->configuration['columns'])) {
      foreach ($this->configuration['columns'] as $alias => $columns) {
        $query->fields($alias, $columns);
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return $this->initializeIterator()->count();
  }

  /**
   * {@inheritdoc}
   */
  public function checkRequirements(): void {
    $table = $this->configuration['table_name'];
    if (!$this->getDatabase()->schema()->tableExists($table)) {
      throw new RequirementsException($table . ' sql table is missing on source site');
    }
    parent::checkRequirements();
  }

}
