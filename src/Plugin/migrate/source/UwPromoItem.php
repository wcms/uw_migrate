<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\uw_migrate\UwContentParser;

/**
 * Retrieves promo items and creates each row for promo content section.
 *
 * @MigrateSource(
 *   id = "uw_promo_item",
 *   source_module = "system"
 * )
 */
class UwPromoItem extends UwNode {

  /**
   * Custom content parser.
   *
   * @var \Drupal\uw_migrate\UwContentParser
   */
  protected $parser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $configuration['node_type'] = 'uw_promotional_item';
    $this->parser = new UwContentParser();
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager, $module_handler);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // Join table with visibility and body values to have it available in
    // self::initializeIterator().
    $query->leftJoin('field_data_field_block_visibility', 'visibility', 'n.nid = visibility.entity_id');
    $query->fields('visibility', ['field_block_visibility_value']);

    $query->leftJoin('field_data_field_body_no_summary', 'body', 'n.nid = body.entity_id');
    $query->fields('body', ['field_body_no_summary_value']);

    $query->leftJoin('field_data_field_do_not_display_title', 'display_title', 'n.nid = display_title.entity_id');
    $query->fields('display_title', ['field_do_not_display_title_value']);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $fetch_all = !empty($this->configuration['fetch_all']);
    $new_rows = [];

    // Prepare rows according to the visibility rules and content sections.
    foreach ($iterator as $row) {
      foreach ($this->getBlockLocations($row['field_block_visibility_value']) as $nid) {
        $delta = 0;
        $content_sections = $this->parser->getContentSections($row['field_body_no_summary_value']);
        foreach ($content_sections as $content) {
          $is_embedded = $this->parser->isEmbeddedTag($content);
          // Filter out embedded tags if we don't need all items.
          if (!$fetch_all && $is_embedded) {
            $delta++;
            continue;
          }

          $new_row = $row;
          $new_row['value'] = $content;
          $new_row['label_display'] = !$row['field_do_not_display_title_value'];
          $new_row['entity_id'] = $new_row['nid'];
          $new_row['node_pos_id'] = $nid;
          $new_row['delta'] = $delta;
          $new_row['ref_id'] = 0;
          if ($is_embedded) {
            $new_row = array_merge($new_row, $this->parser->getElementProperties($content));
          }
          $new_rows[] = $new_row;
          $delta++;
        }
      }
    }

    // Return iterator with new rows.
    return new \ArrayIterator($new_rows);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    // Aggregated mode ignores source node (promo) identifier and section delta.
    if (empty($this->configuration['aggregated'])) {
      $ids = parent::getIds();
      $ids['node_pos_id']['type'] = 'integer';
      $ids['delta']['type'] = 'integer';
    }
    else {
      $ids['node_pos_id']['type'] = 'integer';
    }
    return $ids;
  }

  /**
   * Returns a list of node ids where the block was displayed in D7.
   */
  protected function getBlockLocations($visibility_rules) {
    $aliases = \preg_split('(\r\n|\r|\n)', $visibility_rules);

    if ($visibility_rules) {
      // Convert absolute paths to relative (and valid) aliases.
      foreach ($aliases as &$alias) {
        $parts = parse_url($alias);
        $alias = empty($parts['path']) ? ltrim($parts['path'], '/') : $alias;
      }

      // Load system urls.
      $urls = $this->select('url_alias', 'a')
        ->fields('a', ['source'])
        ->condition('a.alias', $aliases, 'IN')
        ->execute()
        ->fetchAll();

      $nids = array_map(function ($item) {
        preg_match('/node\/([0-9]+)/', $item['source'], $matches);
        return isset($matches[1]) ? $matches[1] : FALSE;
      }, $urls);
    }
    else {
      $content_types = [
        'uw_web_page',
        'uw_web_form',
        'uw_catalog_item',
        'uw_service',
        'uw_image_gallery',
        'uwaterloo_custom_listing',
        'uw_blog',
        'contact',
        'uw_event',
        'uw_news_item',
        'uw_opportunities',
        'uw_ct_person_profile',
        'uw_project',
      ];

      $nids = $this->select('node', 'n')
        ->fields('n', ['nid'])
        ->condition('n.type', $content_types, 'IN')
        ->execute()
        ->fetchCol();
    }

    return array_filter($nids);
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return $this->initializeIterator()->count();
  }

}
