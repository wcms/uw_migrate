<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\paragraphs\Plugin\migrate\source\d7\FieldCollectionItem;

/**
 * Custom Field Collection Item source plugin.
 *
 * @MigrateSource(
 *   id = "uw_field_collection_item",
 *   source_module = "field_collection",
 * )
 */
class UwFieldCollectionItem extends FieldCollectionItem {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'conditions' => [],
      'joins' => [],
    ] + parent::defaultConfiguration();
  }

}
