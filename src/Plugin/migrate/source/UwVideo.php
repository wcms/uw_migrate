<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

/**
 * Source plugin for retrieving embedded videos from content value.
 *
 * @MigrateSource(
 *   id = "uw_video",
 *   source_module = "system"
 * )
 */
class UwVideo extends UwEmbeddedContent {

  /**
   * {@inheritdoc}
   */
  protected function getItems($content) {
    return $this->parser->getVideos($content);
  }

}
