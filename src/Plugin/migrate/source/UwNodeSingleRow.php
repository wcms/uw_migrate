<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

/**
 * Source plugin for homepage banners to combine multiple nodes into 1 row.
 *
 * @MigrateSource(
 *   id = "uw_node_single_row",
 *   source_module = "node"
 * )
 */
class UwNodeSingleRow extends UwNode {

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $init_array = iterator_to_array(parent::initializeIterator());

    if (empty($init_array)) {
      return new \ArrayIterator();
    }

    // The default values for the row.
    $rows = ['nid' => 1];

    // If this is a banner, get the banner settings and order.
    // If not, the items are just the init array.
    if (
      isset($init_array[0]['type']) &&
      $init_array[0]['type'] == 'uw_home_page_banner'
    ) {

      // Get the banners in the correct order.
      $rows['items'] = $this->getBannerOrder($init_array);

      // Get the banner settings.
      $banner_settings = $this->getBannerSettings();

      // Step through and add each banner setting to the rows.
      foreach ($banner_settings as $index => $banner_setting) {
        $rows[$index] = $banner_setting;
      }
    }
    else {
      $rows['items'] = $init_array;
    }

    // Set the single row.
    $single_row = [$rows];

    return new \ArrayIterator($single_row);
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return $this->initializeIterator()->count();
  }

  /**
   * Function to get the banner settings.
   *
   * @return array
   *   Array of banner settings.
   */
  public function getBannerSettings(): array {

    // The banner settings to be migrated.
    $setting_names = [
      'slide_speed' => 'uw_banner_slideshow_slidespeed',
      'transition_speed' => 'uw_banner_slideshow_transitionspeed',
      'autoplay' => 'uw_banner_slideshow_autoplay',
    ];

    // Step through each of the banner settings and get
    // the values from the D7 DB.
    foreach ($setting_names as $index => $setting_name) {

      // Query to get the value from the D7 DB.
      $banner_settings[$index] = $this->variableGet($setting_name, 0);
    }

    return $banner_settings;
  }

  /**
   * Function to get banners in the correct order.
   *
   * @param array $init_array
   *   The banners not in order.
   *
   * @return array
   *   Array of banners in the correct order.
   */
  public function getBannerOrder(array $init_array): array {
    $banner_order = [];

    // Query to get the order of banners from the D7 DB.
    $results = $this->select('draggableviews_structure', 'ds')
      ->fields('ds', ['entity_id', 'weight'])
      ->condition('ds.view_name', 'uw_display_home_page_banners')
      ->condition('ds.view_display', 'page_2')
      ->execute()->fetchAll(\PDO::FETCH_ASSOC);

    // Step through the results and get the order of the banners.
    foreach ($results as $result) {
      $banner_order[$result['weight']] = $result['entity_id'];
    }

    $items = [];
    // Step through the order of the banners and set the items
    // using that order.
    foreach ($banner_order as $order) {

      // Step through each of the banners and if its next in
      // the order, add it to the items array.
      foreach ($init_array as $index => $banner) {

        // If this banner is next in the order, add it to the
        // items array and unset it, so we do not step over
        // that same banner again and break out of the loop,
        // saving computing time.
        if ($banner['nid'] == $order) {
          $items[] = $banner;
          unset($init_array[$index]);
          break;
        }
      }
    }

    return $items;
  }

}
