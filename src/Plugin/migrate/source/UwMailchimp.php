<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\uw_migrate\ContentTransformers\MailchimpFormat;

/**
 * Source plugin for retrieving <ckmailchimp> tags from content.
 *
 * @MigrateSource(
 *   id = "uw_mailchimp",
 *   source_module = "system"
 * )
 */
class UwMailchimp extends UwContentComplete {

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = iterator_to_array(parent::initializeIterator());
    $new_rows = [];
    $content_col = $this->configuration['content_column'];

    foreach ($iterator as $row) {
      $content_sections = $this->parser->getContentSections($row[$content_col]);
      foreach ($content_sections as $content) {
        $mailchimp = $this->parser->getMailchimpTags($content);
        if (empty($mailchimp)) {
          continue;
        }

        // Format html code.
        $formatter = new MailchimpFormat();
        $value = $formatter->transform($mailchimp[0]['sourcecode']);

        $new_row = $row;
        $new_row['value'] = $value;
        $new_rows[] = $new_row;
      }
    }
    // Return iterator with new rows.
    return new \ArrayIterator($new_rows);
  }

  /**
   * Identifies if current piece of content is applicable to current migration.
   */
  public static function isApplicable($content, $parser, $configuration) {
    return preg_match("/<ckmailchimp/", $content);
  }

}
