<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Source plugin for retrieving and formatting content sections.
 *
 * It loads formatted content from the database and splits it into multiple
 * content pieces by using embedded tags as delimiters.
 *
 * @MigrateSource(
 *   id = "uw_content",
 *   source_module = "node"
 * )
 */
class UwContent extends UwEmbeddedContent {

  use UwMigrateTrait;

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $content_col = $this->configuration['content_column'];
    $format_col = $this->configuration['format_column'];
    $fetch_all = !empty($this->configuration['fetch_all']);
    $new_rows = [];

    foreach ($iterator as $row) {
      $content_sections = $this->parser->getContentSections($row[$content_col]);

      foreach ($content_sections as $delta => $content) {
        $is_embedded = $this->parser->isEmbeddedTag($content);
        // Filter out embedded tags if we don't need all items.
        if (!$fetch_all && $is_embedded) {
          continue;
        }

        $new_row = $row;
        $new_row['value'] = $content;
        $new_row['format'] = $format_col;
        $new_row['delta'] = $delta;
        $new_row['ref_id'] = 0;
        if ($is_embedded) {
          $new_row = array_merge($new_row, $this->parser->getElementProperties($content));
        }
        $new_rows[] = $new_row;
      }
    }

    // Return iterator with new rows.
    return new \ArrayIterator($new_rows);
  }

}
