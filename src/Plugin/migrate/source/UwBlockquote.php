<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

/**
 * Source plugin for retrieving embedded <blockquote> tags from content.
 *
 * @MigrateSource(
 *   id = "uw_blockquote",
 *   source_module = "system"
 * )
 */
class UwBlockquote extends UwContentComplete {

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $new_rows = iterator_to_array($iterator);

    if (empty($new_rows)) {
      return new \ArrayIterator($new_rows);
    }

    // The iterator should already contain only blockquote items.
    // We just need to add quote and attribution values here.
    foreach ($new_rows as &$row) {
      $block_quote = $this->parser->getBlockquotes($row['value']);
      if (empty($block_quote)) {
        continue;
      }
      $row['quote'] = $block_quote[0]['content'];
      $row['attribution'] = $block_quote[0]['attribution'];
    }

    // Return iterator with new rows.
    return new \ArrayIterator($new_rows);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable($content, $parser, $configuration) {
    return preg_match("/<blockquote.*blockquote.*>/Us", $content);
  }

}
