<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

/**
 * Source plugin for retrieving embedded <ckpowerbi> tags from content value.
 *
 * @MigrateSource(
 *   id = "uw_powerbi",
 *   source_module = "system"
 * )
 */
class UwPowerBi extends UwEmbeddedContent {

  /**
   * {@inheritdoc}
   */
  protected function getItems($content) {
    return $this->parser->getPowerBiTags($content);
  }

}
