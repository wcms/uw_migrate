<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\paragraphs\Plugin\migrate\source\d7\ParagraphsItem;

/**
 * Custom paragraphs item source plugin.
 *
 * Available configuration keys:
 * - field_name: (optional) If supplied, this will add host entity information
 *   from field table.
 *
 * @MigrateSource(
 *   id = "uw_paragraphs_item",
 *   source_module = "paragraphs",
 * )
 */
class UwParagraphsItem extends ParagraphsItem {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    if (!empty($this->configuration['field_name'])) {
      $field_name = $this->configuration['field_name'];
      $query->join("field_data_{$field_name}", 'field', "p.item_id = field.{$field_name}_value");
      $query->fields('field', ['entity_id', 'revision_id']);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'conditions' => [],
      'joins' => [],
    ] + parent::defaultConfiguration();
  }

}
