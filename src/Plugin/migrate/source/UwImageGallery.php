<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\uw_migrate\UwMigrateSourceTrait;

/**
 * Source plugin for retrieving embedded <ckimagegallery> tags.
 *
 * @MigrateSource(
 *   id = "uw_image_gallery",
 *   source_module = "system"
 * )
 */
class UwImageGallery extends UwEmbeddedContent {

  use UwMigrateSourceTrait;

  /**
   * {@inheritdoc}
   */
  protected function getItems($content) {
    $rows = $this->parser->getImageGalleries($content);

    if (empty($rows)) {
      return [];
    }

    $ids = array_map(function ($item) {
      return $item['ref_id'];
    }, $rows);

    // Load D7 node items.
    $source = $this->getStubMigration([
      'plugin' => 'uw_node',
      'node_type' => 'uw_image_gallery',
      'conditions' => [
        [
          'field' => 'n.nid',
          'value' => $ids,
          'operator' => 'IN',
        ],
      ],
    ])->getSourcePlugin();

    // Map source rows to node IDs for easier access.
    $node_rows = [];
    foreach ($this->getSourceRows($source) as $node_row) {
      $node_rows[$node_row['nid']] = $node_row;
    }

    // Combine embedded tag data with D7 node values.
    foreach ($rows as &$row) {
      if (isset($node_rows[$row['ref_id']])) {
        $row += $node_rows[$row['ref_id']];
      }
    }

    return $rows;
  }

}
