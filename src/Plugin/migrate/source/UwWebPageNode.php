<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Assigns D7 front page node to the existing homepage.
 *
 * @MigrateSource(
 *   id = "uw_web_page",
 *   source_module = "system"
 * )
 */
class UwWebPageNode extends UwNode {

  /**
   * Front page node id.
   *
   * @var string
   */
  protected $frontPage;

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $front_page_path = $this->variableGet('site_frontpage', '');
    if (preg_match('@node/(\d+)$@', $front_page_path, $matches)) {
      $this->frontPage = end($matches);
    }
    return parent::initializeIterator();
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // D8 version creates default Homepage in uw_ct_web_page_install().
    // To avoid duplicate, we assign existing D7 frontpage to a new D8 page.
    if ($this->frontPage == $row->getSourceProperty('nid')) {
      $row->setSourceProperty('front_nid', 1);
    }
    return parent::prepareRow($row);
  }

}
