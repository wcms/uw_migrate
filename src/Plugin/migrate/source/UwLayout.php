<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\uw_migrate\UwContentParser;
use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Source plugin for retrieving content layouts.
 *
 * @MigrateSource(
 *   id = "uw_layout",
 *   source_module = "system"
 * )
 */
class UwLayout extends UwEmbeddedContent {

  use UwMigrateTrait;

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $rows = self::buildLayoutComponents($iterator, $this->configuration['content_column'], $this->parser);
    return new \ArrayIterator($rows);
  }

  /**
   * Build layout structure based on row html content.
   *
   * @return array
   *   Array of sections to be added on node layout builder.
   */
  public static function buildLayoutComponents($items, $content_col, $parser, $ref_row = []): array {
    $new_rows = [];
    foreach ($items as $item_delta => $item) {
      if (!empty($ref_row)) {
        $item += $ref_row;
      }
      $content = empty($ref_row) ? $item[$content_col] : $item['value'];
      $layout_sections = $parser->getLayoutItems($content);
      foreach ($layout_sections as $delta => $section) {
        $new_row = $item;
        $new_row['page_section_delta'] = $item_delta;
        $new_row['component_delta'] = $delta;
        $new_row['component_section_delta'] = -1;
        if (!empty($ref_row)) {
          $new_row['page_section_delta'] = $item['component_delta'];
          $new_row['component_delta'] = $item_delta;
          $new_row['component_section_delta'] = $delta;
        }

        $new_row['layout'] = $section['layout'];
        $new_row['layout_settings'] = $section['layout_settings'];

        $formatted_row = array_merge($item, $section, $new_row);
        $new_rows[] = $formatted_row;

        $firstRegion = current($section['regions'])['content'];
        $sub_rows = self::getSubContent($firstRegion, $parser);
        if ($sub_rows) {
          $new_rows = array_merge(
            $new_rows,
            self::buildLayoutComponents($sub_rows, $content_col, $parser, $formatted_row)
          );
        }
      }
    }
    return $new_rows;
  }

  /**
   * Get possible sub content inside another html content.
   *
   * @param string $content
   *   Html string content.
   * @param \Drupal\uw_migrate\UwContentParser $parser
   *   Html parser.
   *
   * @return array
   *   Array with new rows generate from the content.
   */
  public static function getSubContent(string $content, UwContentParser $parser): array {
    return $parser->getExpandCollapse($content);
  }

}
