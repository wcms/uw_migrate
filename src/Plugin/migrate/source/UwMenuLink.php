<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\menu_link_content\Plugin\migrate\source\MenuLink;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Drupal menu link source from database with filter by menu name.
 *
 * Available configuration key:
 * - menu_name: (optional) One or more source menu machine names.
 *
 * Example:
 *
 * @code
 * source:
 *   plugin: uw_menu_link
 *   menu_name:
 *     - main-menu
 *     - service-menu
 * @endcode
 *
 * In this example, only menu links from the menus machine-named "main-menu" and
 * "service-menu" will be migrated.
 *
 * @MigrateSource(
 *   id = "uw_menu_link",
 *   source_module = "menu"
 * )
 */
class UwMenuLink extends MenuLink {

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * List of source menu names to migrate.
   *
   * @var array
   */
  protected $menuNames = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, MenuLinkManagerInterface $menuLinkManager, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);

    // Ensure that menu_names is an array.
    $this->menuNames = (array) $configuration['menu_name'];

    // Load uw_sites_all.install file to be able to call a function with
    // default links - _uw_sites_all_get_menu_items().
    $this->moduleHandler = $moduleHandler;
    $this->moduleHandler->loadInclude('uw_sites_all', 'install');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.menu.link'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // If menu names were provided, modify the query with an IN clause.
    if (!empty($this->menuNames)) {
      $query->condition('ml.menu_name', $this->menuNames, 'IN');
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $source_path = $row->getSourceProperty('router_path');
    $menu_name = $row->getSourceProperty('menu_name');
    $link_path = $row->getSourceProperty('link_path');
    $url = \str_contains($link_path, 'http') ? NULL : URL::fromUri("internal:/$link_path");

    // Skip the home page link as the link is now an icon and has already
    // been created.
    if ($link_path == '<front>' && $menu_name == 'main-menu') {
      return FALSE;
    }

    $links_mapping = [
      'opportunities' => 'Opportunities',
      'contacts' => 'Contacts',
      'people-profiles' => 'Profiles',
      'blog' => 'Blog',
      'events' => 'Events',
      'news' => 'News',
      'catalogs' => 'Catalogs',
      'services' => 'Services',
      'projects' => 'Projects',
    ];

    // Assign an actual ID to update (not create) the link for selected paths.
    // We're using a separate uw_mlid property to preserve the original mlid
    // value.
    if ($source_path && isset($links_mapping[$source_path]) && $menu_name === 'main-menu' && $url && $url->isRouted()) {
      // Get default values.
      $default_links = _uw_sites_all_get_menu_items();
      $routeName = $url->getRouteName();

      if ($routeName !== 'entity.node.canonical') {
        // Get a list of default links and their IDs.
        $keys = array_keys($default_links);
        $link_id = array_search($links_mapping[$source_path], $keys) + 1;
        $item = $links_mapping[$source_path];
        $row->setSourceProperty('mlid', $link_id);
        $row->setSourceProperty('enabled', !$row->getSourceProperty('hidden'));
        $row->setSourceProperty('route', $default_links[$item]['route_name']);
      }
    }
    // Validate if is a taxonomy link and update existing links.
    elseif ($link_path && $source_path === 'taxonomy/term/%') {
      $database = \Drupal::database();
      $result = $database->select('menu_link_content_data', 'm')
        ->fields('m')
        ->condition('link__uri', "internal:/$link_path")
        ->execute()->fetchObject();
      if ($result) {
        $row->setSourceProperty('mlid', $result->id);
      }
    }
    return parent::prepareRow($row);
  }

}
