<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\uw_migrate\UwContentParser;
use Drupal\uw_migrate\UwContentTransformer;
use Drupal\Core\Database\Query\SelectInterface;

/**
 * Basic class for retrieving embedded content items.
 */
abstract class UwEmbeddedContent extends UwTable {

  /**
   * Custom content parser.
   *
   * @var \Drupal\uw_migrate\UwContentParser
   */
  protected $parser;

  /**
   * Custom content transformer.
   *
   * @var \Drupal\uw_migrate\UwContentTransformer
   */
  protected $transformer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
    $this->parser = new UwContentParser();
    $this->transformer = new UwContentTransformer();
  }

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = parent::query();

    // Join node table and filter out unpublished items.
    $query->join('node', 'n', "n.nid = t.entity_id AND n.status = 1");

    // Include field values of supported content types.
    $supported_node_types = [
      'uw_blog',
      'uw_ct_person_profile',
      'uw_event',
      'uw_news_item',
      'uw_opportunities',
      'uw_project',
      'uw_service',
      'uw_site_footer',
      'uw_web_form',
      'uw_web_page',
      'uw_ct_expand_collapse_group',
      'uwaterloo_custom_listing',
      'uw_catalog_item',
    ];
    $query->condition('n.type', $supported_node_types, 'IN');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = iterator_to_array(parent::initializeIterator());
    // Iterate by rows performing transformation needed before processing the
    // migration.
    $content_col = $this->configuration['content_column'];
    $new_rows = [];
    foreach ($iterator as $row) {
      if (!empty($row[$content_col])) {
        $row[$content_col] = $this->transformer->transform($row[$content_col]);
      }
      $new_rows[] = $row;
    }

    // @todo Change classes inheritance to avoid this condition.
    if (method_exists($this, 'getItems')) {
      $items = [];

      foreach ($new_rows as $row) {
        foreach ($this->getItems($row[$content_col]) as $data) {
          $items[] = $row + $data;
        }
      }

      // Return iterator with new rows.
      $new_rows = new \ArrayIterator($items);
    }
    return $new_rows;
  }

}
