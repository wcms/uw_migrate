<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * Migrate draggableview values.
 *
 * @MigrateSource(
 *   id = "uw_draggableviews",
 *   source_module = "webform",
 * )
 */
class UwDraggableviews extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('draggableviews_structure', 'ds');

    $query->fields('ds', [
      'dvid',
      'view_name',
      'view_display',
      'args',
      'entity_id',
      'weight',
      'parent',
    ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'dvid' => $this->t('id'),
      'view_name' => $this->t('View name'),
      'view_display' => $this->t('View display'),
      'args' => $this->t('View args'),
      'entity_id' => $this->t('Node id'),
      'weight' => $this->t('Weight'),
      'parent' => $this->t('Parent'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Get d7 view values.
    $viewName = $row->getSourceProperty('view_name');
    $viewDisplay = $row->getSourceProperty('view_display');

    // Check if the view values match with the new views in d9.
    if ($viewName == 'uw_people_profile_pages' && $viewDisplay == 'rearrange_people_profiles') {
      $row->setSourceProperty('view_name', 'uw_view_profiles');
      $row->setSourceProperty('view_display', 'rearrange_profiles');
    }
    elseif ($viewName == 'uw_staff_contacts_vw' && ($viewDisplay == 'rearrange_staff_contacts' || $viewDisplay == 'page_1')) {
      $row->setSourceProperty('view_name', 'uw_view_contacts');
      $row->setSourceProperty('view_display', 'rearrange_contacts');
    }
    // You should add a new elseif to migrate other draggableviews.
    else {
      \Drupal::logger('uw_draggableviews')->notice('View name ' . $viewName . ' with display ' . $viewDisplay . '  is not configured in UwDraggableviews.php');
      return FALSE;
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'dvid' => [
        'type' => 'integer',
      ],
    ];
  }

}
