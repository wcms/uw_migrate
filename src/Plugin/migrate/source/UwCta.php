<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\uw_migrate\UwMigrateSourceTrait;

/**
 * Source plugin for retrieving embedded <ckcalltoaction> tags.
 *
 * @MigrateSource(
 *   id = "uw_cta",
 *   source_module = "system"
 * )
 */
class UwCta extends UwEmbeddedContent {

  use UwMigrateSourceTrait;

  /**
   * {@inheritdoc}
   */
  protected function getItems($content) {
    $tags = $this->parser->getCallToActions($content);

    if (empty($tags)) {
      return [];
    }

    $ids = array_map(function ($item) {
      return $item['ref_id'];
    }, $tags);

    // Load D7 content items (nodes or paragraphs) using another source plugin.
    if ($this->configuration['load'] === 'nodes') {
      $migration = $this->getStubMigration([
        'plugin' => 'uw_node',
        'node_type' => 'uw_embedded_call_to_action',
        'conditions' => [
          [
            'field' => 'n.nid',
            'value' => $ids,
            'operator' => 'IN',
          ],
        ],
      ]);
    }
    else {
      // Paragraphs.
      $migration = $this->getStubMigration([
        'plugin' => 'uw_paragraphs_item',
        'bundle' => 'call_to_action',
        'field_name' => 'field_call_to_action',
        'joins' => [
          [
            'table' => 'field_data_field_call_to_action',
            'alias' => 'fcta',
            'condition' => 'p.item_id = fcta.field_call_to_action_value',
          ],
        ],
        'conditions' => [
          [
            'field' => 'fcta.entity_id',
            'value' => $ids,
            'operator' => 'IN',
          ],
        ],
      ]);
    }

    return $this->getSourceRows($migration->getSourcePlugin());
  }

}
