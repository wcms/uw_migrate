<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Drupal\file\Plugin\migrate\source\d7\File;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Custom Drupal 7 file source from database with filter by fields.
 *
 * Available configuration keys:
 * - fields (required): A list of fields that should be considered during
 *  migration. Files attached to these fields will be retrieved from source
 *  database.
 * - properties (optional): A list of additional properties to fetch from the
 *  field table (i.e. alt, title).
 *
 * @MigrateSource(
 *   id = "uw_file",
 *   source_module = "file"
 * )
 */
class UwFile extends File {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager) {
    if (empty($configuration['constants']['source_base_path'])) {
      $configuration['constants']['source_base_path'] = Settings::get('uw_migrate_source', '');
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->orderBy('f.fid');

    if (!isset($this->configuration['fields'])) {
      return $query;
    }

    // Avoid duplicates due to multiple left joins.
    $query->distinct();

    $or = $query->orConditionGroup();
    foreach ($this->getTablesInfo() as $field_table => $columns) {
      $fid_col = "{$field_table}.{$columns['fid']}";

      // Check whether the table exists.
      if (!$this->getDatabase()->schema()->tableExists($field_table)) {
        continue;
      }

      $query->leftJoin($field_table, $field_table, "f.fid = {$fid_col}");
      $or->isNotNull("{$fid_col}");

      // Retrieve requested fields.
      $query->fields($field_table, array_values($columns));
    }
    $query->condition($or);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $this->publicPath = Settings::get('uw_migrate_site_path', $this->publicPath);
    return $iterator;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Validate if the route is files and replace for documents.
    $uri = $row->getSourceProperty('uri');
    if (str_contains($uri, 'uploads/files')) {
      $row->setSourceProperty('file_dest', str_replace('files', 'documents', $uri));
    }
    else {
      $row->setSourceProperty('file_dest', $uri);
    }

    if (!isset($this->configuration['fields'])) {
      return parent::prepareRow($row);
    }

    $properties = $this->configuration['properties'] ?? [];
    // Make sure requested properties exist in the row.
    foreach ($properties as $prop) {
      $row->setSourceProperty($prop, NULL);
    }

    // Make sure all field properties have the same property name.
    // For example, `field_image_alt` becomes `alt`.
    foreach ($this->getTablesInfo() as $cols) {
      foreach ($cols as $alias => $column) {
        $prop = $row->getSourceProperty($column);
        if ($prop !== NULL) {
          $row->setSourceProperty($alias, $prop);
        }
      }
    }

    return parent::prepareRow($row);
  }

  /**
   * Returns a list of tables and columns according to the plugin configuration.
   */
  protected function getTablesInfo() {
    $fields_info = [];
    $properties = $this->configuration['properties'] ?? [];
    foreach ($this->configuration['fields'] as $field_name) {
      $fields_info["field_data_{$field_name}"]['fid'] = "{$field_name}_fid";

      // Supply requested properties.
      foreach ($properties as $prop) {
        $fields_info["field_data_{$field_name}"][$prop] = "{$field_name}_{$prop}";
      }
    }

    return $fields_info;
  }

}
