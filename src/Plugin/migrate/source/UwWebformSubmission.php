<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\uw_migrate\UwWebformTrait;

/**
 * Modified Drupal 7 webform submission source from database.
 *
 * @MigrateSource(
 *   id = "uw_webform_submission",
 *   source_module = "webform",
 * )
 */
class UwWebformSubmission extends DrupalSqlBase {

  use UwWebformTrait;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('webform_submissions', 'wfs');

    $query->fields('wfs', [
      'nid',
      'sid',
      'uid',
      'submitted',
      'remote_addr',
      'is_draft',
      'serial',
    ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'nid' => $this->t('Webform node Id'),
      'sid' => $this->t('Webform submission Id'),
      'uid' => $this->t('User Id of submitter'),
      'submitted' => $this->t('Submission timestamp'),
      'remote_addr' => $this->t('IP Address of submitter'),
      'is_draft' => $this->t('Whether this submission is draft'),
      'webform_id' => $this->t('Id to be used for Webform'),
      'webform_data' => $this->t('Webform submitted data'),
      'webform_uri' => $this->t('Submission uri'),
      'serial' => $this->t('Serial'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $nid = $row->getSourceProperty('nid');
    $sid = $row->getSourceProperty('sid');
    $submitted_data = $this->buildSubmittedData($sid, $row);
    $row->setSourceProperty('webform_id', 'webform_' . $nid);
    $row->setSourceProperty('webform_data', $submitted_data);
    $row->setSourceProperty('webform_uri', '/node/' . $nid);
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['sid']['type'] = 'integer';
    $ids['sid']['alias'] = 'wfs';
    return $ids;
  }

  /**
   * Build submitted data from webform submitted data table.
   */
  protected function buildSubmittedData($sid, $row) {
    $query = $this->select('webform_submitted_data', 'wfsd');
    $query->innerJoin('webform_component', 'wc', 'wc.nid=wfsd.nid AND wc.cid=wfsd.cid');

    $query->fields('wfsd', [
      'no',
      'data',
    ])->fields('wc', [
      'form_key',
      'extra',
      'type',
      'pid',
    ]);
    $wf_submissions = $query->condition('sid', $sid)->execute();

    $submitted_data = [];
    foreach ($wf_submissions as $wf_submission) {
      $extra = unserialize($wf_submission['extra']);
      $destination_form_key = $element['form_key'] = $this->getDestinationFormKey($wf_submission['form_key'], $wf_submission['pid']);

      $multi_value_types = [
        'grid',
        'uwaddress',
      ];
      $is_multiple = !empty($extra['multiple']) || in_array($wf_submission['type'], $multi_value_types, TRUE);

      $item = $is_multiple
        ? $submitted_data[$destination_form_key] ?? []
        : $wf_submission['data'];

      if ($is_multiple && !empty($wf_submission['data'])) {
        $item[$wf_submission['no']] = $wf_submission['data'];
      }

      if (!empty($item)) {
        $submitted_data[$destination_form_key] = $item;
        $components = $row->getSourceProperty('webform_components');
        $components[$destination_form_key] = $wf_submission['type'];
        $row->setSourceProperty('webform_components', $components);
      }
    }
    return $submitted_data;
  }

}
