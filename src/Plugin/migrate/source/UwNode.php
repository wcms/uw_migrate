<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * Custom source node plugin to filter out unpublished content.
 *
 * @MigrateSource(
 *   id = "uw_node",
 *   source_module = "node"
 * )
 */
class UwNode extends Node {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    if ($this->moduleHandler->moduleExists('pathauto')) {
      $query->leftjoin('pathauto_state', 'ps', 'ps.entity_id = n.nid');
      $query->addField('ps', 'pathauto', 'pathauto');
    }
    $query->condition('n.status', 1);
    $query->orderBy('n.nid');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if (!empty($this->configuration['include_page_width'])) {
      $row->setSourceProperty('wide_page_width', $this->getPageSettings($row));
    }
    return parent::prepareRow($row);
  }

  /**
   * Loads node page settings from "uw_page_settings_node" table.
   *
   * @param \Drupal\migrate\Row $row
   *   The source row.
   */
  protected function getPageSettings(Row $row) {
    $nid = $row->getSourceProperty('nid');
    $vid = $row->getSourceProperty('vid');
    $result = $this->select('uw_page_settings_node', 'ps')
      ->fields('ps')
      ->condition('nid', $nid)
      ->condition('vid', $vid)
      ->execute()
      ->fetchField(2);
    return isset($result) ? (int) $result : 0;
  }

}
