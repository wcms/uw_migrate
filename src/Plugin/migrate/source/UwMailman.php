<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

/**
 * Source plugin for retrieving <ckmailman> tags from content.
 *
 * @MigrateSource(
 *   id = "uw_mailman",
 *   source_module = "system"
 * )
 */
class UwMailman extends UwEmbeddedContent {

  /**
   * {@inheritdoc}
   */
  protected function getItems($content) {
    return $this->parser->getMailmanTags($content);
  }

}
