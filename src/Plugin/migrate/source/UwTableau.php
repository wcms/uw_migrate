<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

/**
 * Source plugin for retrieving embedded <tableau> tags from content value.
 *
 * @MigrateSource(
 *   id = "uw_tableau",
 *   source_module = "system"
 * )
 */
class UwTableau extends UwEmbeddedContent {

  /**
   * {@inheritdoc}
   */
  protected function getItems($content) {
    return $this->parser->getTableauTags($content);
  }

}
