<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Source plugin for retrieving embedded <uw-ec-group> tags from content.
 *
 * @MigrateSource(
 *   id = "uw_expand_collapse_group",
 *   source_module = "system"
 * )
 */
class UwExpandCollapseGroup extends UwEmbeddedContent {

  use UwMigrateTrait;

  /**
   * {@inheritDoc}
   */
  public function initializeIterator() {
    $rows = parent::initializeIterator();
    $content_col = $this->configuration['content_column'];
    $ecs = [];
    foreach ($rows as $row) {
      $layout_items = $this->parser->getLayoutItems($row[$content_col]);
      foreach ($layout_items as $page_section_delta => $section) {
        foreach ($section['regions'] as $region) {
          $all_ecs = $this->parser->getExpandCollapse($region['content']);
          foreach ($all_ecs as $ec_page_section_delta => $ec) {
            $ecs[] = array_merge(
              $row,
              [
                'page_section_delta' => $page_section_delta,
                'component_delta' => $ec_page_section_delta,
                'component_section_delta' => 0,
              ],
              $ec,
            );
          }
        }
      }
    }
    return new \ArrayIterator($ecs);
  }

}
