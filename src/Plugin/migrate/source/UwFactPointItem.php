<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Custom field collection item plugin for fact points.
 *
 * @MigrateSource(
 *   id = "uw_fact_point_item",
 *   source_module = "field_collection",
 * )
 */
class UwFactPointItem extends UwFieldCollectionItem {

  const ICON_TYPE = 4;

  const INFOGRAPHIC_TYPE = 5;

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $allValues = [];
    $result = parent::prepareRow($row);

    if ($icon = $row->getSourceProperty('field_eff_hf_icon')) {
      $allValues[] = [
        'field_eff_hf_text_type_of_text' => [
          0 => [
            // Add icon type 4 by default.
            'value' => self::ICON_TYPE,
          ],
        ],
        'field_eff_hf_icon' => $icon,
      ];
    }

    if ($row->getSourceProperty('field_eff_hf_fact')) {
      $allValues[] = [
        'field_eff_hf_text_type_of_text' => [
          0 => [
            // Add icon type 4 by default.
            'value' => self::INFOGRAPHIC_TYPE,
          ],
        ],
        'field_eff_hf_fact' => $row->getSourceProperty('field_eff_hf_fact'),
        'field_eff_hf_type_of_graphic' => $row->getSourceProperty('field_eff_hf_type_of_graphic'),
        'field_infographic_fact_suffix' => $row->getSourceProperty('field_infographic_fact_suffix'),
      ];
    }

    $texts = $row->getSourceProperty('field_eff_hf_text');
    if (!empty($texts)) {
      // Get field values of nested field_eff_hf_text field collection.
      $field_names = array_keys($this->getFields('field_collection_item', 'field_eff_hf_text'));
      foreach ($texts as $text) {
        $item_id = $text['value'];
        $revision_id = $text['revision_id'];

        $value = [];
        foreach ($field_names as $field_name) {
          $field_value = $this->getFieldValues('field_collection_item', $field_name, $item_id, $revision_id);
          $value[$field_name] = $field_value;
        }

        $allValues[] = $value;
      }
    }

    $row->setSourceProperty('field_eff_hf_text', $allValues);
    return $result;
  }

}
