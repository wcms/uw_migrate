<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\uw_migrate\ContentTransformers\ExpandCollapseTransformer;
use Drupal\uw_migrate\UwContentParser;
use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Source plugin for retrieving and formatting content sections.
 *
 * @MigrateSource(
 *   id = "uw_content_complete",
 *   source_module = "node"
 * )
 */
class UwContentComplete extends UwEmbeddedContent {

  use UwMigrateTrait;

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $new_rows = [];

    foreach ($iterator as $row) {
      $new_rows = array_merge(
        $new_rows,
        self::buildContentLayoutRow($row, $this->configuration, $this->parser)
      );
    }

    // Return iterator with new rows.
    return new \ArrayIterator($new_rows);
  }

  /**
   * Build the layout rows.
   *
   * @param array $row
   *   Row to be processed.
   * @param array $configuration
   *   Migration configuration data.
   * @param \Drupal\uw_migrate\UwContentParser $parser
   *   Instance from uw content parse to be used on parses.
   * @param bool $is_nested_row
   *   Flag to build the row as a sub content.
   *
   * @return array
   *   Array of rows build with some data informing row position.
   */
  public static function buildContentLayoutRow(array $row, array $configuration, UwContentParser $parser, $is_nested_row = FALSE): array {
    $new_rows = [];
    $format_col = $configuration['format_column'] ??
      str_replace('value', 'format', $configuration['content_column']);
    $layout_sections = $parser->getLayoutItems($row[$configuration['content_column']]);
    $last_sub_content_group = NULL;
    foreach ($layout_sections as $page_section_delta => $section) {
      foreach ($section['regions'] as $component_delta => $region) {
        $component_region_delta = 0;
        $section_components = $parser->getContentSections($region['content']);
        foreach ($section_components as $component_section_delta => $content) {
          $new_row = $row;
          $content_props = $parser->getElementProperties($content);

          // Get properly html content for rows that refers to an e/c item.
          $is_uwec_block_content = !empty($content_props)
            && $content_props['html_tag'] == ExpandCollapseTransformer::EC_GROUP_TAG;
          if ($is_uwec_block_content) {
            $content = $content_props['value'] ?? $content;
            $new_row += $content_props;
          }

          if (!static::isApplicable($content, $parser, $configuration)) {
            $component_region_delta++;
            continue;
          }

          $new_row['value'] = $content;
          $new_row['format'] = $row[$format_col] ?? NULL;
          $new_row['region'] = $region['region'];
          $new_row['layout'] = $section['layout'];
          $new_row['page_section_delta'] = $page_section_delta;
          $new_row['component_delta'] = $component_delta;
          $new_row['component_section_delta'] = $component_section_delta;
          $new_row['component_region_delta'] = $component_region_delta;
          $new_row['ref_id'] = 0;
          if ($is_nested_row) {
            $new_row['page_section_delta'] = $row['page_section_delta'];
            $new_row['component_delta'] = $row['component_section_delta'];
            $new_row['component_section_delta'] = $page_section_delta;
          }
          $new_row['section'] = $page_section_delta;

          if ($parser->isEmbeddedTag($content)) {
            // Extend row with values from element attributes.
            // Attributes and properties described in UwMigrateTrait.
            // @see \Drupal\uw_migrate\UwMigrateTrait::getAttributesMapping().
            $new_row = array_merge($new_row, $parser->getElementProperties($content));
          }

          // For expand collapse items needs to be validated before being added.
          if ($is_uwec_block_content) {
            // Check if the group is different to avoid duplicated e/c rows.
            if ($last_sub_content_group !== $content_props['group']) {
              // Set -1 for these values.
              // To avoid being found by other migrations.
              $new_rows[] = array_merge(
                $new_row,
                [
                  'component_section_delta' => -1,
                  'group' => -1,
                ]
              );
              $last_sub_content_group = $content_props['group'];
              $component_region_delta++;
            }
          }
          else {
            $new_rows[] = $new_row;
            $component_region_delta++;
          }

          // Merge into $new_rows the nested rows.
          if (static::hasNestedRows($content_props)) {
            $new_config = array_merge($configuration, ['content_column' => 'value']);
            $new_rows = array_merge(
              $new_rows,
              self::buildContentLayoutRow($new_row, $new_config, $parser, TRUE)
            );
          }
        }
      }
    }
    return $new_rows;
  }

  /**
   * Check if there is nested rows on the current html string.
   *
   * At this moment is considered only uw-ec-group tags.
   *
   * @param array $props
   *   The element props.
   *
   * @return bool
   *   True for sub content. Otherwise, false.
   */
  public static function hasNestedRows(array $props = []): bool {
    if (empty($props)) {
      return FALSE;
    }
    $valid_sub_content_tags = [ExpandCollapseTransformer::EC_GROUP_TAG];
    return !empty($props['html_tag']) && in_array($props['html_tag'], $valid_sub_content_tags);
  }

  /**
   * Identifies if current piece of content is applicable to current migration.
   */
  public static function isApplicable($content, $parser, $configuration) {
    $is_embedded = $parser->isEmbeddedTag($content);
    $fetch_all = !empty($configuration['fetch_all']);

    // Filter out embedded tags if we don't need all items.
    return $fetch_all || !$is_embedded;
  }

}
