<?php

namespace Drupal\uw_migrate;

use Drupal\migrate\Plugin\MigrateSourceInterface;

/**
 * Provides some helper methods for custom source plugins.
 */
trait UwMigrateSourceTrait {

  /**
   * Returns a stub migration with given source plugin definition.
   *
   * @param array $source
   *   Array with source plugin definition.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface
   *   Migration object.
   */
  protected function getStubMigration(array $source) {
    $definition = [
      'source' => $source,
      'process' => [],
      'destination' => ['plugin' => 'null'],
    ];

    return \Drupal::service('plugin.manager.migration')
      ->createStubMigration($definition);
  }

  /**
   * Returns a list of source rows.
   */
  protected function getSourceRows(MigrateSourceInterface $source) {
    $rows = [];

    $source->checkRequirements();
    $source->rewind();
    while ($source->valid()) {
      /** @var \Drupal\migrate\Row $row */
      $row = $source->current();
      $rows[] = $row->getSource();
      $source->next();
    }
    return $rows;
  }

}
