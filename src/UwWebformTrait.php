<?php

namespace Drupal\uw_migrate;

/**
 * Helper methods for UW Webform migrations.
 */
trait UwWebformTrait {

  /**
   * Returns destination form_key for the given webform element.
   */
  protected function getDestinationFormKey($form_key, $pid) {
    // Not sure why/how, but D7 form keys may have commas.
    $form_key = str_replace([',', '.'], '_', $form_key);

    // Truncate name if it's too long (the limit is 128 characters).
    if (strlen($form_key) > 128) {
      $form_key = substr($form_key, 128);
    }

    // We might get element with same form_key.
    // However, if form_key is gonna be more than 128 chars, then keep it as-is.
    if (strlen($form_key) < 128 && !empty($pid)) {
      $new_form_key = $form_key . '_' . $pid;
      $form_key = $new_form_key > 128 ? $form_key : $new_form_key;
    }
    return strtolower($form_key);
  }

}
