<?php

namespace Drupal\uw_migrate;

use Drupal\uw_migrate\ContentTransformers\ExpandCollapseTransformer;

/**
 * Provides mapping and some helper methods for UW migrations.
 */
trait UwMigrateTrait {

  /**
   * Provides mapping between custom tags and new block types.
   */
  public function getTagsMapping() {
    return [
      'ckcalltoaction' => 'uw_cbl_call_to_action',
      // 'ckcodepen' => '',
      'ckembeddedmaps' => 'uw_cbl_google_maps',
      'ckfacebook' => 'uw_cbl_facebook',
      'ckfactsfigures' => 'uw_cbl_facts_and_figures',
      'ckimagegallery' => 'uw_cbl_image_gallery',
      'ckmailchimp' => 'uw_cbl_mailchimp',
      'ckmailman' => 'uw_cbl_mailman',
      'ckpowerbi' => 'uw_cbl_powerbi',
      'cktableau' => 'uw_cbl_tableau',
      'cktimeline' => 'uw_cbl_timeline',
      // 'cktint' => '',
      'cktwitter' => 'uw_cbl_twitter',
      'ckvimeo' => 'uw_cbl_remote_video',
      'uwvideo' => 'uw_cbl_remote_video',
      'blockquote' => 'uw_cbl_blockquote',
      ExpandCollapseTransformer::EC_GROUP_TAG => 'uw_cbl_expand_collapse',
    ];
  }

  /**
   * Contains a list of configuration blocks.
   *
   * These blocks act like other embedded items, but they don't require extra
   * migrations and being created during the layout builder migration.
   */
  public function getConfigBlocks() {
    return [
      'uw_cbl_twitter', 'uw_cbl_facebook', 'uw_cbl_expand_collapse',
    ];
  }

  /**
   * Returns regex pattern for embedded tags.
   *
   * @return string
   *   Regex pattern.
   *
   * @see https://regex101.com/r/k2GgLa/1
   */
  public function getTagsRegex() {
    $tags = array_keys($this->getTagsMapping());
    $self = array_diff($tags, $this->getNonSelfClosedTags());

    return '/<' . $this->orGroup($self) . '[^>]*\/.*>|<' . $this->orGroup($tags) . '.*>.*<\/.*' . $this->orGroup($tags) . '\s*>/Us';
  }

  /**
   * Get allowed custom tags.
   *
   * @return string[]
   *   Array of tag names.
   */
  public function getAllowedTags() {
    $tags = array_keys($this->getTagsMapping());
    return $tags + $this->getNonSelfClosedTags();
  }

  /**
   * List of non-self closed tags.
   *
   * @return string[]
   *   Array of tags that can't be self closed.
   */
  public function getNonSelfClosedTags() {
    return [
      'blockquote',
      ExpandCollapseTransformer::EC_GROUP_TAG,
      ExpandCollapseTransformer::EC_GROUP_BLOCK_TAG,
      'ckmailchimp',
    ];
  }

  /**
   * Helper to create regex OR group from array of items.
   */
  public function orGroup($items) {
    return '(' . implode('|', $items) . ')';
  }

  /**
   * Contains mapping between custom tags attributes and source row properties.
   */
  public function getAttributesMapping() {
    return [
      'ckcalltoaction' => [
        'data-calltoaction-nid' => 'ref_id',
      ],
      'cktimeline' => [
        'data-timeline-nid' => 'ref_id',
      ],
      'ckfactsfigures' => [
        'data-factsfigures-nid' => 'ref_id',
      ],
      'ckimagegallery' => [
        'data-imagegallerynid' => 'ref_id',
        'data-gallerytype' => 'gallery_type',
      ],
      'uwvideo' => [
        'href' => 'ref_id',
      ],
      'ckvimeo' => [
        'data-url' => 'ref_id',
      ],
      'ckembeddedmaps' => [
        'data-src' => 'ref_id',
        'data-height' => 'height',
      ],
      'ckmailman' => [
        'data-listname' => 'ref_id',
        'data-servername' => 'server_name',
      ],
      'ckpowerbi' => [
        'data-powerbi-id' => 'ref_id',
      ],
      'ckfacebook' => [
        'data-type' => 'fb_feed_type',
        'data-fburl' => 'fb_url',
        'data-username' => 'fb_page',
      ],
      'cktableau' => [
        'data-url' => 'ref_id',
        'data-height' => 'height',
        'data-server' => 'server',
        'data-site' => 'site_name',
        'data-tabs' => 'display_tabs',
      ],
      'cktwitter' => [
        'data-listname' => 'twitter_listname',
        'data-tweet' => 'twitter_tweet_code',
        'data-type' => 'twitter_feed_type',
        'data-username' => 'twitter_username',
      ],
      'blockquote' => [],
      ExpandCollapseTransformer::EC_GROUP_BLOCK_TAG => [
        'data-ec-section-index' => 'ec-section-index',
      ],
      ExpandCollapseTransformer::EC_GROUP_TAG => [
        'title' => 'title',
        'data-group-index' => 'group-index',
      ],
      'ckmailchimp' => [
        'data-sourcecode' => 'sourcecode',
      ],
    ];
  }

  /**
   * Contains mapping between custom CSS classes and layout builder section.
   */
  public function getLayoutMapping() {
    $layouts = [
      'threecol-33' => [
        'layout' => 'uw_3_column',
        'layout_settings' => [
          'column_class' => 'even-split',
        ],
        'regions' => 3,
      ],
      'col-50' => [
        'layout' => 'uw_2_column',
        'layout_settings' => [
          'column_class' => 'even-split',
        ],
        'regions' => 2,
      ],
      'col-33' => [
        'layout' => 'uw_2_column',
        'layout_settings' => [
          'column_class' => 'larger-right',
        ],
        'regions' => 2,
      ],
      'col-66' => [
        'layout' => 'uw_2_column',
        'layout_settings' => [
          'column_class' => 'larger-left',
        ],
        'regions' => 2,
      ],
    ];

    // <div class="fcol three-a-1…"> layouts.
    for ($i = 1; $i <= 3; $i++) {
      $layouts["three-a-{$i}"] = [
        'layout' => 'uw_3_column',
        'layout_settings' => [
          'column_class' => 'legacy-38-38-24',
        ],
        'regions' => 3,
      ];
    }

    // <div class="fcol three-1…"> layouts.
    for ($i = 1; $i <= 3; $i++) {
      $layouts["three-{$i}"] = [
        'layout' => 'uw_3_column',
        'layout_settings' => [
          'column_class' => 'legacy-24-38-38',
        ],
        'regions' => 3,
      ];
    }

    // <div class="fcol four-1…"> layouts.
    for ($i = 1; $i <= 4; $i++) {
      $layouts["four-{$i}"] = [
        'layout' => 'uw_4_column',
        'layout_settings' => [
          'column_class' => 'legacy-23-27-27-23',
        ],
        'regions' => 4,
      ];
    }

    // <div class="fcol five-1…"> layouts.
    for ($i = 1; $i <= 5; $i++) {
      $layouts["five-{$i}"] = [
        'layout' => 'uw_5_column',
        'layout_settings' => [
          'column_class' => 'legacy-23-19-19-19-20',
        ],
        'regions' => 5,
      ];
    }

    return $layouts;
  }

  /**
   * Returns settings for default layout builder section.
   */
  public function getDefaultLayout() {
    return [
      'layout' => 'uw_1_column',
      'layout_settings' => [],
      'regions' => 1,
    ];
  }

  /**
   * Returns layout builder settings for the given layout class.
   *
   * @param string $class
   *   Css class of D7 layout (i.e. threecol-33).
   *
   * @return array
   *   Layout builder settings for the matched layout.
   */
  public function getLayoutSettings($class) {
    $layouts = $this->getLayoutMapping();
    if (isset($layouts[$class])) {
      return $layouts[$class];
    }
    return $this->getDefaultLayout();
  }

  /**
   * Returns regex pattern for retrieving layout sections.
   *
   * @return string[]
   *   Regex pattern.
   *
   * @see https://regex101.com/r/2FvaRw/1
   */
  public function getLayoutsRegex() {
    return array_keys($this->getLayoutMapping());
  }

  /**
   * Return the entity id from an uri.
   *
   * Examples of uri and return value:
   *  node/1: ['node', 1]
   *  taxonomy/term/2: ['taxonomy/term', 2]
   *
   * @param string $uri
   *   The uri to be extract the entity id.
   *
   * @return array
   *   Entity if found.
   */
  public function extractEntityInfoFromUri(string $uri): ?array {
    $matches = [];
    if (preg_match('/node\/(\d+)/', $uri, $matches)) {
      return [
        'entity_type' => 'node',
        'id' => $matches[1],
      ];
    }
    elseif (preg_match('/taxonomy\/term\/(\d+)/', $uri, $matches)) {
      return [
        'entity_type' => 'taxonomy/term',
        'id' => $matches[1],
      ];
    }
    return [];
  }

}
