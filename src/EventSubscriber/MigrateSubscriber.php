<?php

namespace Drupal\uw_migrate\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\pathauto\PathautoGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Database\Database;
use Drupal\redirect\Entity\Redirect;
use Psr\Log\LoggerInterface;

/**
 * Event subscriber for custom migrations.
 */
class MigrateSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValue;

  /**
   * The pathauto generator.
   *
   * @var \Drupal\pathauto\PathautoGeneratorInterface
   */
  protected $pathautoGenerator;
  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs an instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value
   *   The key value factory.
   * @param \Drupal\pathauto\PathautoGeneratorInterface $pathauto_generator
   *   The pathauto generator.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, KeyValueFactoryInterface $key_value, PathautoGeneratorInterface $pathauto_generator, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->keyValue = $key_value;
    $this->pathautoGenerator = $pathauto_generator;
    $this->logger = $logger;
  }

  /**
   * Saves pathauto states.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The post row save event.
   */
  public function onPostRowSave(MigratePostRowSaveEvent $event): void {
    $migration = $event->getMigration();
    $row = $event->getRow();

    $entity = $this->checkMigration($migration, $row);
    if (!$entity) {
      return;
    }

    $this->updateEntityAlias($entity, $row);
    $this->updateWebformSerial($entity, $row);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MigrateEvents::POST_ROW_SAVE => ['onPostRowSave'],
    ];
  }

  /**
   * Determines if the provided migration is in a predefined list.
   *
   * @param object $migration
   *   The migration object to check against the predefined list.
   * @param object $row
   *   The migration row containing the old path information.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   If the migration is in the predefined list, or NULL otherwise.
   */
  private function checkMigration($migration, $row) {
    $migrations = [
      'uw_url_alias' => [
        'entity_type' => 'node',
        'id_key' => 'new_nid',
      ],
      'uw_url_alias_taxonomy' => [
        'entity_type' => 'taxonomy_term',
        'id_key' => 'new_tid',
      ],
      'uw_webform' => [
        'entity_type' => 'webform',
        'id_key' => 'id',
      ],
    ];

    if (!isset($migrations[$migration->id()])) {
      return;
    }

    // Load entity.
    $entity_type = $migrations[$migration->id()]['entity_type'];
    $entity_id = $row->getDestinationProperty($migrations[$migration->id()]['id_key']);
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);

    return $entity;
  }

  /**
   * Updates the entity alias and creates a redirect if needed.
   *
   * This function updates the alias of a given entity and creates a redirect
   * from the old alias to the new one. It takes into consideration the pathauto
   * state of the entity.
   *
   * @param object $entity
   *   The entity to update the alias for.
   * @param object $row
   *   The migration row containing the old path information.
   */
  public function updateEntityAlias($entity, $row) {
    // Get old entity id.
    $source = explode('/', $row->getSourceProperty('source'));
    $id = $entity->getEntityTypeId() === 'node' ? $source[1] : $source[2];

    // Get pathauto state.
    $db = Database::getConnection('default', 'migrate');
    $pathauto = $db->select('pathauto_state', 'p')
      ->fields('p', ['pathauto'])
      ->condition('entity_id', $id)
      ->execute()->fetchField();

    if ($pathauto) {
      // Get wcsm2 path.
      $old_alias = $entity->path->alias;

      // Set pathaouto to regenerate alias with the new patterns.
      $entity->set("path", ["pathauto" => TRUE]);
      $entity->save();

      $this->updateAliasRedirect($entity, $old_alias);
    }
  }

  /**
   * Creates a redirect from the old alias to the new alias.
   *
   * This function creates a redirect from the old alias to the new alias
   * for a given entity if the new alias is different from the old one.
   * If there is an error while creating the redirect, it will be logged.
   *
   * @param object $entity
   *   The entity for which the redirect is being created.
   * @param string $old_alias
   *   The old alias path to redirect from.
   */
  public function updateAliasRedirect($entity, $old_alias) {
    // Get new alias and create redirection from old path to new path.
    $entity = $this->entityTypeManager->getStorage($entity->getEntityTypeId())->load($entity->id());
    $new_alias = $entity->path->alias;

    if ($new_alias === $old_alias) {
      return;
    }

    try {
      $entity_path = $entity->getEntityTypeId() == 'node' ? $entity->getEntityTypeId() : 'taxonomy/term';
      // Create a redirect from the old alias to the new alias.
      $redirect = Redirect::create([
        'redirect_source' => substr($old_alias, 1),
        'redirect_redirect' => 'internal:/' . $entity_path . '/' . $entity->id(),
        'language' => $entity->language()->getId(),
        'status_code' => 301,
      ]);
      $redirect->save();
    }
    catch (\Exception $e) {
      $this->logger->notice("error creating redirect from $old_alias to $new_alias: " . $e->getMessage());
    }
  }

  /**
   * Updates webform next serial.
   *
   * @param object $entity
   *   The entity to update the alias for.
   * @param object $row
   *   The migration row containing the old path information.
   */
  public function updateWebformSerial($entity, $row) {
    if ($entity->bundle() == 'webform') {
      /** @var \Drupal\webform\WebformEntityStorageInterface $webform_storage */
      $webform_storage = $this->entityTypeManager->getStorage('webform');
      $webform_storage->setNextSerial($entity, $row->getSourceProperty('next_serial'));
    }
  }

}
