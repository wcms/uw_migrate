<?php

namespace Drupal\uw_migrate;

use Drupal\Component\Utility\Html;
use Drupal\uw_migrate\ContentTransformers\ExpandCollapseTransformer;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Provides methods to parse and retrieve special tags and layouts.
 */
class UwContentParser {

  use UwMigrateTrait;

  /**
   * Returns a list of content sections for the given HTML string.
   *
   * Content sections will be split by delimiters, found by given regex.
   *
   * @see \Drupal\uw_migrate\UwContentParser::getHtmlSections()
   */
  public function getContentSections($content, $customTags = TRUE, $include_delimiters = TRUE) {
    // Identify whether content contains any delimiters. They will be used to
    // split the original HTML markup.
    $delimiters = $this->getHtmlSections($content, $customTags);

    $sections = [];
    while (!empty($delimiters)) {
      $delimiter = array_shift($delimiters);
      // Limit the function explode in two items.
      // Ensuring that the rest of the content will be on the 1 position.
      // That is used to update the variable content.
      $parts = explode($this->normalize($delimiter), $this->normalize($content), 2);

      // Append previous section if there is some meaningful content there.
      if ($this->clean($parts[0])) {
        $sections[] = $parts[0];
      }

      // Append delimiter if requested.
      if ($include_delimiters) {
        $sections[] = $this->clean($delimiter, FALSE);
      }

      // Continue with the remaining content.
      if (isset($parts[1])) {
        $content = $parts[1];
      }
    }

    // This is the original content if delimiters are not available or
    // remaining content after the last content split.
    // We're not performing any normalization in this case.
    if ($this->clean($content)) {
      $sections[] = $content;
    }

    // Normalize content items (mainly to fix HTML markup).
    // We skip embedded self-closed tags to preserve original markup.
    foreach ($sections as &$section) {
      if (!$this->isEmbeddedTag($section)) {
        $section = $this->normalize($section);
      }
    }
    return $sections;
  }

  /**
   * Returns a list of requested HTML sections.
   */
  public function getHtmlSections($content, $customTags) {
    // Add html and body tags to crawler works better.
    $crawler = new Crawler('<html><body>' . $content . '</body></html>');

    // Adding tag that wrap e/c tags to be considered as section.
    // @todo think in another way to do that, but this tags must be considered as section.
    $group = !$customTags ? ',' . ExpandCollapseTransformer::EC_GROUP_BLOCK_TAG : '';
    $sections_selector = '.' . implode(', .', $this->getLayoutsRegex()) . $group;
    if ($customTags) {
      $sections_selector .= ',' . implode(',', $this->getAllowedTags());
    }

    $sections = [];
    $crawler->filter($sections_selector)->each(function (Crawler $node) use ($sections_selector, &$sections) {
      // Check if the current element section is a nested.
      // That shouldn't be migrated as a new section.
      $isNestedSection = $node->parents()
        ->closest($sections_selector);
      $htmlContent = $node->outerHtml();
      // Ignore nested sections or empty sections.
      if ($htmlContent !== '' && !$isNestedSection) {
        $sections[] = $htmlContent;
      }
    });
    return $sections;
  }

  /**
   * Returns a list of parsed elements for the given selector.
   */
  public function getEmbeddedItems($content, $selector) {
    $result = [];
    $crawler = new Crawler($content);

    /** @var \DOMElement $element */
    foreach ($crawler->filter($selector) as $element) {
      $result[] = $this->getElementProperties($element);
    }
    return $result;
  }

  /**
   * Returns a list of URLs of embedded videos.
   */
  public function getVideos($content) {
    return $this->getEmbeddedItems($content, 'uwvideo,ckvimeo');
  }

  /**
   * Returns a list of expand collapse elements.
   */
  public function getExpandCollapse($content, $onlyContent = FALSE) {
    $result = $this->getEmbeddedItems($content, ExpandCollapseTransformer::EC_GROUP_TAG);
    if ($onlyContent) {
      return array_column($result, 'value');
    }
    return $result;
  }

  /**
   * Returns a list of embedded maps.
   */
  public function getMaps($content) {
    return $this->getEmbeddedItems($content, 'ckembeddedmaps');
  }

  /**
   * Returns a list of embedded mailmain lists.
   */
  public function getMailmanTags($content) {
    return $this->getEmbeddedItems($content, 'ckmailman');
  }

  /**
   * Returns a list of embedded mailchimp lists.
   */
  public function getMailchimpTags($content) {
    return $this->getEmbeddedItems($content, 'ckmailchimp');
  }

  /**
   * Returns a list of embedded Power BI items.
   */
  public function getPowerBiTags($content) {
    return $this->getEmbeddedItems($content, 'ckpowerbi');
  }

  /**
   * Returns a list of embedded Tableau items.
   */
  public function getTableauTags($content) {
    return $this->getEmbeddedItems($content, 'cktableau');
  }

  /**
   * Returns a list of embedded CTA items.
   */
  public function getCallToActions($content) {
    return $this->getEmbeddedItems($content, 'ckcalltoaction');
  }

  /**
   * Returns a list of embedded CTA items.
   */
  public function getImageGalleries($content) {
    return $this->getEmbeddedItems($content, 'ckimagegallery');
  }

  /**
   * Returns a list of blockquote content sections.
   */
  public function getBlockquotes($content) {
    $result = [];
    $crawler = new Crawler($content);

    /** @var \DOMElement $element */
    foreach ($crawler->filter('blockquote') as $element) {
      $quote_content = $this->getInnerHtml($element);
      $matches = [];
      // Detect blockquote attribution. It can be either inside the <footer> or
      // <em> HTML tag.
      preg_match('/<(footer|em)>.*<\/(footer|em)>/Us', $quote_content, $matches);

      // These tags will be stripped from attribution content.
      $attr_tags = ['<footer>', '</footer>', '<cite>', '</cite>'];
      $result[] = [
        // Strip out the attribution from quote content.
        'content' => empty($matches[0]) ? $quote_content : trim(str_replace($matches[0], '', $quote_content)),
        'attribution' => empty($matches[0]) ? '' : trim(str_replace($attr_tags, '', $matches[0])),
      ];
    }
    return $result;
  }

  /**
   * Returns a list of formatted content items and their layout settings.
   */
  public function getLayoutItems($content) {
    $result = [];
    $layout_sections = $this->getContentSections($content, FALSE);

    /** @var \DOMElement $element */
    $section = [];
    foreach ($layout_sections as $layout_section) {
      $layout_data = $this->getSectionData($layout_section);

      // Skip if section content is empty.
      if (empty($this->clean($layout_section))) {
        continue;
      }

      // Create a new section if it's empty or not applicable.
      if (empty($section) || $layout_data['layout'] === 'uw_1_column' || $section['layout'] !== $layout_data['layout']) {
        $section = [
          'layout' => $layout_data['layout'],
          'layout_settings' => $layout_data['layout_settings'],
          'regions' => [],
        ];
      }

      // Append content of current DOM element.
      $section['regions'][] = [
        'region' => $this->getRegionName(count($section['regions'])),
        'content' => $this->normalize($layout_data['content']),
      ];

      // If the section is already full, append it to the layout and clear.
      if (count($section['regions']) === $layout_data['regions']) {
        $result[] = $section;
        $section = [];
      }
    }

    // Append the last section in case it wasn't complete in the loop above.
    // It might happen when we didn't find all expected columns.
    if (!empty($section)) {
      $result[] = $section;
    }

    return $result;
  }

  /**
   * Returns the ID attribute value for the given element.
   *
   * ID attribute is taken from self::getAttributesMapping() and expected to be
   * mapped to `ref_id` row property.
   */
  public function getElementId(\DOMElement $element) {
    $attributes = $this->getAttributesMapping();
    if (isset($attributes[$element->nodeName])) {
      foreach ($attributes[$element->nodeName] as $attr => $row_property) {
        if ($row_property === 'ref_id') {
          return $element->getAttribute($attr);
        }
      }
    }
    return 0;
  }

  /**
   * Returns element properties based on HTML attributes.
   *
   * @param string|\DOMElement $element
   *   A piece of HTML content or DOMElement object.
   *
   * @return array
   *   A list of retrieved properties.
   */
  public function getElementProperties($element) {
    $properties = [];
    $attributes = $this->getAttributesMapping();
    if (!$element instanceof \DOMElement) {
      $crawler = new Crawler($element);
      $selector = implode(',', array_keys($attributes));
      $element = $crawler->filter($selector)->getNode(0);
    }

    // ref_id property should be always available.
    if ($element instanceof \DOMElement && isset($attributes[$element->nodeName])) {
      $properties['html_tag'] = $element->nodeName;
      foreach ($attributes[$element->nodeName] as $attr => $row_property) {
        if ($element->hasAttribute($attr)) {
          $properties[$row_property] = trim($element->getAttribute($attr));
        }
      }
    }

    if (isset($properties['html_tag'])) {
      // For Facebook, add either url or page as ref_id property.
      if ($properties['html_tag'] === 'ckfacebook') {
        $properties['ref_id'] = $properties['fb_url'] ?? $properties['fb_page'];

        // Make sure widget type is not empty.
        if (empty($properties['fb_feed_type'])) {
          $properties['fb_feed_type'] = empty($properties['fb_url']) ? 'timeline' : 'singlepost';
        }
      }
      elseif ($properties['html_tag'] === 'cktwitter') {
        $properties['tweet'] = $properties['tweet'] ?? '';
        $tweet_parts = $this->getTwitterParts($properties['tweet']);

        // Supply username, if empty.
        if (empty($properties['twitter_username']) && !empty($tweet_parts['username'])) {
          $properties['twitter_username'] = $tweet_parts['username'];
        }
        // Use either url or username as ref_id property.
        $properties['ref_id'] = $properties['tweet'] ?? $properties['twitter_username'];
      }
      elseif ($properties['html_tag'] === ExpandCollapseTransformer::EC_GROUP_TAG) {
        $properties['value'] = $this->getInnerHtml($element);
        $properties['group'] = (int) $element->getAttribute('data-group-index') ?? 0;
        $properties['has_embedded_tag'] = $this->isEmbeddedTag($properties['value']);
        // If is an embedded tag we need to merge these attributes on the row.
        if ($properties['has_embedded_tag']) {
          $properties = array_merge($this->getElementProperties($properties['value']), $properties);
        }
      }
    }

    return $properties;
  }

  /**
   * Detects if given HTML snippet is embedded tag.
   *
   * @param string $content
   *   HTML snippet, usually an item from self::getContentSections().
   *
   * @return bool
   *   TRUE if this embedded tag and FALSE otherwise.
   */
  public function isEmbeddedTag($content) {
    return (bool) preg_match($this->getTagsRegex(), $content);
  }

  /**
   * Removes HTML tags, whitespaces and linebreaks from HTML snippet.
   */
  public function clean($content, $strip_tags = TRUE) {
    // Replace a commonly used non-breaking space: &nbsp.
    // https://stackoverflow.com/a/40724830
    $content = str_replace("\xc2\xa0", ' ', $content);

    // Carriage Return.
    $content = str_replace(["\r", "\n"], '', $content);
    // Line Feed.
    $content = str_replace(["\n", ""], '', $content);
    // Tab.
    $content = str_replace(["\t", ""], '', $content);
    // Remove whitespaces.
    $content = str_replace('&nbsp;', ' ', $content);

    // Replace &lt; &gt; used as text for placeholders to avoid stripping them.
    // in the remove special chars below.
    $content = str_replace('&lt;', '___LT___', $content);
    $content = str_replace('&gt;', '___GT___', $content);

    // Remove other special characters.
    $content = preg_replace("/&#?[a-z0-9]{2,8};/i", '', $content);

    if ($strip_tags) {
      $allowed_tags = array_merge([
        'a', 'img', 'iframe',
      ], array_keys($this->getTagsMapping()));

      // Allow embedding tags.
      $allowed_tags_list = array_map(function ($item) {
        return "<$item>";
      }, $allowed_tags);

      $content = strip_tags($content, implode('', $allowed_tags_list));
    }

    // Replace the placeholders with the &lt; &gt; codes.
    $content = str_replace('___LT___', '&lt;', $content);
    $content = str_replace('___GT___', '&gt;', $content);

    return trim($content);
  }

  /**
   * Normalizes an HTML snippet and removes whitespaces.
   */
  protected function normalize($content) {
    // Close un-closed tags and fix other markup issues.
    $content = Html::normalize(trim($content));
    // Simplify new lines to avoid unnecessary empty lines.
    return str_replace(["\r\n", "\r"], "\n", trim($content));
  }

  /**
   * Returns inner HTML for the given element.
   */
  protected function getInnerHtml(\DOMElement $element) {
    $innerHTML = '';
    $children = $element->childNodes;
    foreach ($children as $child) {
      $innerHTML .= $child->ownerDocument->saveXML($child);
    }

    // saveXML() encodes HTML entities, so we return it back to original.
    $innerHTML = Html::decodeEntities($innerHTML);

    return trim($innerHTML);
  }

  /**
   * Parses tweet parts from given embedded tweet code.
   */
  public function getTwitterParts($tweet) {
    $parts = [
      'username' => '',
      'code' => '',
    ];

    $url = trim(parse_url($tweet, PHP_URL_PATH), '/');
    $url_parts = explode('/', $url);
    if (empty($url_parts)) {
      return $parts;
    }

    // First part should be always username.
    $parts['username'] = $url_parts[0];
    // If the 2nd part is "status", the next one should tweet code.
    if (!empty($url_parts[1]) && $url_parts[1] === 'status') {
      $parts['code'] = $url_parts[2];
    }

    return $parts;
  }

  /**
   * Returns layout builder section data for the given HTML string.
   *
   * Section data will include layout name, settings and amount of available
   * regions.
   *
   * @param string $content
   *   A piece of HTML content.
   *
   * @return array
   *   Layout builder section data.
   */
  protected function getSectionData($content) {
    $props = $this->getElementProperties($content);
    if (!empty($props) && $props['html_tag'] == ExpandCollapseTransformer::EC_GROUP_BLOCK_TAG) {
      return $this->getLayoutSettings('default') + ['content' => $content];
    }
    $layouts = $this->getLayoutMapping();
    $classes = array_map(function ($class) {
      return ".{$class}";
    }, array_keys($layouts));
    $selector = implode(',', $classes);

    $element = (new Crawler($content))->filter($selector)->getNode(0);

    // No special layouts means we have to deal with default (one col) section.
    if (empty($element)) {
      return $this->getLayoutSettings('default') + ['content' => $content];
    }

    // Prepare section settings based on layout.
    $info['content'] = $this->getInnerHtml($element);
    foreach ($layouts as $class => $layout_settings) {
      if (strpos($element->getAttribute('class'), $class) !== FALSE) {
        $info += $layout_settings;
        break;
      }
    }

    return $info;
  }

  /**
   * Returns region name based on given component delta.
   *
   * @param int $delta
   *   Delta of the section component.
   *
   * @return false|string
   *   Region name or FALSE if it was not found.
   */
  protected function getRegionName($delta) {
    $regions = ['first', 'second', 'third', 'fourth', 'fifth'];
    return isset($regions[$delta]) ? $regions[$delta] : FALSE;
  }

}
